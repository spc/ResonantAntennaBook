clear % BesselSkinDepth.m see Lieberman (12.2.10) and Thomson 1927 
% See also Relton "Applied Bessel Functions" p177
clc
%% Variables
% Use ne = 1e16, 1e17, or 1e18 [m-3] for skin depths 20cm, 6.5cm, or 2cm
ne = 1e17; % electron density [m-3] for optimal deltac (arbitrary choice)
% skin depth is inversely proportional to sqrt(ne)
p = 30; % 30 [Pa]
f = 13.56e6; % 13.56 MHz
%% Parameters
Tgas = 400; % [K] arbitrary warm gas
sigma = 1e-19; % e-n cross-section [m2]
Te = 3*11600; % 3 eV in K
%% Constants 
k = 1.38e-23; % Boltzmann's constant [J/K]
m = 9.1e-31; % [kg] e mass
mu0 = pi*4e-7; % permeability of free space [H/m]
q = 1.602e-19; % e charge [C]
eps0 = 8.85e-12; % permittivity of free space [F/m]
%% Collision frequency v, simple estimate
cthe = sqrt(8*k*Te/(pi*m));
ngas = p/(k*Tgas);
v = ngas*sigma*cthe; % collision frequency [rad/s]
w = 2*pi*f; % RF angular frequency [rad/s]
ratio = v/w;
%% electrical conductivity
conductivity = ne*q^2/(m*v) % [siemens]=[1/(ohm.m)]
%% collisional skin depth
deltac = sqrt(m*v/(pi*f*mu0*ne*q^2)) % skin depth [m]
%% Poloidal electric field Etheta, Lieberman (12.2.10)
%deltacOpt = 6.5; % "optimal" skin depth [cm] (arbitrary choice here)
%R = deltacOpt/0.57; % plasma current radius [cm] (Lieberman 1st ed p399)
R = 0.1133; % [m] choose optimal radius for deltac = 0.0646 m
r = [0:0.02:1]*R; % radial variable [cm}
beta = sqrt(2)/deltac; % Lieberman (12.2.9)
j = sqrt(-1);
% Lieberman (12.2.10) for Etheta (NB Bessel J1 and J0): (Hz0 ignored)
Et = -sqrt(j).*(beta./conductivity).*...
    besselj(1,-sqrt(j).*beta*r)./...
    besselj(0,-sqrt(j).*beta*R);

% Lieberman (12.2.8) for Hz (NB twice bessel J0 this time):
Hz = besselj(0,-sqrt(j).*beta*r)./besselj(0,-sqrt(j).*beta*R);

figure(2)
% Lieberman (12.2.12) for absorbed power, but as a function of radius:
Sr = -0.5*real(Et.*conj(Hz)); % 1/2 Re (E . Hz*) POYNTING's vector
% negative sign because it is inward power, along -r
Smax = 1.1443; % to normalize the power
plot(100*r,Sr/Smax,'k-','linewidth',1), hold on
plot(-100*r,Sr/Smax,'k-','linewidth',1),
legend('optimal skin depth, 6.5 cm','fontsize',10),
legend('boxoff')
xlabel('plasma radius [cm]')
ylabel('normalized power')
text(-4,0.6,'- - - skin depth 20 cm','fontsize',10)
text(-4,0.4,'\cdot \cdot \cdot  skin depth 2 cm','fontsize',10)
xlim([-100*R 100*R])
ylim([-0.05 1.1])
