Run BesselSkinDepth.m for Matlab output Fig. 2.
The book Fig. 5.3 is obtained by editing the electron density "ne" as follows:
"Use ne = 1e16, 1e17, or 1e18 [m-3] for skin depths 20cm, 6.5cm, or 2cm"