%% Calculate the mutual inductances between antenna elements
% CalcMatMutToeplitz (was CalcMatMut)
for n = 1:N
    
    xa(n) = (n-1)*H1;
    ya(n) = H2/2;
    xb(n) = (n-1)*H1;
    yb(n) = -H2/2;
    
end

%k=1;

for n = 1:N
        
        if n==1 % was k
            
           % if n==N do this at the end
                
                % mu1aa(n,k)=LEnds;
             
               %d1ab = H2;
                %del1ab = -H1+eps;
                %al1ab = 2*(H1)+del1ab;
                %bet1ab = (H1)+del1ab;
                %gam1ab = (H1)+del1ab;
                
                %mu1ab(n,k) = facmu1.*1e-9.*(al1ab.*asinh(al1ab./d1ab) - bet1ab.*asinh(bet1ab./d1ab) - gam1ab.*asinh(gam1ab./d1ab) + del1ab.*asinh(del1ab./d1ab) - ...
                    %sqrt(al1ab.^2 + d1ab^2)+sqrt(bet1ab.^2 + d1ab^2)+sqrt(gam1ab.^2 + d1ab^2)-sqrt(del1ab.^2 + d1ab^2));
                
               %mu2(n,k)=L2;
            %else
                mu1aa(n)=L1;
                
                d1ab = H2;
                del1ab = -H1+eps;
                al1ab = 2*(H1)+del1ab;
                bet1ab = (H1)+del1ab;
                gam1ab = (H1)+del1ab;
                
                mu1ab(n) = facmu1.*1e-9.*(al1ab.*asinh(al1ab./d1ab) - bet1ab.*asinh(bet1ab./d1ab) - gam1ab.*asinh(gam1ab./d1ab) + del1ab.*asinh(del1ab./d1ab) - ...
                    sqrt(al1ab.^2 + d1ab^2)+sqrt(bet1ab.^2 + d1ab^2)+sqrt(gam1ab.^2 + d1ab^2)-sqrt(del1ab.^2 + d1ab^2));
                
                mu2(n)=L2;
            %end
            
        else
            
            d1aa = eps;
            del1aa = abs(xa(n)-xa(1))-H1+eps;
            al1aa = 2*(H1)+del1aa;
            bet1aa = (H1)+del1aa;
            gam1aa = (H1)+del1aa;
            
            mu1aa(n) = facmu1.*1e-9.*(al1aa.*asinh(al1aa./d1aa) - bet1aa.*asinh(bet1aa./d1aa) - gam1aa.*asinh(gam1aa./d1aa) + del1aa.*asinh(del1aa./d1aa) - ...
                sqrt(al1aa.^2 + d1aa^2)+sqrt(bet1aa.^2 + d1aa^2)+sqrt(gam1aa.^2 + d1aa^2)-sqrt(del1aa.^2 + d1aa^2));
            
            d1ab = H2;
            del1ab = abs(xa(n)-xb(1))-H1+eps;
            al1ab = 2*(H1)+del1ab;
            bet1ab = (H1)+del1ab;
            gam1ab = (H1)+del1ab;
            
            mu1ab(n) = facmu1.*1e-9.*(al1ab.*asinh(al1ab./d1ab) - bet1ab.*asinh(bet1ab./d1ab) - gam1ab.*asinh(gam1ab./d1ab) + del1ab.*asinh(del1ab./d1ab) - ...
                sqrt(al1ab.^2 + d1ab^2)+sqrt(bet1ab.^2 + d1ab^2)+sqrt(gam1ab.^2 + d1ab^2)-sqrt(del1ab.^2 + d1ab^2));
            
            
            d2 = abs(xa(n)-xa(1))+eps;
            del2 = -H2;
            al2 = 2*(H2)+del2;
            bet2 = (H2)+del2;
            gam2 = (H2)+del2;
            
            mu2(n) = facmu1.*1e-9.*(al2.*asinh(al2./d2) - bet2.*asinh(bet2./d2) - gam2.*asinh(gam2./d2) + del2.*asinh(del2./d2) - ...
                sqrt(al2.^2 + d2^2)+sqrt(bet2.^2 + d2^2)+sqrt(gam2.^2 + d2^2)-sqrt(del2.^2 + d2^2));
    end
end
mu1aa = toeplitz(mu1aa,mu1aa); % convert all three vectors to Toeplitz matrices
mu1aa(N,N)=LEnds; % Reinstate the exception: mu1aa(n,k)=LEnds for n=k=N
mu1ab = toeplitz(mu1ab,mu1ab);
mu2 = toeplitz(mu2,mu2);
