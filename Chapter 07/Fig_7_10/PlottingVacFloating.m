%% Main program PlasmaOnComplexImage.m

clear
clc

%% plot des spectres d'impedance
load('f','f');
load('ZinVide','ZinVide')
load('fnet','fnet');
load('Rnet','Rnet')
figure(1)
clf
loglog(fnet/1e6,Rnet,'r','LineWidth',1)
axis([6 40 1e-2 1e4]), hold on

loglog(f/1e6,real(ZinVide),'k','LineWidth',1)
set(gca,'fontname','times','fontsize',16,'LineWidth',1.0,...
    'TickLength',[0.02 0.025])%,...
    %'XTickLabel',{'10'}) % for axes
xlabel('frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')
ylabel('real impedance [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
text(20,0.2,'MTL model','fontsize',16,'fontname','times')
text(21.5,11,'measure','fontsize',16,'fontname','times','color','red')
text(18,4000,'DC-floating antenna','fontsize',16,'fontname','times')
text(18,1500,'(no plasma)','fontsize',16,'fontname','times')
v=[1e-2 1e4];
mark=13.15*[1 1];
plot(mark,v,'k--','MarkerSize',8,'LineWidth',1.5), hold on
text(10.45,0.1,'mode  \it{m}\rm{ = 8}','fontsize',16,'fontname','times')
text(10.35,0.03,'13.15  MHz','fontsize',16,'fontname','times')
text(5.9,0.0047,'6','fontsize',16,'fontname','times')
%text(19,0.0055,'20','fontsize',16,'fontname','times')
%text(28.5,0.0055,'30','fontsize',16,'fontname','times')
%text(38,0.0055,'40','fontsize',16,'fontname','times')
text(28,400,'\it{m}\rm{ = 2}','fontsize',16,'fontname','times')
text(19.2,430,'4','fontsize',16,'fontname','times')
text(15.4,1200,'6','fontsize',16,'fontname','times')
text(10.9,900,'10','fontsize',16,'fontname','times')
text(9.9,700,'12','fontsize',16,'fontname','times')
text(9.1,450,'14','fontsize',16,'fontname','times')

