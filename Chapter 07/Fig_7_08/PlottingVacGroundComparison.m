% PlottingVacGroundComparison.m
clear, clc

%% plot impedance spectra
load('f','f');
load('ZinVide','ZinVide'); % EM model
% lumped element model:
load('ZinVideLumped','ZinVideLumped');
figure(1)
clf
loglog(f/1e6,real(ZinVideLumped),'r','LineWidth',1)
axis([6 40 1e-2 1e4]), hold on
axis([7 40 1e-1 0.3e4]), hold on

loglog(f/1e6,real(ZinVide),'k','LineWidth',1)
axis([6 40 1e-2 1e4]), hold on
set(gca,'fontname','times','fontsize',16,'LineWidth',1,...
    'TickLength',[0.02 0.025])%,...
    %'XTickLabel',{'10'}) % for axes
xlabel('frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')
ylabel('real impedance [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
text(20,0.1,'MTL model','fontsize',16,'fontname','times')
text(20,0.035,'lumped element','fontsize',16,'fontname','times','color','red')
text(19,4000,'grounded antenna','fontsize',16,'fontname','times')
text(19,1500,'(no plasma)','fontsize',16,'fontname','times')
text(10.3,0.1,'mode  \it{m}\rm{ = 8}','fontsize',16,'fontname','times')
text(11.5,0.03,'13  MHz','fontsize',16,'fontname','times')
v=[1e-2 1e4];
mark=13*[1 1];
plot(mark,v,'k--','MarkerSize',8,'LineWidth',1.5), hold on
text(27,55,'\it{m}\rm{ = 2}','fontsize',16,'fontname','times')
text(19.2,430,'4','fontsize',16,'fontname','times')
text(15.4,2900,'6','fontsize',16,'fontname','times')
text(10.9,2800,'10','fontsize',16,'fontname','times')
text(9.8,1000,'12','fontsize',16,'fontname','times')
text(9.1,100,'14','fontsize',16,'fontname','times')

