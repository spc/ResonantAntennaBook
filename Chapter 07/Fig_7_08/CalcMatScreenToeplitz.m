%% Calculate the mutual inductances between antenna and screen images
DscreenC = Dscreen; % distance of capacitors from the screen [cm]
DscreenL = Dscreen; % distance of inductors from the screen [cm]


%k=1; % remove double loop and double index for Toeplitz treatment
for n = 1:N
            
            d1aa = 2*Dscreen;
            del1aa = abs(xa(n)-xa(1))-H1+eps;
            al1aa = 2*(H1)+del1aa;
            bet1aa = (H1)+del1aa;
            gam1aa = (H1)+del1aa;
            
            mu1Imaa(n) = facmu1.*1e-9.*(al1aa.*asinh(al1aa./d1aa) - bet1aa.*asinh(bet1aa./d1aa) - gam1aa.*asinh(gam1aa./d1aa) + del1aa.*asinh(del1aa./d1aa) - ...
                sqrt(al1aa.^2 + d1aa^2)+sqrt(bet1aa.^2 + d1aa^2)+sqrt(gam1aa.^2 + d1aa^2)-sqrt(del1aa.^2 + d1aa^2));
           
            d1ab = sqrt(H2.^2+4*Dscreen.^2);
            del1ab = abs(xa(n)-xb(1))-H1+eps;
            al1ab = 2*(H1)+del1ab;
            bet1ab = (H1)+del1ab;
            gam1ab = (H1)+del1ab;
            
            mu1Imab(n) = facmu1.*1e-9.*(al1ab.*asinh(al1ab./d1ab) - bet1ab.*asinh(bet1ab./d1ab) - gam1ab.*asinh(gam1ab./d1ab) + del1ab.*asinh(del1ab./d1ab) - ...
                sqrt(al1ab.^2 + d1ab^2)+sqrt(bet1ab.^2 + d1ab^2)+sqrt(gam1ab.^2 + d1ab^2)-sqrt(del1ab.^2 + d1ab^2));
            
            
            d2 = sqrt((xa(n)-xa(1)).^2+4*Dscreen.^2)+eps;
            del2 = -H2;
            al2 = 2*(H2)+del2;
            bet2 = (H2)+del2;
            gam2 = (H2)+del2;
            
            muIm2(n) = facmu1.*facmu2*1e-9.*(al2.*asinh(al2./d2) - bet2.*asinh(bet2./d2) - gam2.*asinh(gam2./d2) + del2.*asinh(del2./d2) - ...
                sqrt(al2.^2 + d2^2)+sqrt(bet2.^2 + d2^2)+sqrt(gam2.^2 + d2^2)-sqrt(del2.^2 + d2^2));
   
end
mu1Imaa = toeplitz(mu1Imaa,mu1Imaa); % convert all three vectors to Toeplitz matrices
mu1Imab = toeplitz(mu1Imab,mu1Imab);
muIm2 = toeplitz(muIm2,muIm2);

mu1aa = mu1aa-mu1Imaa;
mu1ab = mu1ab-mu1Imab;
mu2 = mu2-muIm2;