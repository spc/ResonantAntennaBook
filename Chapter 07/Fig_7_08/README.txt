README.txt for book Fig. 7.8

1) Run PlasmaOffVacGround.m to obtain the EM model impedance spectrum (same as for Fig. 7.7), to create ZinVide.mat data

2) Run PlasmaOffVacGroundLumped.m to obtain the lumped element model impedance spectrum, to create ZinVideLumped.mat data

3) Run PlottingVacGroundComparison.m to compare EM and lumped element impedance, and obtain book Fig. 7.8.
