%% PlottingVacGroundVoltages.m

clear
clc

%% plot experimental measurements of node voltages:
L=load('A10A16_13.364MHz.dat');
B=L(:,5);
A=L(:,3);
figure(1)
clf
subplot(221)
N=25;
norm1 = max(A);
AmpEnva = [0.5263,0.6354,0.7394,0.8891,1,0.8716,0.7343,0.638,0.5546];
AmpEnvx = [1,4,7,10,13,16,19,22,25];
 plot(1:N,A/norm1,'v-k','LineWidth',2)
        hold
 plot(1:N,B/norm1,'s-r','LineWidth',2)
 plot(AmpEnvx,AmpEnva,'b','LineWidth',2)
axis([1 25 -0.2 1.2]), hold on
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,...
    'TickLength',[0.02 0.025])
xlabel('node number \it{n}\rm{ = 1 to 25}','interpreter','tex','fontsize',16,'fontname','times')
ylabel('normalized voltage','interpreter','tex','fontsize',16,'fontname','times')
legend('\it{An}','\it{Bn}','env','tex','fontsize',16,'fontname','times')
text(1.7,1.08,'(a) grounded, measurement','fontsize',16,'fontname','times')

figure(2)
clf
subplot(221)
load('AA','AA');
load('BB','BB')
trefV=18e-9; % 
w=2*pi*13.35e6; % (=13.0 + 0.35 MHz) 
norm = max(real(AA.*exp(1i.*w.*trefV)));
AmpEnva = [0.5737,0.6714,0.7548,0.8635,1,0.8635,0.7548,0.6714,0.5737];
AmpEnvx = [1,4,7,10,13,16,19,22,25];
plot(1:N,real(AA.*exp(1i.*w.*trefV))/norm,'v-k','LineWidth',2)
        hold
plot(1:N,real(BB.*exp(1i.*w.*trefV))/norm,'s-r','LineWidth',2)
plot(AmpEnvx,AmpEnva,'b','LineWidth',2)
axis([1 25 -0.2 1.2]), hold on

set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,...
    'TickLength',[0.02 0.025])
xlabel('node number \it{n}\rm{ = 1 to 25}','interpreter','tex','fontsize',16,'fontname','times')
ylabel('normalized voltage','interpreter','tex','fontsize',16,'fontname','times')
text(1.7,1.08,'(b) grounded, MTL model','fontsize',16,'fontname','times')

figure(3)
clf
subplot(221)
load('AAlumped','AAlumped');
load('BBlumped','BBlumped')
trefV=18e-9; % 
w=2*pi*13.55e6; % (=13.2 + 0.35 MHz) 
norm1 = max(real(AAlumped.*exp(1i.*w.*trefV)));
AmpEnva = [0.7439,0.8238,0.8478,0.8478,1,0.8478,0.8478,0.8238,0.7439];
AmpEnvx = [1,4,7,10,13,16,19,22,25];
 plot(1:N,real(AAlumped.*exp(1i.*w.*trefV))/norm1,'v-k','LineWidth',2)
        hold
plot(1:N,real(BBlumped.*exp(1i.*w.*trefV))/norm1,'s-r','LineWidth',2)
plot(AmpEnvx,AmpEnva,'b','LineWidth',2)
axis([1 25 -0.2 1.2]), hold on

set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,...
    'TickLength',[0.02 0.025])
xlabel('node number \it{n}\rm{ = 1 to 25}','interpreter','tex','fontsize',16,'fontname','times')
ylabel('normalized voltage','interpreter','tex','fontsize',16,'fontname','times')
text(1.7,1.08,'(c) grounded, lumped-element model','fontsize',16,'fontname','times')


