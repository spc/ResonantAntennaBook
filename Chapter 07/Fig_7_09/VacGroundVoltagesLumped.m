%% Main program VacGroundVoltagesLumped.m
% from PlasmaOffVacGroundLumped.m
% lumped element model (see InitZin5 below)
clear, clc

% physical constants
q = 1.6e-19; % electron charge magnitude [C]
mu0 = 4*pi*1e-7; % permeability of vacuum [H/m]
eps0 = 8.85e-12; % permittivity of vacuum [F/m]
me = 9.1e-31; % electron mass [kg]
epsr = 1; % the majority of the reactor is in vacuum
rhoCu = 16.8e-9; % resistivity of copper [ohm.m]
rhoAl = 27e-9; % resistivity of aluminium [ohm.m]

%% Frequency range
finf = 6e6; % [Hz]
fsup = 40e6; % [Hz]
df = 1e4; % [Hz]
f = logspace(log10(finf),log10(fsup),1000); % frequency array in log space
w = 2*pi*f;

%% antenna parameters
N = 25; % number of legs
nbt = 2; % number of ground connections

% ground nodes (Kt = 1 for nodes on A, Kt = 2 for nodes on B)
Kt1 = 1;
Nt1 = 10; % ground on A10
Nt2 =16; % ground on A16
Kt2 = 1;

Nf = 13; % RF input node

% Dimensions:
% index 1 for stringers, index 2 for legs
% Attention units in [cm], used for mutual inductance calculations

H1 = 5; % stringer length [cm]
H2 = 120; % leg length [cm]

a1 = 0.6; % stringer effective radius [cm]
a2 = 0.4; % leg radius [cm]

% Dscreen = distance to backplate:
% (open-top reactor for network analyser measurements)
Dscreen = 5; % distance to back screen [cm]

% Capacitance and self inductances:
C1 = 375e-12; % stringer capacitor [F]

L1 = mu0.*(H1/100)./2./pi.*(log(2.*H1./a1)-1); % stringer self-inductance

L2 = mu0.*(H2/100)./2./pi.*(log(2.*H2./a2)-1); % leg self-inductance

% connector impedances
dTL = 0.0; % length of 50ohm transmission line (zero for direct connection)
dconS = 0.05; % stub connector length [m]
aconS = 0.0025; % stub connector radius [m]

% Resistances
skin = sqrt(2.*rhoCu./w./mu0); % skin depth in copper [m]
skinAl = sqrt(2.*rhoAl./w./mu0); % skin depth in aluminium [m]

Surf1 = pi.*((a1/100).^2-((a1/100)-skin).^2); % stringer cross-section area
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2); % leg cross-section area

% Estimated surface return resistance in aluminium housing: 
% (arbitrarily spread over 10 leg radii)
SurfRet = 10.*a2./100.*skinAl;

% stub connector cross-section area
SurfRcons = pi.*((aconS/100).^2-((aconS/100)-skin).^2); 


Rsd1 = rhoCu.*H1/100./Surf1; % stringer conductor resistance [ohms]
Rsd2 = rhoCu./Surf2; % leg resistance per unit length
Rsret = rhoAl./SurfRet; % estimated housing resistance per unit length

% stub connectors inductance and resistance:
LconS = mu0.*(dconS)./2./pi.*(log(2.*(dconS)./aconS)-1); 
RconS = rhoCu.*dconS./SurfRcons;

% Equivalent Series Resistance for capacitors
ESR = 1.5*(0.001+1e-4*f.^2/1e12); % [ohms]

% Resistive losses for vacuum loading:
R1 = Rsd1 + ESR; % stringer total resistance [ohms]
R2 = Rsd2.*H2/100; % leg resistance

%% Enable/disable parameters:
facG = 1; % impedance factor to ground = 1 for grounded antenna
facmu1 =1; % facmu1 for stringer mutuals
facmu2 = 1; % facmu2=1 for leg mutuals

%% Lumped element calculation in vacuum (without plasma)

InitZin5Lumped

ZinVideLumped = ZinVide;
%% select one frequency for node voltage calculations:

onefrequPlasma5VACLUMPED
AAlumped = AA;
BBlumped = BB;
save('AAlumped','AAlumped')
save('BBlumped','BBlumped')


