README.txt for book Fig. 7.9

1) Run VacGroundVoltages.m to obtain the node voltages AA.mat and BB.mat

2) Run VacGroundVoltagesLumped.m to obtain the node voltages AALumped.mat and BBLumped.mat

3) Run PlottingVacGndVoltages.m for the 3 graphs in book Fig. 7.9