%% Definition vecteurs unitaire SA et SB

Iin = 1;

for k = 1:N
    
    if k==Nf
        
        Iin1(k) = Iin;
        
    else
        Iin1(k) = 0;
        
    end
end

[DD,VV]=size(Iin1);
if DD==1
    Iin1=Iin1';
else
end


%% frequency choice

f = 13.55e6; % chosen 0.35 MHz ABOVE resonance 13.2 MHz for lumped-element
w = 2*pi*f;
p = find(w);

skin = sqrt(2.*rhoCu./w./mu0);
skinAl = sqrt(2.*rhoAl./w./mu0);
Surf1 = pi.*((a1/100).^2-((a1/100)-skin).^2);
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2);
SurfRet = 10.*a2./100.*skinAl;
SurfRcons = pi.*((aconS/100).^2-((aconS/100)-skin).^2);


RconS = rhoCu.*dconS./SurfRcons;
Rsd1 = rhoCu.*H1/100./Surf1; % stringer conductor resistance
Rsd2 = rhoCu./Surf2; % leg resistance per unit length

Rsret = rhoAl./SurfRet; % estimated housing resistance per unit length

% Equivalent Series Resistance for capacitors

ESR = 3*(0.0025+1e-4*f.^2/1e12);
R1 = Rsd1 + ESR; % total stringer resistance
R2 = Rsd2.*H2/100; % leg resistance

matCR0 = diag(diag(matCR));
matCX0 = diag(diag(matCX));

% eigenvalue solution for long legs

matZ = 1i*w*matLR-w*matLX+(Rsd2+Rsret)*Id;
matY = 1i*w*matCR-w*matCX;

ZY = matZ*matY;
[TV,Gam] = eig(ZY);
Mlam = Id/sqrt(Gam);

for k =1:N
    for n = 1:N
        if n==k
            Ep(n,k) = exp(sqrt(Gam(n,k))*H2/200);
            Em(n,k) = exp(-sqrt(Gam(n,k))*H2/200);
            
        else
            Ep(n,k) = 0;
            Em(n,k) = 0;
            
        end
    end
end

% matrices impedances stringer

ZG = facG*(RconS+1i*w*LconS);
if nbt==1
    for k = 1:N
        
        for j = 1:N
            if j==k
                if Kt1==1
                    
                    if j==Nt1
                        Ya(j,k) = 1/ZG;
                        Yb(j,k) = 0;
                    else
                        Ya(j,k) = 0;
                        Yb(j,k) = 0;
                    end
                else
                    if j==Nt1
                        Yb(j,k) = 1/ZG;
                        Ya(j,k) = 0;
                    else
                        Ya(j,k) = 0;
                        Yb(j,k) = 0;
                    end
                end
            else
                Ya(j,k) = 0;
                Yb(j,k) = 0;
            end
        end
    end
else
    for k = 1:N
        
        for j = 1:N
            if j==k
                if Kt1==1
                    
                    if j==Nt1
                        Ya(j,k) = 1/ZG;
                        Yb(j,k) = 0;
                    elseif j==Nt2
                        Ya(j,k) = 1/ZG;
                        Yb(j,k) = 0;
                    else
                        Ya(j,k) = 0;
                        Yb(j,k) = 0;
                    end
                else
                    if j==Nt1
                        Yb(j,k) = 1/ZG;
                        Ya(j,k) = 0;
                    elseif j==Nt2
                        Yb(j,k) = 1/ZG;
                        Ya(j,k) = 0;
                    else
                        Ya(j,k) = 0;
                        Yb(j,k) = 0;
                    end
                end
            else
                Ya(j,k) = 0;
                Yb(j,k) = 0;
            end
        end
    end
end
Zmu1 = 1i*w*mu1aa + (R1-1i/w/C1)*Id;
Zmu2 = 1i*w*mu1ab;
Yc = matZ\TV*sqrt(Gam)/TV;

% general equation

MC = Ep+Em;
MS = Ep-Em;

m1 = UT\Zmu1/U;
m2 = UT\Zmu2/U;

OS = (m1+m2)\TV*MC+Yc*TV*MS;
OD = (m1-m2)\TV*MS+Yc*TV*MC;

O1 = 1/2*(Ya-Yb)*TV*MC;
O2 = -1/2*(Yb+Ya)*TV*MS;
O3 = -1/2*(Yb+Ya)*TV*MC;
O4 = 1/2*(Ya-Yb)*TV*MS;

DEL = ((OS-O3)/(OS-O3-O1)*(O2+O4-OD)-O4)\Iin1;
SIG = (OS-O3-O1)\(O2+O4-OD)*DEL;

AA = TV*(MC*SIG-MS*DEL)/2;
BB = TV*(MC*SIG+MS*DEL)/2;


Zin = AA(Nf);

Rin = real(Zin);
Xin = imag(Zin);
ZConectic
Rin2;
Pin = 2000; % arbitrary choice of 2000 W input power
Iin = sqrt(Pin/Rin);


%% Re-definition of vectors SA,SB

for k = 1:N
    
    if k==Nf
        
        Iin1(k) = Iin;
        
    else
        Iin1(k) = 0;
        
    end
end

[DD,VV]=size(Iin1);
if DD==1
    Iin1=Iin1';
else
end

DEL = ((OS-O3)/(OS-O3-O1)*(O2+O4-OD)-O4)\Iin1;
SIG = (OS-O3-O1)\(O2+O4-OD)*DEL;

AA = TV*(MC*SIG-MS*DEL)/2;
BB = TV*(MC*SIG+MS*DEL)/2;

IA = Yc*TV*(MC*DEL-MS*SIG)/2;
IB = Yc*TV*(MC*DEL+MS*SIG)/2;

MM = Zmu1\UT*AA;
KK = Zmu1\UT*BB;

SA = -Ya*AA;
SB = -Yb*BB;
IGA = sum(SA);
IGB = sum(SB);
if Kt1==1
    It1 = SA(Nt1);
else
    It1 = SB(Nt1);
end
if Kt2==1
    It2 = SA(Nt2);
else
    It2 = SB(Nt2);
end
IC = Iin-IGA-IGB;



dz = 1e-2;
z = -H2/200+eps:dz:H2/200+eps;
nz = find(z);

diaZ = real(diag(matZ));
diaY = real(diag(matY));


for nz = min(nz):max(nz)
    
    for k =1:N
        for n=1:N
            if n==k
                Epz(n,k) = exp(sqrt(Gam(n,k))*z(nz));
                Emz(n,k) = exp(-sqrt(Gam(n,k))*z(nz));
                MCz(n,k) = Epz(n,k)+Emz(n,k);
                MSz(n,k) = Epz(n,k)-Emz(n,k);
            else
                Epz(n,k) = 0;
                Emz(n,k) = 0;
                MCz(n,k) = 0;
                MSz(n,k) = 0;
            end
        end
    end
    
    Vz = TV*(MCz*SIG-MSz*DEL)/2;
    Iz = Yc*TV*(MCz*DEL-MSz*SIG)/2;
    

    for nn = 1:N
        nnn = num2str(nn);
        fff=(['V' nnn '(nz)=Vz(nn);']);
        eval(fff);
        fff=(['I' nnn '(nz)=Iz(nn);']);
        eval(fff);

    end
end



for nn = 1:N
    nnn = num2str(nn);
    if nn==1
        
        fff=(['MV=V' nnn ';']);
        eval(fff);
        fff=(['MI=I' nnn ';']);
        eval(fff);

      
  
    else
        
        fff=(['MV=cat(1,MV,V' nnn ');']);
        eval(fff);
        fff=(['MI=cat(1,MI,I' nnn ');']);
        eval(fff);

        
    end
end

%% figure 3: A(t)

on3 = 1;
T = 1/f;

T = 1/f;
dt = T/100;
t = eps:dt:2*T;

for k=1:N
    
    
    nk = num2str(k);
    fff=(['A' nk '=AA(k).*exp(1i*w*t);']);
    eval(fff);
    fff=(['B' nk '=BB(k).*exp(1i*w*t);']);
    eval(fff);
    fff=(['IA' nk '=IA(k).*exp(1i*w*t);']);
    eval(fff);
    fff=(['IB' nk '=IB(k).*exp(1i*w*t);']);
    eval(fff);
    
    if on3==1
        
        if k==1
            figure(3), clf
            subplot(2,1,1)
            fff=(['plot(t/1e-9,real(A' nk '))']);
            eval(fff);
            hold
            fff=(['plot(t/1e-9,real(B' nk '))']);
            eval(fff);
            xlabel('t [ns]')
            ylabel('[V]')
            title('tensions An(t)')
            subplot(2,1,2)
            fff=(['plot(t/1e-9,real(IA' nk '))']);
            eval(fff);
            hold
            fff=(['plot(t/1e-9,real(IB' nk '))']);
            eval(fff);
            xlabel('t [ns]')
            ylabel('[A]')
            title('courants IA(t), IB(t)')
        else
            figure(3)
            subplot(2,1,1)
            fff=(['plot(t/1e-9,real(A' nk '))']);
            eval(fff);
            fff=(['plot(t/1e-9,real(B' nk '))']);
            eval(fff);
            subplot(2,1,2)
            fff=(['plot(t/1e-9,real(IA' nk '))']);
            eval(fff);
            fff=(['plot(t/1e-9,real(IB' nk '))']);
        end
    else
    end
    
    
end

%% figure 4 : abs(A), abs(B)



on4 = 1;

if on4==1;
    
    trefV = 18e-9; % input('time (ns) for voltages:')*1e-9;
    trefI = 18e-9; % input('time (ns) for currents:')*1e-9;
else
end

for k=1
    
    if on4==1
        figure(4)
        clf
        
        plot(1:N,real(AA.*exp(1i.*w.*trefV)),'v-k','LineWidth',3)
        hold
        plot(1:N,real(BB.*exp(1i.*w.*trefV)),'s-r','LineWidth',3)
        title('An (triangles) and Bn (squares)');
        xlabel('n')
        ylabel('[V]')

    else
    end
    
    
    
end


IIA = real(IA.*exp(1i.*w.*trefI));
IIB = real(IB.*exp(1i.*w.*trefI));

%% figure 5 : abs(IA)

on5 = 1;

for k=1
    if on5==1
        figure(5)
        clf
        
        plot(1:N,real(IA.*exp(1i.*w.*trefI)),'v-k','LineWidth',2)
        hold
        plot(1:N,real(IB.*exp(1i.*w.*trefI)),'s-r','LineWidth',2)
        xlabel('n')
        ylabel('A')
        title('IA, IB')
        
    else
    end
end


%% figure 6 : abs(M), abs(K)

on6 = 1;

for k=1
    
    if on6==1
        figure(6)
        clf
        title('Mn (triangles) and Kn (squares), red:lumped solution');
        
        
        plot(1:N,abs(MM),'v-k','LineWidth',2)
        hold
        plot(1:N,abs(KK),'v-r','LineWidth',2)
        
        xlabel('n')
        ylabel('[A]')
        title('red=KK, black==MM')
        
    else
    end
    
    
    
end


% Figure 7 : Mappings In(z) , Vn(z) 

figure(7),clf
n = 1:1:N;
subplot(2,1,1)
mesh(z,(n-1)*H1/100,abs(MI));
subplot(2,1,2)
mesh(z,(n-1)*H1/100,abs(MV));




