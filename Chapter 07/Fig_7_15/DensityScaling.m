%% DensityScaling.m

clear
clc

%neExpt = [0.679 1 1.45 2.05]*1e16; % [1/m3] Remy data for A10A16
%Prf = [500 1000 1500 2000]; % RF power [W] Remy data for A10A16

neExpt = [0.7 1.1 2.1 20]*1e16; % [1/m3] Remy data for floating
Prf = [500 1000 2000 15000]; % RF power [W] Remy data for floating

ne1D = [0 Prf*2.1e16/2000]; % ne proportional Prf (power balance 1D model)
Prf1D = [0 Prf];
subplot(111)
plot(Prf,neExpt,'sq',Prf1D,ne1D)
axis([0 15000 0 2.2e17])
xlabel('RF power [W]')
ylabel('plasma density [1/m3]')
title('Density vs RF power scaling [A10A16]')
legend('Remy measurements','Power balance 1D model')
