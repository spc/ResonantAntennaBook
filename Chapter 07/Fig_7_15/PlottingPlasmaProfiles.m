%% PlottingPlasmaProfiles.m
clear
clc
%% Read Remy's data:
Profile=load('AxialCylindricalProbeInTheCenterAntenna.txt', '-ascii');
x = Profile(5:75,1); n = Profile(5:75,2);
n=2.5*n; % arbitrary probe correction

map=load('mapping_100x100_interval=133.33mm.txt', '-ascii');
xprobe = [0:120/9:120];
xprobe(1) = .1;
%nprobe = [1.58 2.32 2.4 2.55 2.75 2.8 2.7 2.63 2.56 1.38];
map';
nprobe = (map(:,6)+map(:,5))/2;
nprobe';

figure(1)
subplot(211),
xlegs = [15:15:120-15]; % positions of m=8 nodes (7)

one = ones(1,length(xlegs));
%plot(x/10, 3.169*n./max(n),'k',xlegs,3.169*one/2,'sqk','LineWidth',2)
plot(x/10, n/1e17,'k',xlegs,one/2,'sqk','LineWidth',2)
hold on
plot(xprobe,1.955*nprobe/2.72e17,'o','MarkerFaceColor','k','MarkerEdgeColor','k')
ylim([0 2.5]), 
xlim([0 120]), % Floating
xlabel('position across the legs [cm]','fontsize',16,'fontname','times')
ylabel('\it n_i \rm [10^{17}m^{-3}]','interpreter','tex','fontsize',16,'fontname','times')
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0) % for axes
set(gca,'ytick',[1 2 3])

subplot(212),
nprobe = (map(6,:)+map(5,:))/2;
plot(xprobe,2.255*nprobe/3.157e17,'o','MarkerFaceColor','k','MarkerEdgeColor','k')
hold on
%CurrentProfile = load('CurrentProfile.txt','-ascii')
%xstanding = load('x.txt','-ascii')
%plot(xstanding*100,CurrentProfile.^2/16) % to see if standing wave
%reproduces measured density profile
ylim([0 2.5]), 
xlim([0 120]), % Floating
xlabel('position along central leg [cm]','fontsize',16,'fontname','times')
ylabel('\it n_i \rm [10^{17}m^{-3}]','interpreter','tex','fontsize',16,'fontname','times')
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0) % for axes
set(gca,'ytick',[1 2 3])

figure(3)
subplot(111),
map=load('mapping_100x100_interval=133.33mm.txt', '-ascii');
%surf(map) % the maximum IS at the centre
[X,Y] = meshgrid(0:120/9:120); % see Fig. 8 dissipative paper
mx = max(max(map));
map=map/mx;
N = 8; % number of contours. 8 is best
v = [1/N:1/N:1]; % only five contours by default
contour(X,Y,map',v,'k','LineWidth',2.0), hold on
%contour(X,Y,map',[0.5 0.5],'k','LineWidth',2.0)
plot(60,60,'xk','MarkerSize',10,'LineWidth',2.0)
%plot([0 120],[60 60],'-.k')
plot(60,120,'vk','MarkerFaceColor','k','MarkerSize',10)
%grid
%clabel(cs, h, v)
%axis equal
axis([0 120 0 120])
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0) % for axes
xlabel('position across the legs [cm]','fontsize',16,'fontname','times')
ylabel('position along the legs [cm]','fontsize',16,'fontname','times')
text(35,127,'RF connection point','fontsize',16,'fontname','times')


