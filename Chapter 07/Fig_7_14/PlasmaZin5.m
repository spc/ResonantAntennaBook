% PlasmaZin5.m
% keep in memory the matrices of mutuals without plasma:
mu1aab = mu1aa;
mu1abb = mu1ab;
mu2b = mu2;

% USE TOEPLITZ UNIFORM STRIPLINE FOR p.u.l. capacitance matrix
MicrostripTest % USING PARTIAL IMAGE THEORY FOR GLASS MICROSTRIP ON PLASMA
    matCR = real(CM); % using Potential Coefficients stripline cap. matrix
    matCX = imag(CM); % dissipation in stripline
% add in the parallel baseplate miscrostrip capacitance matrix:
% this is not strictly correct: should add the voltage matrix P of 
% the various images BEFORE calculating the inverse of the net P values.
%    matCR = matCR + matCRb; % matCRb shifts to slightly lower frequency,
% not a huge error.
%% Calculate impedance as a function of frequency

p = find(w);

disp('calculation with plasma')
disp(' ')
tN0 = 10;
Lfr = length(f);
for p = min(p):max(p)
    
    testN0 = p/Lfr;
    
    if testN0*100>=tN0
        disp([num2str(tN0) ' %'])
        tN0 = tN0+10;
    else
    end
    
CalcMatScreenPlasmaToeplitz % depends on frequency via plasma skin depth
    
 % Definition of per unit length matrices for the legs   
    matL2 = mu2/H2*100;
    matLR = real(matL2);
    matLX = imag(matL2);
    
    % eigenvalue solution for long legs
    
     % matZ and matY = matrices Z and Y for the multiline analysis 
    matZ = 1i*w(p)*matLR-w(p)*matLX+(Rsd2(p)+Rsret(p))*Id;
    matY = 1i*w(p)*matCR-w(p)*matCX;
    
    ZY = matZ*matY;
    [TV,Gam] = eig(ZY);
    Mlam = Id/sqrt(Gam);
    
    for k =1:N
        for n = 1:N
            if n==k
                Ep(n,k) = exp(sqrt(Gam(n,k))*H2/200);
                Em(n,k) = exp(-sqrt(Gam(n,k))*H2/200);
                
            else
                Ep(n,k) = 0;
                Em(n,k) = 0;
                
            end
        end
    end
    
 % ZG is the impedance of a ground stub connector:
 % facG >> 1 for FLOATING antenna here:
    ZG = facG*(RconS(p)+1i*w(p)*LconS);
    
    if nbt==1
        for k = 1:N
            
            for j = 1:N
                if j==k
                    if Kt1==1
                        
                        if j==Nt1
                            Ya(j,k) = 1/ZG;
                            Yb(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    else
                        if j==Nt1
                            Yb(j,k) = 1/ZG;
                            Ya(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    end
                else
                    Ya(j,k) = 0;
                    Yb(j,k) = 0;
                end
            end
        end
    else
        for k = 1:N
            
            for j = 1:N
                if j==k
                    if Kt1==1
                        
                        if j==Nt1
                            Ya(j,k) = 1/ZG;
                            Yb(j,k) = 0;
                        elseif j==Nt2
                            Ya(j,k) = 1/ZG;
                            Yb(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    else
                        if j==Nt1
                            Yb(j,k) = 1/ZG;
                            Ya(j,k) = 0;
                        elseif j==Nt2
                            Yb(j,k) = 1/ZG;
                            Ya(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    end
                else
                    Ya(j,k) = 0;
                    Yb(j,k) = 0;
                end
            end
        end
    end
    
% Stringer impedance matrix: Zmu1 for on-line mutual inductances
% including stringer resistance des stringers;
% Zmu2 for opposite-line mutual inductances

    Zmu1 = 1i*w(p)*mu1aa + (R1(p)-1i/w(p)/C1)*Id;
    Zmu2 = 1i*w(p)*mu1ab;
% Yc = characteristic admittance of the MTL  
    Yc = matZ\TV*sqrt(Gam)/TV;
    
    % general equation:
    
    MC = Ep+Em;
    MS = Ep-Em;
    
    m1 = UT\Zmu1/U;
    m2 = UT\Zmu2/U;
    
    OS = (m1+m2)\TV*MC+Yc*TV*MS;
    OD = (m1-m2)\TV*MS+Yc*TV*MC;

    O1 = 1/2*(Ya-Yb)*TV*MC;
    O2 = -1/2*(Yb+Ya)*TV*MS;
    O3 = -1/2*(Yb+Ya)*TV*MC;
    O4 = 1/2*(Ya-Yb)*TV*MS;

    DEL = ((OS-O3)/(OS-O3-O1)*(O2+O4-OD)-O4)\Iin1;
    SIG = (OS-O3-O1)\(O2+O4-OD)*DEL;
    
    
    % node voltages:
    AA = TV*(MC*SIG-MS*DEL)/2;
    BB = TV*(MC*SIG+MS*DEL)/2;
    
    % Source currents SA and SB:
    
    SA = -Ya*AA;
    SB = -Yb*BB;
    
    IGA(p) = sum(SA);
    IGB(p) = sum(SB);
    
    if Kt1==1
        It1(p) = SA(Nt1);
    else
        It1(p) = SB(Nt1);
    end
    if Kt2==1
        It2(p) = SA(Nt2);
    else
        It2(p) = SB(Nt2);
    end
    IC(p) = Iin-IGA(p)-IGB(p);

    % Impedance
    
    Zin(p) = AA(Nf);
end
%% Correction for transmission line:

Rin = real(Zin);
Xin = imag(Zin);
ZConectic
ZinPlasma = Zin2;

