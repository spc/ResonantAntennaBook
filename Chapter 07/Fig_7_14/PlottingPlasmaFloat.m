% PlottingPlasmaFloat.m
% using data created using PlasmaOnComplexImageFLOATING.m
clear, clc
%% plot the impedance spectra:

%% Read experimental data:
S=load('NF_P500W_Zsc_R.txt', '-ascii');
f500W = S(:,1); R500W = S(:,2);
S=load('NF_P1000W_Zsc_R.txt', '-ascii');
f1000W = S(:,1); R1000W = S(:,2);
S=load('NF_P2000W_Zsc_R.txt', '-ascii');
f2000W = S(:,1); R2000W = S(:,2);
S=load('NF_PL_Net_R.txt', '-ascii');
fvac = S(:,1); Rvac = S(:,2);

figure(1)
subplot(221),
plot(f500W,R500W,'-o',f1000W,R1000W,'-*',f2000W,R2000W,'-sq',...
fvac,Rvac/10,'k','Linewidth',2)
ylim([0 60]), xlim([12.5 14]), % Floating
legend('500 W','1500 W', '2000 W','vac x 10','Location','NorthEast',...
    'interpreter','tex','fontsize',16,'fontname','times')
legend BOXOFF
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,...
    'TickLength',[0.02 0.025])
xlabel('frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')
ylabel('real impedance [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
text(12.55,55,'(a) floating, measurement','fontsize',16,'fontname','times')

%% EM complex image model 
%(data from successive runs of PlasmaOnComplexImageFLOATING.m)
load('f')
load('ZinVide')
load('Zin500W')
Zin500W = ZinPlasma;
load('Zin1500W')
Zin1000W = ZinPlasma;
load('Zin2000W')
Zin2000W = ZinPlasma;

subplot(223),
plot(f/1e6,real(Zin500W),f/1e6,real(Zin1000W),f/1e6,real(Zin2000W),...
    f/1e6,real(ZinVide)/10,'k','Linewidth',2)
ylim([0 60]), xlim([12.5 14]), % Floating
legend('500 W','1500 W', '2000 W','vac x 10','Location','NorthEast',...
    'interpreter','tex','fontsize',16,'fontname','times')
legend BOXOFF
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,...
    'TickLength',[0.02 0.025])
xlabel('frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')
ylabel('real impedance [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
text(12.55,55,'(b) floating, MTL model','fontsize',16,'fontname','times')

