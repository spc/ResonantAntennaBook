% MicrostripTest.m
% x axis is height above ground plane (top plate)
% y axis is length along the antenna, parallel to the dielectric interface

%% General constants
pre = 1/(4*pi*eps0*epsFoam); % 1/(4 pi eps0) pre-multiplies for medium

% % Antenna and plasma geometry
aa =  0.00225; % height of leg axes above dielectric foam [m]
ww = dbv+gaine-aa; % glass thickness [m]
b = a2/100; % leg (source) radius [m]
d = H1/100; % leg spacing [m]

% % Source (xs,ys) and observation (xk,yk) co-ordinates
y = [0:1:N-1]*d; % leg axis lateral positions y(i) and y(s)
Y2 = y.*y; % array of all (yk-ys)^2 values, since ys=0, arbitrarily.
% x>w for region validity above dielectric
xk = ww+aa; % observation point: all other legs at same height as source leg

K = (epsFoam-epsGlass)/(epsFoam+epsGlass); % k(Paul)=-K(Silvester). -1<K<1 for eps>0
xs = ww+aa; % INPUT the source general height

% Calculate Maxwell potential coefficient array P(N):
Pm = zeros(1,N); % initialize Green's function (unit charge potential)

    % SOURCE x'=xs. Unit source strength +1
    Pm = - log((xk-xs).^2+Y2);
     if abs(Pm(1))==Inf % precaution if leg is the source itself
         Pm(1) = - log(b^2); % source potential per C/m         
     end
  Pm;
    % PRIMARY IMAGE x'= (2w-xs). Charge strength +K
    xp=(2*ww-xs); % x' is the new charge position
Pm = Pm - K*log((xk-xp).^2+Y2);

    % FOR EACH INFINITE IMAGE. Charge strength -(1-K^2)*K^(n-1)
    % x' = -2(n-1)w-xs
Nterms =20; % number of terms in series
Psum = zeros(Nterms,N); % initialize sum
for n = 1:Nterms
xp = (-xs-2*(n-1)*ww); % xp is the new charge position
Pm = Pm + (1-K^2)*K^(n-1)*log((xk-xp).^2+Y2);
end
Pm = pre*Pm;
PM = toeplitz(Pm,Pm); % (bi)symmetric Toeplitz potential matrix

CM = inv(PM);
Crow = CM(1:6,1); % correction for reference plane at Vrf instead of 0V.

CM=CM/(1+0.1*i); % correction for reference plane at Vrf instead of 0V.