%% Main program PlasmaOnComplexImageFLOATING.m
% EM model using Complex Image for plasma in the floating antenna
clear, clc

% physical constants
q = 1.6e-19;
mu0 = 4*pi*1e-7;
eps0 = 8.85e-12;
me = 9.1e-31;
rhoCu = 16.8e-9;
rhoAl = 27e-9;
epsr = 1;
epsGlass = 7.75; % see Science Data Book, ISBN 0 05 002487 6
% ed. R. M. Tennent, Oliver and Boyd, Edinburgh, 1971:
% "Glass relative permittivity = 5 - 10 at low frequencies"
epsFoam = 1; % 85% porosity

%% Frequency range around mode m=8
finf = 12e6; % [Hz]
fsup = 14e6; % [Hz]
df = 1e4; % [Hz]
f = finf:df:fsup;
w = 2*pi*f;

%% antenna parameters
N = 25; % number of legs
nbt = 2; % number of ground connections

% ground nodes (Kt = 1 for nodes on A, Kt = 2 for nodes on B)
Kt1 = 1;
Nt1 = 10; % ground on A10
Nt2 =16; % ground on A16
Kt2 = 1;

Nf = 13; % RF input node

% Dimensions:
% index 1 for stringers, index 2 for legs
% Attention units in [cm], used for mutual inductance calculations

H1 = 5; % stringer length [cm]
H2 = 120; % leg length [cm]

a1 = 0.6; % stringer effective radius [cm]
a2 = 0.4; % leg radius [cm]

% Dbaseplate = distance to backplate,
Dbaseplate = 5; % [cm!!] (WAS Dscreen)
% Dtop = plasma gap = distance to top plate (closed reactor for plasma)
Dtop = 0.07; % distance to top ground electrode [m!!]

% Capacitance and self inductances:
C1 = 375e-12; % stringer capacitor [F]

L1 = mu0.*(H1/100)./2./pi.*(log(2.*H1./a1)-1); % stringer self-inductance

L2 = mu0.*(H2/100)./2./pi.*(log(2.*H2./a2)-1); % leg self-inductance

% connector impedances
dTL = 0.3; % [m] internal coaxial cable connection for plasma expts.
dconS = 0.05; % stub connector length [m]
aconS = 0.0025; % stub connector radius [m]

% Resistances
skin = sqrt(2.*rhoCu./w./mu0); % skin depth in copper [m]
skinAl = sqrt(2.*rhoAl./w./mu0); % skin depth in aluminium [m]

Surf1 = pi.*((a1/100).^2-((a1/100)-skin).^2);
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2);

% Estimated surface return resistance in aluminium housing: 
% (arbitrarily spread over 10 leg radii)
SurfRet = 10.*a2./100.*skinAl;

% stub connector cross-section area
SurfRcons = pi.*((aconS/100).^2-((aconS/100)-skin).^2); 

Rsd1 = rhoCu.*H1/100./Surf1; % stringer conductor resistance
Rsd2 = rhoCu./Surf2; % leg resistance per unit length
Rsret = rhoAl./SurfRet; % estimated housing resistance per unit length

% stub connectors inductance and resistance:
LconS = mu0.*(dconS)./2./pi.*(log(2.*(dconS)./aconS)-1);
RconS = rhoCu.*dconS./SurfRcons;

% Equivalent Series Resistance for capacitors
ESR = 1.5*(0.001+1e-4*f.^2/1e12); % [ohms]

% Resistive losses for vacuum loading:
R1 = Rsd1 + ESR; % total stringer resistance
R2 = Rsd2.*H2/100; % leg resistance

%% Plasma power scan, density data for floating antenna
% line average density calibrated with microwave interferometer;
% density profile measured with Langmuir probe
ne500 =7e15; % 500 W, electron density [1/m3] FLOATING, RF=A13.
ne = ne500; % 500 W, electron density [1/m3] FLOATING, RF=A13.
%ne =ne500*1.47; % 1000 W, electron density [1/m3] FLOATING, RF=A13.
%ne =ne500*1.7; % 1000 W, electron density [1/m3] FLOATING, RF=A13.
%ne =ne500*3.02; % 2000 W, electron density [1/m3] FLOATING, RF=A13.

%% plasma parameters
p = 1.5; % pressure [Pa]
num = 2.6e7*p; % collision frequency nu_m [1/s]  = 2.6e7 * p[Pa]
Te = 4; % electron temperature [V]
V0 = 25; % assumed DC sheath potential [V]
LamD = sqrt(eps0*Te/q/ne); % Debye length [m]
gaine = sqrt(2)./3.*LamD.*(2.*V0./Te).^(3./4); %  estimate sheath width [m]
% sheath is 0.5mm for ne=8e15 1/m3, to 0.3mm for ne=3*8e15 1/m3

dbv = 0.01; % [m] 4mm copper radius + 3mm glass beads + 3 mm glass tile
% total distance to plasma is dbv (fixed) + gaine (variable)

%% Enable/disable parameters:
% facG multiplies the ground stub impedance ZG = Rcons+j*w*Lcons:
% for GROUNDED antenna, facG = 1 (no change to the stub impedance)
% for FLOATING antenna, facG >> 1 ("infinite" impedance, open circuit)
% facG therefore determines whether floating or grounded
% (see ZG in InitZin5.m)
facG = 1e8; % impedance factor for stubs to ground >>1 for FLOATING antenna
facmu1 =1; % facmu1=1 for stringer mutuals
facmu2 = 1; % facmu2=1 for leg mutuals

%% Calculation for vacuum (without plasma)

InitZin5Stripline % stripline for legs; partial inductances for stringers

%% Calculation with plasma

PlasmaZin5

%% intermediate plot impedance spectra

% Read experimental data (floating antenna):
S=load('NF_P500W_Zsc_R.txt', '-ascii');
f500W = S(:,1); R500W = S(:,2);
S=load('NF_P1000W_Zsc_R.txt', '-ascii');
f1000W = S(:,1); R1000W = S(:,2);
S=load('NF_P2000W_Zsc_R.txt', '-ascii');
f2000W = S(:,1); R2000W = S(:,2);
S=load('NF_PL_Net_R.txt', '-ascii');
fvac = S(:,1); Rvac = S(:,2);

figure(2)
subplot(211),
plot(f500W,R500W,'-o',f1000W,R1000W,'-*',...
    f2000W,R2000W,'-sq',fvac,Rvac/10,'-+')
ylim([0 80]), xlim([12 14]),
legend('500 W','1000 W','2000 W','vac/10')
hold on

subplot(212),
plot(f/1e6,real(ZinPlasma),f/1e6,real(ZinVide)/10)
ylim([0 80])
xlim([12 14])
legend('plasma','vacuum/10')
hold on

%save('f','f');
% run for vacuum using InitZin5Stripline.m
%save('ZinVide','ZinVide')
% repeat for density at 500W, 1500W, 2000W, using PlasmaZin5:
%save('Zin1500W','ZinPlasma') % PROTECT OLD FILES FROM NEW CHANGES

% run PlottingPlasmaFloat.m to plot Fig. 7.15

%select one frequency % not used here
% onefrequPlasma5 % not used here
