% ToeplitzVacStripline.m
% x axis is height above ground plane (top plate)
% y axis is length along the antenna, parallel to the dielectric interface

% % General constants
pre = 1/(4*pi*eps0); % 1/(4 pi eps0) pre-multiplies for vacuum valid zone

% % Antenna geometry
a = Dbaseplate/100; % height of leg axes above baseplate [m]
b = a2/100; % leg (source) radius [m]
d = H1/100; % leg spacing [m] (stringer length)
h = Dbaseplate/100 + Dtop; % total gap between the groundplanes [m]
% ignore dbv because of high glass epsilon

% % Source (xs,ys) and observation (xk,yk) co-ordinates
y = [0:1:N-1]*d; % leg axis lateral positions y(i) and y(s)
Y2 = y.*y; % array of all (yk-ys)^2 values, since ys=0, arbitrarily.

xk = a; % observation point: all other legs at same height as source leg
xs = a; % the source general height
Q=1; % potential matrix is for unit charge per unit length.

% Calculate Maxwell potential coefficient array P(N):
% (this method not used here - see Kammler expression below)
P = zeros(1,N); % initialize Green's function (unit charge potential)
Nreflns = 500; % was 10000;

for n=0:Nreflns
    % SOURCE x'=xs-2hn. Unit source strength +1
    xps = xs-2*h*n;
    if n==0 %precaution if leg is the source itself
        Y2(1)=b^2; % because b^2 must affect ONLY Y2(1)=0!!!
    P = P - log(Y2); 
        Y2(1) = 0; % reset Y2(1) for all other calculations
    else
    P = P - log((xk-xps).^2+Y2); 
    end
    
    % TOP PLATE IMAGE x'=-xs-2hn, opposite charge
     xpi = -xs-2*h*n;
     P = P + log((xk-xpi).^2+Y2);
     
    % Source Reflection in baseplate is at xr = (2h-x'); opposite charge
    xrs = 2*h-xps;
    P = P + log((xk-xrs).^2+Y2); 
    
    % Image Reflection in baseplate is at xr = (2h-x'); same charge
    xri = 2*h-xpi;
    P = P - log((xk-xri).^2+Y2); 
end

PM = pre*Q*toeplitz(P,P); % (bi)symmetric Toeplitz potential matrix
CM=inv(PM); % capacitance matrix per unit length NOT USED HERE

%figure(1)
%subplot(221) % plot potential matrix P:
%plot(y,PM,'-*',y,y*0), axis tight
%ylabel('leg potential [V/C/m]')
%xlabel('antenna leg position [m]')
%title('stripline in vacuum')

% INFINITE SUM GREEN'S FUNCTION METHOD
% D W Kammler 1968 IEEE Trans. Microw. Theory Tech. MTT-16 925
Pt = zeros(N); % initialize
y = [0:1:N-1]*d; % we account for leg radius inside the k,j loop
for k=1:N % source position, xs
for j=1:N % other (floating, uncharged) leg positions, xi
    if k==j % if leg is the source itself
        sh1 = sinh(0.5*pi.*b./h); % wire radius b, avoid singularity
    else
        sh1 = sinh(0.5*pi.*(y(j)-y(k))./h);
    end
s2 = sin(0.5*pi.*(xk-xs)./h);
sh3 = sh1;
s4 = sin(0.5*pi.*(xk+xs)./h);
Pt(k,j) = -pre*log((sh1.^2+s2.^2)./(sh3.^2+s4.^2));
end
end
Pt;
C=inv(Pt); % capacitance matrix per unit length from closed expression
% USED THIS Kammler method for InitZin5Stripline.m

%subplot(222), plot(y,Pt,'-sq',y,y*0), axis tight
%ylabel('leg potential [V/C/m]')
%xlabel('antenna leg position [m]')
%title('Kammler limit')