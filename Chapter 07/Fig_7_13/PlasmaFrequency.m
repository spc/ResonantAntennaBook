%% PlasmaFrequency.m
clear
clc

% physical constants
q = 1.6e-19;
eps0 = 8.85e-12;
me = 9.1e-31;

ne = logspace(log10(1e11),log10(1e17),100);
wpe = sqrt(ne.*q^2./(eps0.*me));

f = 13.56e6;
w = 2*pi*f;

loglog(ne,wpe,[1e11 1e17],[13.56e6 13.56e6],...
    [1e11 1e17],10*[13.56e6 13.56e6])
legend('wpe','wRF','10*wRF')
xlabel('ne [m-3]'), ylabel('angular frequency [rad/s]')


