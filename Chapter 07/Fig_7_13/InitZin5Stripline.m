% InitZin5Stripline.m
%% Calculate the matrices
% Define the RF input current array
Iin = 1; % unit input RF current

for k = 1:N
    
    if k==Nf
        
        Iin1(k) = Iin;
        
    else
        Iin1(k) = 0;
        
    end
end

[DD,VV]=size(Iin1);
if DD==1
    Iin1=Iin1';
else
end
%% Definition of Matrices Id, U, UT

for k = 1:N
    
    for j = 1:N
        
        if j==k;
            
            Id(k,j) = 1;
            
        else
            Id(k,j) = 0;
        end
    end
end

for k = 1:N
    
    for j = 1:N
        
        if j==k-1
            
            
            S1(k,j) = 1;
        else
            S1(k,j) = 0;
        end
    end
end


U = Id-S1;
UT = transpose(U);

%% Calculate inductance matrices
LEnds = 1i*1e4; % high inductance ("open circuit") at the end nodes

CalcMatMutToeplitz  % mutual inductances between stringers and legs
CalcMatScreenToeplitz % keep for baseplate image sringers

ToeplitzVacStripline % stripline p.u.l. cap. matrix for legs
% all leg (mu2) calculations are replaced by stripline (closed reactor)
% stripline inductance matrix calculated from 
% stripline capacitance matrix in vacuum (homogeneous dielectric)
matCR = C; % series % C; % Kammler
matCX = 0*Id;
matLR = mu0*eps0*epsr*Id/matCR; % includes cap. co. to all images
matLX = 0*Id;

% Definition of per unit length matrices for the legs

%matL2 = mu2/H2*100;
%VacLmatrix = matL2; % store vacuum inductance p.u.l. matrix
%matLR = real(matL2);
%matLX = imag(matL2);
%matCR = mu0*eps0*epsr*Id/matLR; % includes cap. co. to all images
%VacCmatrix = matCR; % store vacuum capacitance p.u.l. matrix
%matCX = 0*Id;

% (capacitive coupling to stringers is ignored)
%% Calculate impedance as a function of frequency

p = find(w);

disp('calculating without plasma')
disp(' ')
tN0 = 10;
Lfr = length(f);
for p = min(p):max(p) % loop through the frequency range
    
    testN0 = p/Lfr;
    
    if testN0*100>=tN0
        disp([num2str(tN0) ' %']) % counter for calculation progress
        tN0 = tN0+10;
    else
    end
    
    % eigenvalue solution for long legs only (stringers are short)
    
    % matZ and matY = matrices Z and Y for the multiline analysis 
    matZ = 1i*w(p)*matLR-w(p)*matLX+(Rsd2(p)+Rsret(p))*Id;
    matY = 1i*w(p)*matCR-w(p)*matCX;
    
    ZY = matZ*matY;
    [TV,Gam] = eig(ZY);
    Mlam = Id/sqrt(Gam);
    
    for k =1:N
        for n = 1:N
            if n==k
                Ep(n,k) = exp(sqrt(Gam(n,k))*H2/200);
                Em(n,k) = exp(-sqrt(Gam(n,k))*H2/200);
                
            else
                Ep(n,k) = 0;
                Em(n,k) = 0;
                
            end
        end
    end
    
    % matrices YA et YB (grounding of nodes A et B),
    % ZG is the impedance of a stub connector    
    ZG = facG*(RconS(p)+1i*w(p)*LconS);
    
    if nbt==1
        for k = 1:N
            
            for j = 1:N
                if j==k
                    if Kt1==1
                        
                        if j==Nt1
                            Ya(j,k) = 1/ZG;
                            Yb(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    else
                        if j==Nt1
                            Yb(j,k) = 1/ZG;
                            Ya(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    end
                else
                    Ya(j,k) = 0;
                    Yb(j,k) = 0;
                end
            end
        end
    else
        for k = 1:N
            
            for j = 1:N
                if j==k
                    if Kt1==1
                        
                        if j==Nt1
                            Ya(j,k) = 1/ZG;
                            Yb(j,k) = 0;
                        elseif j==Nt2
                            Ya(j,k) = 1/ZG;
                            Yb(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    else
                        if j==Nt1
                            Yb(j,k) = 1/ZG;
                            Ya(j,k) = 0;
                        elseif j==Nt2
                            Yb(j,k) = 1/ZG;
                            Ya(j,k) = 0;
                        else
                            Ya(j,k) = 0;
                            Yb(j,k) = 0;
                        end
                    end
                else
                    Ya(j,k) = 0;
                    Yb(j,k) = 0;
                end
            end
        end
    end
    
%   Matrices stringer impedance: Zmu1 = in-line mutuals
%   plus resistance of the stringers;
%   Zmu2 = opposite stringer mutuals

    Zmu1 = 1i*w(p)*mu1aa + (R1(p)-1i/w(p)/C1)*Id;
    Zmu2 = 1i*w(p)*mu1ab;
% Yc = characteristic admittance of the multiline  
    Yc = matZ\TV*sqrt(Gam)/TV;
    
    % general equation
    
    MC = Ep+Em;
    MS = Ep-Em;
    
    m1 = UT\Zmu1/U;
    m2 = UT\Zmu2/U;
    
    OS = (m1+m2)\TV*MC+Yc*TV*MS;
    OD = (m1-m2)\TV*MS+Yc*TV*MC;

    O1 = 1/2*(Ya-Yb)*TV*MC;
    O2 = -1/2*(Yb+Ya)*TV*MS;
    O3 = -1/2*(Yb+Ya)*TV*MC;
    O4 = 1/2*(Ya-Yb)*TV*MS;

    DEL = ((OS-O3)/(OS-O3-O1)*(O2+O4-OD)-O4)\Iin1;
    SIG = (OS-O3-O1)\(O2+O4-OD)*DEL;
    
    
    % node voltages:
    AA = TV*(MC*SIG-MS*DEL)/2;
    BB = TV*(MC*SIG+MS*DEL)/2;
    
    % Total ground currents through grounding
    %points IGA and IGB, currents for each grounding connection It1 and It2,
    %capacitive ground current IC
    
    SA = -Ya*AA;
    SB = -Yb*BB;
    
    IGA(p) = sum(SA);
    IGB(p) = sum(SB);
    
    if Kt1==1
        It1(p) = SA(Nt1);
    else
        It1(p) = SB(Nt1);
    end
    if Kt2==1
        It2(p) = SA(Nt2);
    else
        It2(p) = SB(Nt2);
    end
    IC(p) = Iin-IGA(p)-IGB(p);

    % Impedance
    
    Zin(p) = AA(Nf);
    
end

%% Impedance transformation due to transmission line (length=dTL)
Rin = real(Zin);
Xin = imag(Zin);
ZConectic % calculate impedance transformation due to transmission line
ZinVide = Zin2;



