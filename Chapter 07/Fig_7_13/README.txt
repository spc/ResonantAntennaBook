README.txt for book Fig. 7.13

1) Run  PlasmaOnGrounded_500W.m, PlasmaOnGrounded_1000W.m,
PlasmaOnGrounded_1500W.m, PlasmaOnGrounded_2000W.m,
to create the corresponding data files for the power scan
500 W, 1000 W, 1500 W, and 2000 W.

2) Run  PlottingPlasmaGround.m  to plot book Fig. 7.13.