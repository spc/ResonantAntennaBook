% PlottingVacGround.m
% measured and modeled real impedance spectra 
% for grounded antenna in vacuum
% using data created using PlasmaOffComplexImageVacGround.m
clear, clc

%% plot the impedance spectra:
load('f','f'); % model frequency
load('ZinVide','ZinVide'); % model input impedance for vacuum
load('fnet','fnet'); % network analyser frequency measurement
load('Rnet','Rnet'); % network analyser real impedance measurement
figure(1)
clf
loglog(fnet/1e6,Rnet,'r','LineWidth',1) % log/log plot measurement
axis([6 40 1e-2 1e4]), hold on

loglog(f/1e6,real(ZinVide),'k','LineWidth',1) % log/log plot model
set(gca,'fontname','times','fontsize',16,'LineWidth',1,...
    'TickLength',[0.02 0.025])
xlabel('frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')
ylabel('real impedance [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
text(31.1,9.2,'MTL','fontsize',16,'fontname','times')
text(30.9,4,'model','fontsize',16,'fontname','times')
text(23,0.025,'measurement','fontsize',16,'fontname','times','color','red')
text(19,4000,'grounded antenna','fontsize',16,'fontname','times')
text(19,1500,'(no plasma)','fontsize',16,'fontname','times')
v=[1e-2 1e4];
mark=13*[1 1];
plot(mark,v,'k--','MarkerSize',8,'LineWidth',1.5), hold on
text(10.3,0.1,'mode  \it{m}\rm{ = 8}','fontsize',16,'fontname','times')
text(11.5,0.03,'13  MHz','fontsize',16,'fontname','times')
text(5.9,0.0047,'6','fontsize',16,'fontname','times')
text(27,55,'\it{m}\rm{ = 2}','fontsize',16,'fontname','times')
text(19.2,430,'4','fontsize',16,'fontname','times')
text(15.4,2100,'6','fontsize',16,'fontname','times')
text(10.9,2800,'10','fontsize',16,'fontname','times')
text(9.8,1000,'12','fontsize',16,'fontname','times')
text(9.1,100,'14','fontsize',16,'fontname','times')



