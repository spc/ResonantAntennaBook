% PlaneDispersionRAID.m % exact for cold, collisionless plasma with ions
clc, clf, clear
%% constants
c = 2.99e8; % speed of light [m/s]
q = 1.602e-19; % electron charge [C]
m = 9.109e-31; % electron mass [kg]
mp = 1.67e-27; % proton mass [kg]
MAr = 40*mp; % argon ion mass [kg]
MH2 = 2*mp; % hydrogen ion mass assuming H2+ [kg]
M = 40*mp; % ion mass for H2+
eps0 = 8.85e-12; % vacuum permittivity [m-3 kg-1 s4 A2] [F/m?]
%% experiment parameters
B0 = 200e-4; % [T]
ne = 1e18; % electron density [m-3]
fRF= 13.56e6; % excitation RF frequency [Hz]
%% important (angular) frequencies
wRF = 2*pi*fRF; %
wce = q*B0/m; % electron cyclotron frequency [rad/s]
wci = q*B0/M; % ion cyclotron frequency [rad/s]
wpe = sqrt(ne.*q^2./(eps0*m)); % electron plasma frequency
wpi = sqrt(ne.*q^2./(eps0*M)); % ion plasma frequency
wp = sqrt(wpe.^2+wpi.^2); % (exact) plasma frequency
%% CUTOFF frequencies Lieberman94 Table 4.2 p117
%% O wave cutoff
wcoO = wp % O wave cutoff in, exact, Lieberman Table 4.2
%% R wave and X wave cutoff
wcoR = ((wce-wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2 % exact Lieberman 4.5.17
%% L wave and X wave cutoff
wcoL = ((-wce+wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2 % exact Lieberman 4.5.18
%% RESONANCE frequencies Lieberman94 Table 4.2 p117 
wresR = wce % R wave resonance exact Lieberman 4.5.17
wresL = wci % L wave resonance exact Lieberman 4.5.18
B=-(wce^2+wci^2+wpe^2+wpi^2); % set up the quadratic in w^2
C=wpe^2*wci^2+wpi^2*wce^2+wce^2*wci^2;
wUH=sqrt((-B+sqrt(B^2-4*C))/2) % upper hybrid resonance exact
wLH=sqrt((-B-sqrt(B^2-4*C))/2) % lower hybrid resonance exact
%% DISPERSION RELATIONS k=k(w)
minw = 1e4; % frequency range [rad/s]
maxw = 2e11;
w = logspace(log10(minw),log10(maxw),10000); % frequency axis (for RAID)
k0 = w./c;
kmax = 300; % [1/m] arbitrary wavenumber axis maximum
%% O wave
kO = sqrt(k0.^2-wp.^2/c^2); % w2 = wp2 + k2c2; k2=k02-wp2/c2 exact
%% R wave
kR = k0.*sqrt(1 - wpe.^2./(w.*(w-wce))- wpi.^2./(w.*(w+wci))); % Lieberman 4.5.16 exact
%% L wave
kL = k0.*sqrt(1 - wpe.^2./(w.*(w+wce))- wpi.^2./(w.*(w-wci))); % Lieberman 4.5.16 exact
%% X wave Lieberman 4.5.19
kX = sqrt(kR.^2.*kL.^2./(1 - wpe.^2./(w.^2-wce.^2)- wpi.^2./(w.^2-wci.^2)))./k0; % exact
%% PLOT 4 linear dispersion relations. See Lieberman Fig. 4.10
figure(1), clf
%% O wave
semilogy(kO,w,'y-','linewidth',2), hold on % exact dispersion relation
semilogy(k0,c*k0,'k--') % plot w/k = c
%% R wave
semilogy(kR,w,'m-','linewidth',2), % exact dispersion relation
%% L wave
semilogy(kL,w,'c-','linewidth',2), % exact dispersion relation
%% X wave
semilogy(kX,w,'g-','linewidth',2), % exact dispersion relation
% cut offs same as wR, wL
%% plot 3 cutoffs
semilogy([0,kmax/20],[wp,wp],'k-','linewidth',3), % O cutoff
text(kmax/20,wp,'wp') % O cutoff
semilogy([0,kmax/20],[wcoR,wcoR],'k-','linewidth',3), % R cutoff
text(kmax/20,wcoR,'wR') % R cutoff
semilogy([0,kmax/20],[wcoL,wcoL],'k-','linewidth',3), % L cutoff
text(kmax/20,wcoL,'wL') % L cutoff
%% plot 4 resonances
semilogy([0,kmax],[wce,wce],'k--','linewidth',2), % R resonance
text(kmax,wce,'wce') % R resonance
semilogy([0,kmax],[wci,wci],'k--','linewidth',2), % L resonance
text(kmax,wci,'wci') % L resonance
semilogy([0,kmax],[wUH,wUH],'k--','linewidth',2),
text(kmax,wUH,'wUH') % X resonance at upper hybrid frequency
semilogy([0,kmax],[wLH,wLH],'k--','linewidth',2),
text(kmax,wLH,'wLH') % X resonance at lower hybrid frequency
%% RF frequency marker
semilogy([0,kmax/10],[wRF,wRF],'.-','linewidth',3),
text(kmax/10,wRF,'13.56 MHz')
%% graphics
xlabel('k [1/m]'), ylabel('w [rad/s]')
title('semilogy plot of all 4 dispersion relations in Ar')
xlim([.1,kmax]), ylim([minw,maxw])
legend('O wave','w=ck','R whistler','L wave','X wave')

%% PLANE DISPERSION CURVE ADDED TO FIG. 16 COMPARISON
%% Density scan for k(ne) at 13.56 MHz Fig. 16
% Lieberman 4.5.16(a) exact for whistler (R) wave
k0 = wRF./c; %wciH2, wciAr and wce are constants at B0 = 200 Gauss
n0Scan = [1:7]*1e18; % peak electron density experimental scan [m-3]
neScan = 0.5*n0Scan; % GUESS some average electron density in the column
wpe = sqrt(neScan.*q^2./(eps0*m)); % electron plasma frequency
wpiH2 = sqrt(neScan.*q^2./(eps0*MH2)); % H2 ion plasma frequency
wpiAr = sqrt(neScan.*q^2./(eps0*MAr)); % Ar ion plasma frequency
wciH2 = q*B0/MH2; % H2 ion cyclotron frequency [rad/s]
wciAr = q*B0/MAr; % Ar ion cyclotron frequency [rad/s]
kR13MHzH2 = k0.*sqrt(1 - wpe.^2./(wRF.*(wRF-wce))- ...
    wpiH2.^2./(wRF.*(wRF+wciH2)));
kR13MHzAr = k0.*sqrt(1 - wpe.^2./(wRF.*(wRF-wce))- ...
    wpiAr.^2./(wRF.*(wRF+wciAr)));
kR13MHz = k0.*sqrt(1 - wpe.^2./(wRF.*(wRF-wce)));
lambdaz13MHzH2 = 100*2*pi./kR13MHzH2; % axial wavelength in cm
lambdaz13MHzAr = 100*2*pi./kR13MHzAr; % axial wavelength in cm
lambdaz13MHz = 100*2*pi./kR13MHz; % axial wavelength in cm
% for comparison with FigComparison:
%% Figure 16 from BdotFigures.m
figure(16), clf
load('LambdaH2.txt') % first, the experimental data H2
n0H2 = LambdaH2(:,1);
Errn0H2 = LambdaH2(:,2);
LambdaH = LambdaH2(:,3);
ErrLambdaH2 = LambdaH2(:,4);

load('LambdaAr.txt')% first, the experimental data Ar
n0Ar = LambdaAr(:,1);
Errn0Ar = LambdaAr(:,4);
LambAr = LambdaAr(:,2);
ErrLambdaAr = LambdaAr(:,3);
% see help: errorbar(x,y,yneg,ypos,xneg,xpos,'o')
errorbar(n0H2,LambdaH,ErrLambdaH2,ErrLambdaH2,Errn0H2,Errn0H2,...
    'ksq','linewidth',1,'MarkerSize',11,'MarkerFaceColor','w'), hold on
errorbar(n0Ar,LambAr,ErrLambdaAr,ErrLambdaAr,Errn0Ar,Errn0Ar,...
    'k^','linewidth',1,'MarkerSize',9,'MarkerFaceColor','w'),

% ADD PLANE WAVE DISPERSION PLOT second
plot(n0Scan/1e18,lambdaz13MHz,'m^-','linewidth',1,'MarkerSize',8,...
    'MarkerFaceColor','m')

% now the analytical and Comsol profiles:
load('SemiAnalytic.txt'); % third, the helicon 2nd order semi-analytical 
nAn = SemiAnalytic(:,1);
LamAn = SemiAnalytic(:,2);
plot(nAn,LamAn,'ro-','linewidth',1,'MarkerSize',7,'MarkerFaceColor','r'), hold on


load('COMSOLwavelengths.txt'); % fourth, NEW 4th order H,LTG,HTG
% data estimated from Fig 40, "Helicon modes.docx"
n4thOrder = [1.15    1.395    1.93    2.24    2.74    2.93...
    3.95    4.79    5.47 6.31    6.955];
Lam4thOrder = [28.5   26   21.68   19.9   17.9   17.1   14.51   13.14...
    12.0 11.11   10.28];
plot(n4thOrder,Lam4thOrder,'ks-','linewidth',1,'MarkerSize',10,'MarkerFaceColor','g')


load('COMSOLwavelengths.txt'); % fifth, the numerical simulation
nCom = COMSOLwavelengths(:,1);
LamCom = COMSOLwavelengths(:,2);
plot(nCom,LamCom,'b*-','linewidth',1,'MarkerSize',12,'MarkerFaceColor','b')

xlim([0 8]), ylim([9 36]),
xlabel('electron density  \itn\rm_0  [10^{18} m^{-3} ]')
ylabel('axial wavelength  \lambda\it_z \rm[cm]')
%legend('hydrogen data','argon data','\itk\rm_{1,1} eigenvalue',...
%    'numerical simulation','classical dispersion H2',...
%   'classical dispersion Ar','location','northeast')
legend('hydrogen data','argon data','R plane wave',...
  '\itk\rm{(1,1)} 2nd order','\itH\rm{(1,1)} 4th order','numerical simulation','location','northeast')
set(gca,'fontsize',14,'LineWidth',1.0,...
    'TickLength',[0.015 0.025])

%% PLANE WAVE DISPERSION CURVE ADDED TO FIG. 17 B0 scan COMPARISON
%% B0 scan for k(B0) at 13.56 MHz Fig. 17
% Lieberman 4.5.16(a) exact for whistler (R) wave
k0 = wRF./c; %wciH2, wciAr and wce are constants at B0 = 200 Gauss
% the density varied at the same time (see Fig. 8)
load('n0versusB0.txt')
B0scan = n0versusB0(:,1)*1e-4; % B0 [T] experimental scan
B0scan = B0scan(1:6); % limit scan to 650 Gauss
n0scan = n0versusB0(:,2)*1e18; % ne [m-3] experimental scan 
n0scan = n0scan(1:6);
neAv = 0.5*n0scan; % GUESS.
wpe = sqrt(neAv.*q^2./(eps0*m)); % electron plasma frequency
wpiAr = sqrt(neAv.*q^2./(eps0*MAr)); % Ar ion plasma frequency
wce = q*B0scan/m; % electron cyclotron frequency [rad/s]
wciAr = q*B0scan/MAr; % Ar ion cyclotron frequency [rad/s]
%kR13MHzArB0 = k0.*sqrt(1 - wpe.^2./(wRF.*(wRF-wce))- ...
%    wpiAr.^2./(wRF.*(wRF+wciAr)));
kR13MHzB0 = k0.*sqrt(1 - wpe.^2./(wRF.*(wRF-wce)));
%lambdaz13MHzArB0 = 100*2*pi./kR13MHzArB0; % axial wavelength in cm
lambdaz13MHzB0 = 100*2*pi./kR13MHzB0; % axial wavelength in cm
% for comparison with FigComparison:
%% Figure 17 from BdotFigures.m
figure(17), clf
load('LambdaArB0.txt')
B0 = LambdaArB0(:,1);
LamdaMes = LambdaArB0(:,2);
ErrLambda = LambdaArB0(:,3);
LambdaAnalytic = LambdaArB0(:,4); % SWAPPED?
LambdaComsol = LambdaArB0(:,5); % SWAPPED?
errorbar(B0,1e2*LamdaMes,ErrLambda,'kv-','linewidth',1,'MarkerSize',7,...
    'MarkerFaceColor','w'), hold on
% ADD CLASSICAL DISPERSION PLOT
plot(B0scan*1e4,lambdaz13MHzB0,'k^-','linewidth',1,'MarkerSize',8,...
    'MarkerFaceColor','m')
plot(B0,1e2*LambdaComsol,'ro-','linewidth',1,'MarkerSize',7,...
    'MarkerFaceColor','r')
plot(B0,1e2*LambdaAnalytic,'b*-','linewidth',1,'MarkerSize',10,...
    'MarkerFaceColor','b')

xlim([100 700]), ylim([10 25]),
xlabel('magnetic field  \itB\rm_0 [Gauss]')
ylabel('axial wavelength  \lambda\it_z \rm[cm]')
%legend('argon data','\itk\rm_{1,1} eigenvalue',...
%    'numerical simulation','classical dispersion Ar','location','southeast')
legend('argon data','plane wave dispersion',...
    '\itk\rm_{1,1} eigenvalue','numerical simulation','location','southeast')
set(gca,'fontsize',14,'LineWidth',1.0,...
    'TickLength',[0.015 0.025])

%% Check limits for negligible displacement current
wRF = 2*pi*fRF; % from above
wce = q*B0/m; % electron cyclotron frequency [rad/s] from above
wpe = sqrt(ne.*q^2./(eps0*m)); % electron plasma frequency; from above
nParallel = (wRF^2*eps0*m)/q^2;
B0 = 200e-4; 
nPerpendicular = eps0*B0^2/m;



