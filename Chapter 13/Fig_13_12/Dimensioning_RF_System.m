% Dimensioning_RF_System.m
clear
clc

%% physical constants
eps0 = 8.85e-12;
mu0 = 4*pi*1e-7;

%% operating frequency
f = 13.56e6;
w = 2*pi*f;

%% Coaxial line Characteristics:
% D-->length, epsr-->dieletric perm., sigD-->dielectric cond.,
% sigm-->conductor cond., 
% a-->inner conductor radius (r_int in Fig. 13.13),
% b--> outer conductor radius (r_ext in Fig. 13.13), 
% Z0r--> lossless cable characteristic impedance, 

D = 0.1;

epsr = 2.02;
sigD = 1e-20;
sigm = 5.8e7;

a = 1e-3;

% If you want to define the cable by a characteristic impedance Z0r and
% compute the required outer radius b, set defcable=1 below. 
% If not set to 1,
% the cable is defined by its outer radius b and Z0r is computed.

% Note that here we define the lossless characteristic impedance Z0r. The
% lossy characteristic impedance Z0 is defined once the line per meter
% electrical parameters are computed.

defcable = 1;
if defcable==1
    Z0r = 50;
    b = a.*exp(2.*pi.*Z0r.*sqrt(eps0.*epsr./mu0)); % (13.13)
else
    b = 0.004;
    Z0r = 1./2./pi.*sqrt(mu0./eps0./epsr).*log(b./a); % (13.13)
end

delta = sqrt(2./w./mu0./sigm);

% line per meter electrical parameters (13.12)
Rline = 1./sigm./delta.*(1./2./pi./a + 1./2./pi./b);
Lline = mu0./2./pi.*log(b./a);
Cline = 2.*pi.*eps0.*epsr./log(b./a);
Gline = 2.*pi.*sigD./log(b./a);

% lossy cable impedance
Z0 = sqrt((Rline+1i.*w.*Lline)./(Gline+1i.*w.*Cline)); % (13.11)
% cable propagation factor
gam = sqrt((Rline+1i.*w.*Lline).*(Gline+1i.*w.*Cline));


%% Generator ouput impedance 
Rg = 50;

%% Definition of the Load impedance

% Load impedance
Rload = [20 25 20];
Xload = [-10 0 10];
Zant = Rload+1i.*Xload;

%% Impedance at the coaxial line input (Zload transformed by the cable)
Zin = Z0.*(Zant+Z0.*tanh(gam.*D))./(Z0+Zant.*tanh(gam.*D)); % (13.15)
Rin = real(Zin);
Xin = imag(Zin);

%% RF power : Power P and Zant must have the same size/length: here we in fact define a relation Zant(P)
P = [100 500 1000];
% Current and Voltage at RF generator output
Ig = sqrt(P./Rg);
Vg = sqrt(P.*Rg);

%% T Matching (lossless)
Rmax = 100;
bm = -1.1*sqrt(Rg.*Rmax);
am = (-bm.*Rin-sqrt(Rin.*Rg.*(bm.^2-Rin.*Rg)))./Rin; % (13.16)
cm = (-Rg.*(bm+Xin)-sqrt(Rin.*Rg.*(bm.^2-Rin.*Rg)))./Rg; % (13.16)

xam = 1i.*am;
xbm = 1i.*bm;
xcm = 1i.*cm;

% current and voltage at the coaxial line input
Vin = ((Rg-xam)-xcm.*(1-(Rg-xam)./xbm)).*Ig; % (13.18) re-arranged
Iin = (1-(Rg-xam)./xbm).*Ig; % (13.18)
% current and voltage in matching element (b)
Ibm = Ig - Iin;
Vbm = Ibm.*xbm;

% forward and backward components of the coaxial line standing wave
Vp = (Vin+Z0.*Iin)./2; % (13.21)
Vm = (Vin-Z0.*Iin)./2; % (13.21)

% current and voltage at the load input
Vant = Vp.*exp(-gam.*D)+Vm.*exp(gam.*D); % (13.14)
Iant = 1./Z0.*(Vp.*exp(-gam.*D)-Vm.*exp(gam.*D)); % (13.14)

% power at the cable input
Pin = real(Vin.*conj(Iin)); % (13.22)
% power at the load input
Pant = real(Vant.*conj(Iant)); % (13.22)
% Dissipated power into cable
Pdiss = Pin-Pant;

figure(1)
clf
subplot(2,1,1)
plot(P,abs(Ig),'ks-','linewidth',2)
hold
plot(P,abs(Ibm),'vr-','linewidth',2)
plot(P,abs(Iin),'vb-','linewidth',2)
legend('I_{a}','I_{b}','I_{c}','location','northwest')
xlabel('P_g [W]')
ylabel('[A]')
grid
title('Currents and potentials in matching elements a,b,c')
subplot(2,1,2)
plot(P,abs(Vg),'ks-','linewidth',2)
hold
plot(P,abs(Vbm),'vr-','linewidth',2)
plot(P,abs(Vin),'vb-','linewidth',2)
legend('V_{a}','V_{b}','V_{c}','location','northwest')
xlabel('P_g [W]')
ylabel('[V]')
grid

figure(2)
clf
subplot(2,1,1)
plot(P,Pant,'k-s','linewidth',2)
hold
plot(P,Pin,'r-s','linewidth',2)
title('Powers at coax. and load inputs')
xlabel('P_g [W]')
ylabel('[W]')
grid
legend('P_{ant}','P_{in}')

subplot(2,1,2)
plot(P,Pdiss,'k-s','linewidth',2)
title('Power dissipated into coax.')
xlabel('P_g [W]')
ylabel('[W]')
grid
legend('P_{diss}')


