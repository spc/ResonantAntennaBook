The calculations in  Dimensioning_RF_System.m  correspond to book Fig. 13.12
and sections 13.3.1 to 13.3.3.

The equation numbers are referenced, corresponding to the book.