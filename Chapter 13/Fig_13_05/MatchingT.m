% MatchingT.m
clear
clc


%% Operating frequency f[Hz]
f = 13.56e6;
w = 2.*pi.*f;


%% Definition of the load to be matched: Zant = R + 1i*X
% Here we define an impedance path to be be match, during an hypothetical
% power ramp. Each complex value of Zant corresponds to a given value
% of RF input power Pin.

R = [11 12 35 51 66 82 95 112 114 105 90 65 42 25];
X = [-81 -82 -86 -87 -82 -75 -68 -40 -20 8 22 35 36 28];
%% Figure 3 for the representative impedance path
figure(3)
plot(R,X,'b-o','linewidth',2)
xlabel('R')
ylabel('X')

Zant = R+1i*X;

%% Figure 5 for the arbitrary dependence of plasma density on RF power
ne = [1e16:2e16:13e16 1.7e17 2e17 2.5e17 3e17 4e17 6e17 1e18];

ne1 = 2e17;
P1 = 200;
ne2 = 1e18;
P2 = 2500;
ne3 = 1;
P3 = 1;

Pin = (ne3.*(P1 - P2).*(ne3 - ne).*ne + ne2.^2.*(ne3.*P1 - ne1.*P3 - P1.*ne + P3.*ne) -...
ne1.^2.*(ne3.*P2 + (-P2 + P3).*ne) + ne2.*((-ne3.^2).*P1 + ne1.^2.*P3 + (P1 - P3).*ne.^2) +...
ne1.*(ne3.^2.*P2 + (-P2 + P3).*ne.^2))./((ne1 - ne2).*(ne1 - ne3).*(ne2 - ne3));

figure(5)
plot(Pin,ne,'k-o','linewidth',2)
axis([0 2700 0 11e17])
xlabel('Pin [W]')
ylabel('n_e [m^{-3}]')
%% Generator parameter
Rg = 50;

%% Fixed element (b) definition for T-matching
% blim is the limit value for b, Rmax is the highest real impedance value
% to be matched. Here the b element is taken to be a capacitor. The limit
% value for this capacitor is Cblim. CCCCb is the set value.


Rmax = max(R)+5;
blim = -sqrt(Rg*Rmax);
Cblim = -1/blim/w;

CCCCb =100e-12;
bb = -1./w./CCCCb;

if abs(bb)<=abs(blim)
    disp('WARNING: (b) limit passed->imaginary solution')
else
end

StCb=num2str(CCCCb/1e-12);


%% Solutions for T matching

k = find(R);

for k = min(k):max(k)
    
    
    %% Solutions for T matching:  two possible solutions
    
    b(k) = bb;
    
    c1(k) = (sqrt(R(k).*Rg.*(b(k).^2 - R(k).*Rg)) - Rg.*(b(k) + X(k)))./Rg;
    
    c2(k) = -((sqrt(R(k).*Rg.*(b(k).^2 - R(k).*Rg)) + Rg.*(b(k) + X(k)))./Rg);
    
    a1(k) = ((-b(k)).*R(k) + sqrt(R(k).*Rg.*(b(k).^2 - R(k).*Rg)))./R(k);
    
    a2(k) = -((b(k)).*R(k) + sqrt(R(k).*Rg.*(b(k).^2 - R(k).*Rg)))./R(k);
    
    
end

%% Figure(6)
% Plots of the matching box tunable impedance elements, for the defined
% impedance path (here plotted as function of the hypothetical RF power)

for ttt=1
    figure(6)
    clf
    subplot(2,1,1)
    plot(Pin,a1,'k*-','linewidth',3)
    hold
    plot(Pin,a2,'r*-','linewidth',3)
    xlabel('Pin [W]')
    ylabel('imepedance {\ita} [\Omega]')
    legend('matching solution 1','matching solution 2','Location','East','FontSize',14)
    grid
    
    subplot(2,1,2)
    plot(Pin,c1,'k*-','linewidth',3)
    hold
    plot(Pin,c2,'r*-','linewidth',3)
    xlabel('Pin [W]')
    ylabel('imepedance {\itc} [\Omega]')
    legend('matching solution 1','matching solution 2','Location','NorthEast','FontSize',14)
    grid
end

%% matching elements construction

% Here we define the properties of the tunable elements (a) and (c). These
% elements are made by a series combination of a variable capacitor (Ca1,2 and Cc1,2), of
% range [Cmini,Cmaxi] in [F], and an inductor (La1,2 and Lc1,2).

% The inductance for each of these inductors can be set to a chosen value by setting
% the parameters (FixLa1,2, FixLc1,2) to 1. When set to 1, the value of the
% inductors will be given by the inductances defined just below as La1f,
% Lc1f,La2f, Lc2f.

% If(FixLa1,2, FixLc1,2) are not set to 1, the values for the inductances
% are determined to maximize the use of the tunable capacitor range


Cmini = 25e-12;
Cmaxi = 250e-12;

FixLa1 = 1;
FixLc1 = 1;

La1f = 5.5e-6;
Lc1f = 5.6e-6;

FixLa2 = 1;
FixLc2 = 1;

La2f = 2.1e-6;
Lc2f = 3.1e-6;


%% these are just incremental parameters for out of range detection, 
% meaning when the whole impedance path
% cannot be matched with the defined matching elements.

lima1 = 0;
lima2 = 0;
limc1 = 0;
limc2 = 0;

%% Elements a and c determination

for ttt = 1:1
    maxa1 = max(max(a1));
    mina1 = min(min(a1));
    
    if FixLa1==1
        
        La1 =La1f;
    else
        if maxa1>0
            La1 = (maxa1+1/w/Cmaxi)/w;
        else
            La1 = 0;
        end
    end
    
    k = find(a1);
    for k = min(k):max(k)
        
        Ct = 1./w./(w.*La1-a1(k));
        
        if Ct>Cmaxi
            Ca1(k) = Cmaxi;
            lima1 = lima1+1;
        elseif Ct<Cmini
            Ca1(k) = Cmini;
            lima1 = lima1+1;
        else
            Ca1(k) = Ct;
        end
    end
    
    maxa2 = max(max(a2));
    mina2 = min(min(a2));
    
    if FixLa2==1
        
        La2 =La2f;
    else
        if maxa2>0
            La2 = (maxa2+1/w/Cmaxi)/w;
        else
            La2 = 0;
        end
    end
    
    k = find(a2);
    
    for k = min(k):max(k)
        
        Ct = 1./w./(w.*La2-a2(k));
        
        if Ct>Cmaxi
            Ca2(k) = Cmaxi;
            lima2 = lima2+1;
        elseif Ct<Cmini
            Ca2(k) = Cmini;
            lima2 = lima2+1;
        else
            Ca2(k) = Ct;
        end
    end
    
    
    maxc1 = max(max(c1));
    minc1 = min(min(c1));
    
    
    if FixLc1==1
        
        Lc1 =Lc1f;
    else
        if maxc1>0
            Lc1 = (maxc1+1/w/Cmaxi)/w;
        else
            Lc1 = 0;
        end
    end
    
    k = find(c1);
    for k = min(k):max(k)
        
        Ct = 1./w./(w.*Lc1-c1(k));
        
        if Ct>Cmaxi
            Cc1(k) = Cmaxi;
            limc1 = limc1+1;
        elseif Ct<Cmini
            Cc1(k) = Cmini;
            limc1 = limc1+1;
        else
            Cc1(k) = Ct;
        end
    end
    
    
    maxc2 = max(max(c2));
    minc2 = min(min(c2));
    
    if FixLc2==1
        
        Lc2 =Lc2f;
    else
        if maxc2>0
            Lc2 = (maxc2+1/w/Cmaxi)/w;
        else
            Lc2 =0;
        end
    end
    
    k = find(c2);
    for k = min(k):max(k)
        
        Ct = 1./w./(w.*Lc2-c2(k));
        
        if Ct>Cmaxi
            Cc2(k) = Cmaxi;
            limc2 = limc2+1;
        elseif Ct<Cmini
            Cc2(k) = Cmini;
            limc2 = limc2+1;
        else
            
            Cc2(k) = Ct;
        end
    end
    
    SLa1 = num2str(La1/1e-6);
    SLa2 = num2str(La2/1e-6);
    SLc1 = num2str(Lc1/1e-6);
    SLc2 = num2str(Lc2/1e-6);
    
end

%% Figure(2)
% plots for matching check : matched impedance Zin and reflected power.

for ttt =1:1
    Za1 = 1i.*(w.*La1-1./w./Ca1);
    Zb1 = -1i./w./CCCCb;
    Zc1 = 1i.*(w.*Lc1-1./w./Cc1);
    
    Zf1 = Za1 + 1./(1./Zb1 + 1./(R + 1i.*X + Zc1));
    
    
    Za2 = 1i.*(w.*La2-1./w./Ca2);
    Zb2 = -1i./w./CCCCb;
    Zc2 = 1i.*(w.*Lc2-1./w./Cc2);
    
    Zf2 = Za2 + 1./(1./Zb2 + 1./(R + 1i.*X + Zc2));
    
    Gam1 = ((Zf1)-50)./((Zf1)+50);
    PrPf1 = abs(Gam1).^2;
    
    Gam2 = ((Zf2)-50)./((Zf2)+50);
    PrPf2 = abs(Gam2).^2;
    
    
    figure(2)
    clf
    subplot(3,1,1)
    plot(Pin,real(Zf1),'*-k','linewidth',3)
    hold
    plot(Pin,imag(Zf1),'*-r','linewidth',3)
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('Matched impedance [\Omega]','FontWeight','bold')
    title(['Matching Check solution 1, Re(Zin) in black, in imag(Zin) red'],'FontWeight','bold')
    grid
    
    subplot(3,1,2)
    plot(Pin,real(Zf2),'*-k','linewidth',3)
    hold
    plot(Pin,imag(Zf2),'*-r','linewidth',3)
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('Matched impedance [\Omega]','FontWeight','bold')
    title(['Matching Check solution 2, Re(Zin) in black, in imag(Zin) red'],'FontWeight','bold')
    grid
    
    subplot(3,1,3)
    plot(Pin,PrPf1,'*-k','linewidth',3)
    hold
    plot(Pin,PrPf2,'*-r','linewidth',3)
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('Reflected power [W]','FontWeight','bold')
    title(['Solution 1 black,, solution 2 red'],'FontWeight','bold')
    grid
    
end

%% Figure(8): plots of the matching solutions in terms of matching elements.

% If one of the curves appears in red, it means that, at least for one point, the needed capacitor has reached a limit
% of the defined tunable capacitor range. The value of the required fixed
% inductor is indicated in each figure title.

for ttt = 1:1
    
    figure(8)
    clf
    subplot(2,2,1)
    
    if limc1>=1
        plot(Pin,Ca1./1e-12,'*-r','linewidth',3)
    else
        plot(Pin,Ca1./1e-12,'*-k','linewidth',3)
    end
    
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('pF','FontWeight','bold')
    title(['C_{a1} , with L_{a1} = ' SLa1 '\muH'],'FontWeight','bold')
    grid
    
    subplot(2,2,2)
    
    if lima1>=1
        plot(Pin,Cc1./1e-12,'*-r','linewidth',3)
    else
        plot(Pin,Cc1./1e-12,'*-k','linewidth',3)
    end
    
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('pF','FontWeight','bold')
    title(['C_{c1} , with L_{c1} = ' SLc1 '\muH'],'FontWeight','bold')
    grid
    subplot(2,2,3)
    
    if limc2>=1
        plot(Pin,Ca2./1e-12,'*-r','linewidth',3)
    else
        plot(Pin,Ca2./1e-12,'*-k','linewidth',3)
    end
    
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('pF','FontWeight','bold')
    title(['C_{a2}, with L_{a2} = ' SLa2 '\muH'],'FontWeight','bold')
    subplot(2,2,4)
    grid
    if lima2>=2
        plot(Pin,Cc2./1e-12,'*-r','linewidth',3)
    else
        plot(Pin,Cc2./1e-12,'*-k','linewidth',3)
    end
    
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('pF','FontWeight','bold')
    title(['C_{c2}, with L_{c2} = ' SLc2 '\muH'],'FontWeight','bold')
    grid
end

%% Figure(4): 
% currents and potentials into the matching box. 
% (Iin --> Ia, IB --> Ib, IL --> Ic) (Vin --> Va, VB --> Vb, VL --> Vc)

for ttt=1
    % Solution 1
    Zin1 = 1i.*(a1 + bb - bb.^2./(bb + c1 - 1i.*R + X));
    
    Iin1 = sqrt(Pin./real(Zin1));
    
    VB1 = sqrt((bb.^2.*Iin1.^2.*(R.^2 + (c1 + X).^2))./(R.^2 + (bb + c1 + X).^2));
    
    IB1 = sqrt((bb.^2.*Iin1.^2.*(R.^2 + (c1 + X).^2))./(R.^2 + (bb + c1 + X).^2))./sqrt(bb.^2);
    
    IL1 = sqrt((bb.^2.*Iin1.^2)./(R.^2 + (bb + c1 + X).^2));
    
    Vin1 = sqrt(Iin1.^2).*sqrt((bb.^2.*(R.^2 + (c1 + X).^2) + 2.*a1.*bb.*(R.^2 + (c1 + X).*(bb + c1 + X)) + a1.^2.*(R.^2 + (bb + c1 + X).^2))./(R.^2 + (bb + c1 + X).^2));
    
    VL1 = sqrt(R.^2 + X.^2).*sqrt((bb.^2.*Iin1.^2)./(R.^2 + (bb + c1 + X).^2));
    
    % Solution 2
    
    Zin2 = 1i.*(a2 + bb - bb.^2./(bb + c2 - 1i.*R + X));
    
    Iin2 = sqrt(Pin./real(Zin2));
    
    VB2 = sqrt((bb.^2.*Iin2.^2.*(R.^2 + (c2 + X).^2))./(R.^2 + (bb + c2 + X).^2));
    
    IB2 = sqrt((bb.^2.*Iin2.^2.*(R.^2 + (c2 + X).^2))./(R.^2 + (bb + c2 + X).^2))./sqrt(bb.^2);
    
    IL2 = sqrt((bb.^2.*Iin2.^2)./(R.^2 + (bb + c2 + X).^2));
    
    Vin2 = sqrt(Iin2.^2).*sqrt((bb.^2.*(R.^2 + (c2 + X).^2) + 2.*a2.*bb.*(R.^2 + (c2 + X).*(bb + c2 + X)) + a2.^2.*(R.^2 + (bb + c2 + X).^2))./(R.^2 + (bb + c2 + X).^2));
    
    VL2 = sqrt(R.^2 + X.^2).*sqrt((bb.^2.*Iin2.^2)./(R.^2 + (bb + c2 + X).^2));
    
    
    
    figure(4)
    clf
    
    subplot(2,2,1)
    plot(Pin,Iin1,'k','linewidth',3)
    hold
    plot(Pin,IB1,'r','linewidth',3)
    plot(Pin,IL1,'b','linewidth',3)
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('[A]','FontWeight','bold')
    title('solution 1: Iin (black), Ib (red), Ic (blue)','FontWeight','bold')
    grid
    
    subplot(2,2,2)
    plot(Pin,Iin2,'k','linewidth',3)
    hold
    plot(Pin,IB2,'r','linewidth',3)
    plot(Pin,IL2,'b','linewidth',3)
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('[A]','FontWeight','bold')
    title('solution 2: Iin (black), Ib (red), Ic (blue)','FontWeight','bold')
    grid
    
    subplot(2,2,3)
    plot(Pin,Vin1,'k','linewidth',3)
    hold
    plot(Pin,VB1,'r','linewidth',3)
    plot(Pin,VL1,'b','linewidth',3)
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('[V]','FontWeight','bold')
    title('solution 1: Vin (black), Vb (red), Vload (blue)','FontWeight','bold')
    grid
    
    subplot(2,2,4)
    plot(Pin,Vin2,'k','linewidth',3)
    hold
    plot(Pin,VB2,'r','linewidth',3)
    plot(Pin,VL2,'b','linewidth',3)
    xlabel('Pin [W]','FontWeight','bold')
    ylabel('[V]','FontWeight','bold')
    title('solution 1: Vin (black), Vb (red), Vload (blue)','FontWeight','bold')
    grid
    
end

%% Figure(9): power disspation in the matching box elements

for ttt=1
    % Definition of the matching box element resistances
    Ra = 0.05;
    Rb = 0.05;
    Rc = 0.05;
    
    figure(9)
    clf
    subplot(2,1,1)
    plot(Pin,Ra.*Iin1.^2,'sk-','linewidth',3)
    hold
    plot(Pin,Rb.*IB1.^2,'sr-','linewidth',3)
    plot(Pin,Rc.*IL1.^2,'sb-','linewidth',3)
    xlabel('Pin [kW]','FontWeight','bold')
    ylabel('[W]','FontWeight','bold')
    title('Solution 1: P_{a} (black), P_{b} (red), P_{c} (blue)')
    grid
    
    subplot(2,1,2)
    plot(Pin,Ra.*Iin2.^2,'vk-','linewidth',3)
    hold
    plot(Pin,Rb.*IB2.^2,'vr-','linewidth',3)
    plot(Pin,Rc.*IL2.^2,'vb-','linewidth',3)
    xlabel('Pin [kW]','FontWeight','bold')
    ylabel('[W]','FontWeight','bold')
    title('Solution 1: P_{a} (black), P_{b} (red), P_{c} (blue)')
    grid
end

