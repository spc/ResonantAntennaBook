Run Matching.m
Book Fig. 13.6 corresponds to output Fig. 6

The figures are reproduced by setting the inductors to have user defined
values, i.e. by setting the parameters:

FixLa1 = 1;
FixLc1 = 1;
FixLa2 = 1;
FixLc2 = 1;

and setting the user defined values :
La1f = 5.5e-6;
Lc1f = 5.6e-6;
La2f = 2.1e-6;
Lc2f = 3.1e-6;

which is done by default in the file. But in general it is recommended
to start with setting these parameters to 0, for a first estimation of
the required inductances, using the full range of the variable capacitors.