% orbitsPhGsolution.m
% exact for cold, collisionless plasma with ions
clc, clf, clear
%% constants
q = 1.602e-19; % electron charge [C]
m = 9.109e-31; % electron mass [kg]
%% experiment parameters
B0 = 200e-4; % [T]
E0 = 1; % Electric field [V/m]
%% frequencies
fRF = 13.56e6; % excitation RF frequency [Hz]
wRF = 2*pi*fRF; %
wce = q*B0/m; % electron cyclotron frequency [rad/s]
%% UNBOUNDED MOTION, TIME SOLUTION DIRECTLY
%% cyclotron resonance driven by an R wave
% (Swanson Section 2.9.3, integrated for x,y)
figure(1), clf
subplot(121)
t = [0:0.0001:18]/fRF; % 18 RF periods 
% note that 7.4 e-cyclotron periods = 18% of one RF period (approximately)
xeC = (-E0/wce/B0)*(wce*t.*sin(wce*t)+cos(wce*t)-1);
yeC = (-E0/wce/B0)*(-wce*t.*cos(wce*t)+sin(wce*t));
plot(1e6*xeC,1e6*yeC,'b','linewidth',2)
axis([-1 1 -1 1])
axis square
text(-1.3,0.9,'(a)','fontsize',16)
xlabel('\itx \rm[micron]'), ylabel('\ity \rm[micron]')
%% PhG solution for R-wave (Ey = jEx)
% respect magnetized plasma: nu << w << wce
figure(1)
subplot(122)
t = [0:0.0001:18]/fRF;
nu = wRF/3; % "much smaller" for magnetized definition. Was wRF/20
wHat = wRF + j*nu; 
Ex = E0*exp(-j*wRF*t);
uxPI = (-q/m)*j*Ex/(wHat - wce); % Particular Integral for ux
C1=5e2; C2=5e2;
uxCF1 = C1*exp(-(nu+j*wce)*t);
uxCF2 = C2*exp(-(nu-j*wce)*t); % Complementary Fn.
x = uxPI/(-j*wRF) + uxCF1/(-(nu+j*wce)) + uxCF2/(-(nu-j*wce));
uyPI = j*uxPI;
uyCF1 = j*C1*exp(-(nu+j*wce)*t);
uyCF2 = -j*C2*exp(-(nu-j*wce)*t); % Complementary Fn.
y = uyPI/(-j*wRF) + uyCF1/(-(nu+j*wce)) + uyCF2/(-(nu-j*wce));
plot(1e6*real(x),1e6*real(y)), hold on
xPI = uxPI/(-j*wRF);
yPI = uyPI/(-j*wRF);
plot(1e6*real(xPI),1e6*real(yPI),'b','linewidth',2)
text(-1.3,0.9,'(b)','fontsize',16)
axis([-1, 1, -1, 1])
axis square
xlabel('\itx \rm[micron]'), ylabel('\ity \rm[micron]')

