% cutoffRAID.m % exact for cold, collisionless plasma with ions
clc, clf, clear
%% constants
c = 2.99e8; % speed of light [m/s]
q = 1.602e-19; % electron charge [C]
m = 9.109e-31; % electron mass [kg]
mp = 1.67e-27; % proton mass [kg]
M = 40*mp; % ion mass for Ar+
eps0 = 8.85e-12; % vacuum permittivity [m-3 kg-1 s4 A2] [F/m?]
%% experiment parameters
B0 = 200e-4; % [T]
ne = 1e18; % electron density [m-3]
fRF= 13.56e6; % excitation RF frequency [Hz]
lambdaRF = c/fRF
%% important (angular) frequencies
wRF = 2*pi*fRF; %
wce = q*B0/m % electron cyclotron frequency [rad/s]
wci = q*B0/M; % ion cyclotron frequency [rad/s]
wpe = sqrt(ne.*q^2./(eps0*m)); % electron plasma frequency
wpi = sqrt(ne.*q^2./(eps0*M)); % ion plasma frequency
wp = sqrt(wpe.^2+wpi.^2); % (exact) plasma frequency
%% CUTOFF frequencies Lieberman94 Table 4.2 p117
%% O wave cutoff
wcoO = wp; % O wave cutoff in, exact, Lieberman Table 4.2
%% R wave and X wave cutoff
wcoR = ((wce-wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2; % exact Lieberman 4.5.17
%% L wave and X wave cutoff
wcoL = ((-wce+wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2; % exact Lieberman 4.5.18
%% RESONANCE frequencies Lieberman94 Table 4.2 p117 
wresR = wce; % R wave resonance exact Lieberman 4.5.17
wresL = wci; % L wave resonance exact Lieberman 4.5.18
B=-(wce^2+wci^2+wpe^2+wpi^2); % set up the quadratic in w^2
C=wpe^2*wci^2+wpi^2*wce^2+wce^2*wci^2;
wUH=sqrt((-B+sqrt(B^2-4*C))/2); % upper hybrid resonance exact
wLH=sqrt((-B-sqrt(B^2-4*C))/2); % lower hybrid resonance exact
%% DISPERSION RELATIONS k=k(w)
minw = 1e4; % frequency range [rad/s]
maxw = 2e11;
w = logspace(log10(minw),log10(maxw),10000); % frequency axis (for RAID)
k0 = w./c;
kmax = 300; % [1/m] arbitrary wavenumber axis maximum
%% O wave
kO = sqrt(k0.^2-wp.^2/c^2); % w2 = wp2 + k2c2; k2=k02-wp2/c2 exact
%% R wave
kR = k0.*sqrt(1 - wpe.^2./(w.*(w-wce))- wpi.^2./(w.*(w+wci))); % Lieberman 4.5.16 exact
%% L wave
kL = k0.*sqrt(1 - wpe.^2./(w.*(w+wce))- wpi.^2./(w.*(w-wci))); % Lieberman 4.5.16 exact
%% X wave Lieberman 4.5.14
kX = sqrt(kR.^2.*kL.^2./(1 - wpe.^2./(w.^2-wce.^2)- wpi.^2./(w.^2-wci.^2)))./k0; % exact
%% PLOT 4 linear dispersion relations. See Lieberman Fig. 4.10
figure(1), clf
%% O wave
semilogy(kO,w,'y-','linewidth',2), hold on % exact dispersion relation
semilogy(k0,c*k0,'k--') % plot w/k = c
%% R wave
semilogy(kR,w,'m-','linewidth',2), % exact dispersion relation
%% L wave
semilogy(kL,w,'c-','linewidth',2), % exact dispersion relation
%% X wave
semilogy(kX,w,'g-','linewidth',2), % exact dispersion relation
% cut offs same as wR, wL
%% plot 3 cutoffs
semilogy([0,kmax/20],[wp,wp],'k-','linewidth',3), % O cutoff
text(kmax/20,wp,'wp') % O cutoff
semilogy([0,kmax/20],[wcoR,wcoR],'k-','linewidth',3), % R cutoff
text(kmax/20,wcoR,'wR') % R cutoff
semilogy([0,kmax/20],[wcoL,wcoL],'k-','linewidth',3), % L cutoff
text(kmax/20,wcoL,'wL') % L cutoff
%% plot 4 resonances
semilogy([0,kmax],[wce,wce],'k--','linewidth',2), % R resonance
text(kmax,wce,'wce') % R resonance
semilogy([0,kmax],[wci,wci],'k--','linewidth',2), % L resonance
text(kmax,wci,'wci') % L resonance
semilogy([0,kmax],[wUH,wUH],'k--','linewidth',2),
text(kmax,wUH,'wUH') % X resonance at upper hybrid frequency
semilogy([0,kmax],[wLH,wLH],'k--','linewidth',2),
text(kmax,wLH,'wLH') % X resonance at lower hybrid frequency
%% RF frequency marker
semilogy([0,kmax/10],[wRF,wRF],'.-','linewidth',3),
text(kmax/10,wRF,'13.56 MHz')
xlabel('k [1/m]'), ylabel('w [rad/s]')
title('semilogy plot of all 4 dispersion relations in Ar')
xlim([.1,kmax]), ylim([minw,maxw])
legend('O wave','w=ck','R whistler','L wave','X wave')
%% refractive index and cut-off UNMAGNETIZED
% N^2 = epsp = k^2/k0^2
epsp = (1 - wpe^2./w.^2); % relative permittivity (collisionless)
pos = find(epsp>0); % indices for positive epsilon

figure(2), clf
plot(w/wpe,epsp,'b--','linewidth',2), hold on
plot(w(pos)/wpe,epsp(pos),'b','linewidth',2) % plot for positive values 

xmax = 3; ymin = -5; ymax = 2;
plot([0 xmax],[0 0],'k','linewidth',1) % x=0 axis

plot([1.7 xmax],[1 1],'k--','linewidth',1) % asymptote (vacuum propagation)
text(2,1.23,'vacuum asymptote = 1') %

plot([1 1],[ymin ymax],'k','linewidth',1) % cut-off frequency
text(1.05,-3,'cut-off frequency') %
text(1.05,-3.4,'\omega = \omega_{pe}') %
text(0.1,-1.5,'evanescent')
text(1.5,0.4,'propagating')
xlabel('\omega/\omega_{pe}'),
ylabel('\epsilon_p = \itN\rm^2 = \itk\rm^2/\itk\rm_0^2')
ylim([ymin,ymax]), xlim([0,xmax])

%% refractive index and cut-off MAGNETIZED RAID
%wpe/wce = 16.0411 % more than an order of magnitude ratio
wRF13MHz = 2*pi*13.56e6/wpe; % 0.0015
wRF100MHz = 2*pi*100e6/wpe; % 0.0111

epsRwave = (1 - wpe^2./(w.*(w-wce))); % epsr R wave (collisionless)
posR = find(epsRwave>0 & epsRwave<1); % 
posRbis = find(epsRwave>5);

epsLwave = (1 - wpe^2./(w.*(w+wce))); % epsr L wave (collisionless)
posL = find(epsLwave>0); % indices for positive epsilon

figure(3), clf

semilogx(w/wpe,epsRwave,'r--','linewidth',2), hold on
semilogx(w(posR)/wpe,epsRwave(posR),'r','linewidth',2),
semilogx(w(posRbis)/wpe,epsRwave(posRbis),'r','linewidth',2),

semilogx(w/wpe,epsLwave,'g--','linewidth',2),
semilogx(w(posL)/wpe,epsLwave(posL),'g','linewidth',2),

semilogx(w/wpe,epsp,'b--','linewidth',2),
semilogx(w(pos)/wpe,epsp(pos),'b','linewidth',2) % plot for positive values 

xmin = 1e-3; xmax = 3; ymin = -5000; ymax = 12000;
semilogx([1e-3 3],[0 0],'k','linewidth',1) % x=0 axis

semilogx([1 1],[ymin ymax],'k','linewidth',1) % cut-off frequency
text(0.75,2000,'cut-off frequency, \omega = \omega_{pe}','rotation',90) %

text(0.08,2000,'e-cyclotron resonance, \omega_{ce}','rotation',90)

semilogx([wRF13MHz wRF13MHz],[5600 ymax],'k--','linewidth',1) % 
text(0.0015,2000,'13.56 MHz','rotation',90) %

semilogx([wRF100MHz wRF100MHz],[1000 8000],'k--','linewidth',1) % 
text(0.0108,8000,'100 MHz','rotation',90) %

annotation('doublearrow',[0.18 0.363],[0.65 0.65])
text(0.0023,6700,'RF range','backgroundcolor','w')

text(0.0015,-500,'evanescent')
text(0.0015,700,'propagating')

text(0.015,2000,'R whistler')
text(0.0034,-3000,'L')
text(0.0085,-3000,'unmagnetized','backgroundcolor','w')
text(0.08,-3000,'R')

xlabel('log [ \omega/\omega_{pe} ]'),
ylabel('\epsilon_p = \itN\rm^2 = \itk\rm^2/\itk\rm_0^2')
ylim([ymin,ymax]), 
xlim([xmin,3])

%% refractive index and cut-off MAGNETIZED ARTIFICIAL
wceA = 0.75*wpe; % ARTIFICIALLY VERY HIGH E-CYCLOTRON FREQUENCY
epsRwave = (1 - wpe^2./(w.*(w-wceA))); % epsr R wave (collisionless)
posR = find(epsRwave>0 & epsRwave<1); % 
posRbis = find(epsRwave>5); 

epsLwave = (1 - wpe^2./(w.*(w+wceA))); % epsr L wave (collisionless)
posL = find(epsLwave>0); % indices for positive epsilon

figure(4), clf
plot(w/wpe,epsRwave,'r--','linewidth',2), hold on
plot(w(posR)/wpe,epsRwave(posR),'r','linewidth',2),
plot(w(posRbis)/wpe,epsRwave(posRbis),'r','linewidth',2),

plot(w/wpe,epsLwave,'g--','linewidth',2),
plot(w(posL)/wpe,epsLwave(posL),'g','linewidth',2),


plot(w/wpe,epsp,'b--','linewidth',2),
plot(w(pos)/wpe,epsp(pos),'b','linewidth',2) % plot for positive values 

xmax = 2; ymin = -15; ymax = 15;
plot([0 xmax],[0 0],'k','linewidth',1) % x=0 axis

plot([1 xmax],[1 1],'k--','linewidth',1) % asymptote (vacuum propagation)
text(1.2,2,'vacuum asymptote = 1') %

plot([1 1],[ymin ymax],'k','linewidth',1) % cut-off frequency
text(0.95,3.2,'cut-off frequency','rotation',90) %
%text(1.02,8.5,'\omega = \omega_{pe}','rotation',90) %

text(0.7,2,'e-cyclotron resonance','rotation',90)

text(0.35,9,'R')
text(0.27,7.4,'whistler')
text(0.08,-6,'L')
text(0.2,-10,'unmagnetized')
text(0.86,-13,'R')

text(0.1,-2,'evanescent')
text(0.1,2,'propagating')

xlabel('\omega/\omega_{pe}'),
ylabel('\epsilon_p = \itN\rm^2 = \itk\rm^2/\itk\rm_0^2')
ylim([ymin,ymax]), 
xlim([0,xmax])



