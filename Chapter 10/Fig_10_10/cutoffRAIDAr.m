% cutoffRAIDAr.m copied from cutoffRAID.m 
% exact for cold, collisionless plasma with ions
clc, clf, clear
%% constants
c = 2.99e8; % speed of light [m/s]
q = 1.602e-19; % electron charge [C]
m = 9.109e-31; % electron mass [kg]
mp = 1.67e-27; % proton mass [kg]
M = 40*mp; % ion mass for Ar+
eps0 = 8.85e-12; % vacuum permittivity [m-3 kg-1 s4 A2] [F/m?]
%% experiment parameters
B0 = 200e-4; % [T]
ne = 1e18; % electron density [m-3]
fRF= 13.56e6; % excitation RF frequency [Hz]
lambdaRF = c/fRF
%% important (angular) frequencies
wRF = 2*pi*fRF; %
wce = q*B0/m % electron cyclotron frequency [rad/s]
wci = q*B0/M; % ion cyclotron frequency [rad/s]
wpe = sqrt(ne.*q^2./(eps0*m)); % electron plasma frequency
wpi = sqrt(ne.*q^2./(eps0*M)); % ion plasma frequency
wp = sqrt(wpe.^2+wpi.^2); % (exact) plasma frequency
%% CUTOFF frequencies Lieberman94 Table 4.2 p117
%% O wave cutoff
wcoO = wp; % O wave cutoff in, exact, Lieberman Table 4.2
%% R wave and X wave cutoff
wcoR = ((wce-wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2; % exact Lieberman 4.5.17
%% L wave and X wave cutoff
wcoL = ((-wce+wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2; % exact Lieberman 4.5.18
%% RESONANCE frequencies Lieberman94 Table 4.2 p117 
wresR = wce; % R wave resonance exact Lieberman 4.5.17
wresL = wci; % L wave resonance exact Lieberman 4.5.18
B=-(wce^2+wci^2+wpe^2+wpi^2); % set up the quadratic in w^2
C=wpe^2*wci^2+wpi^2*wce^2+wce^2*wci^2;
wUH=sqrt((-B+sqrt(B^2-4*C))/2); % upper hybrid resonance exact
wLH=sqrt((-B-sqrt(B^2-4*C))/2); % lower hybrid resonance exact
%% DISPERSION RELATIONS k=k(w)
minw = 1e4; % frequency range [rad/s]
maxw = 2e11;
w = logspace(log10(minw),log10(maxw),10000); % frequency axis (for RAID)
k0 = w./c;
kmax = 300; % [1/m] arbitrary wavenumber axis maximum
%% O wave
kO = sqrt(k0.^2-wp.^2/c^2); % w2 = wp2 + k2c2; k2=k02-wp2/c2 exact
%% R wave
kR = k0.*sqrt(1 - wpe.^2./(w.*(w-wce))- wpi.^2./(w.*(w+wci))); % Lieberman 4.5.16 exact
%% L wave
kL = k0.*sqrt(1 - wpe.^2./(w.*(w+wce))- wpi.^2./(w.*(w-wci))); % Lieberman 4.5.16 exact
%% X wave Lieberman 4.5.14
kX = sqrt(kR.^2.*kL.^2./(1 - wpe.^2./(w.^2-wce.^2)- wpi.^2./(w.^2-wci.^2)))./k0; % exact
%% PLOT 4 linear dispersion relations. See Lieberman Fig. 4.10
figure(1), clf
%% O wave
semilogy(kO,w,'m-','linewidth',1), hold on % exact dispersion relation
text(40,1e11,'O, R, L, X') % all 4 branches are superposed here
%% R wave
semilogy(kR,w,'r-','linewidth',1), hold on % exact dispersion relation
%text(50,1e11,'R,')
text(110,6e8,'R whistler')
%% L wave
semilogy(kL,w,'b-','linewidth',1), hold on % exact dispersion relation
text(4.5,3e4,'L')
%text(60,1e11,'L,')

%% X wave
semilogy(kX,w,'g-','linewidth',1), hold on % exact dispersion relation
% cut offs same as wR, wL
text(70,3e6,'X')
%% plot 3 cutoffs
semilogy([0,kmax/20],[wp,wp],'k-','linewidth',1), % O cutoff
text(4.5,1.2e11,'\omega_{\itp}','fontsize',12) % O cutoff at wp
semilogy([0,kmax/20],[wcoR,wcoR],'k-','linewidth',1), % R cutoff
text(-17,6e10,'\omega_{\itR}','fontsize',12) % R cutoff at wcoR
semilogy([0,kmax/20],[wcoL,wcoL],'k-','linewidth',1), % L cutoff at wcoL
text(4.5,3.7e10,'\omega_{\itL}','fontsize',12) % L cutoff
%% plot 4 resonances
semilogy([0,kmax],[wce,wce],'k--','linewidth',1), % R resonance
text(1.01*kmax,wce,'\omega_{\itce}','fontsize',12) % R resonance
semilogy([0,kmax],[wci,wci],'k--','linewidth',1), % L resonance
text(1.01*kmax,wci,'\omega_{\itci}','fontsize',12) % L resonance
semilogy([0,kmax],[wUH,wUH],'k--','linewidth',1),
text(1.01*kmax,wUH,'\omega_{\itUH}','fontsize',12) % X resonance at upper hybrid frequency
semilogy([0,kmax],[wLH,wLH],'k--','linewidth',1),
text(1.01*kmax,wLH,'\omega_{\itLH}','fontsize',12) % X resonance at lower hybrid frequency
%% RF frequency marker
semilogy([0,kmax/8],[wRF,wRF],'.-','linewidth',2),
text(1.1*kmax/8,wRF,'13.56 MHz','fontsize',12)
xlabel('\itk \rm[1/m]','fontsize',12), ylabel('\omega [rad/s]','fontsize',12)
xlim([.1,kmax]), ylim([minw,maxw])
legend('O wave','R wave','L wave','X wave','location','southeast')
