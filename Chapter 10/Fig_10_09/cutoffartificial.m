% cutoffartificial.m % exact for cold, collisionless plasma with ions
% FOR FIG. DispersionArtificial
clc, clf, clear % see also Lieberman94 Fig. 4.10
%% constants
c = 2.99e8 % speed of light [m/s]
q = 1.602e-19; % electron charge [C]
m = 9.109e-31; % electron mass [kg]
mp = 1.67e-27; % proton mass [kg]
M = 0.002*mp; % artificial reduced ion mass
eps0 = 8.85e-12; % vacuum permittivity [m-3 kg-1 s4 A2] [F/m?]
%% experiment parameters
B0 = 200e-4; % [T]
ne = 1e15; % electron density [m-3]
fRF= 13.56e6; % excitation RF frequency [Hz]
%% important (angular) frequencies
wRF = 2*pi*fRF; %
wce = q*B0/m; % electron cyclotron frequency [rad/s]
wci = q*B0/M; % ion cyclotron frequency [rad/s]
wpe = sqrt(ne.*q^2./(eps0*m)); % electron plasma frequency
wpi = sqrt(ne.*q^2./(eps0*M)); % ion plasma frequency
wp = sqrt(wpe.^2+wpi.^2); % (exact) plasma frequency
%% CUTOFF frequencies Lieberman94 Table 4.2 p117
%% O wave cutoff
wcoO = wp; % O wave cutoff in, exact, Lieberman Table 4.2
%% R wave and X wave cutoff
wcoR = ((wce-wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2; % exact Lieberman 4.5.17
%% L wave and X wave cutoff
wcoL = ((-wce+wci)+sqrt((wce-wci).^2+4*(wp.^2+wce.*wci)))/2; % exact Lieberman 4.5.18
%% RESONANCE frequencies Lieberman94 Table 4.2 p117 
wresR = wce; % R wave resonance exact Lieberman 4.5.17
wresL = wci; % L wave resonance exact Lieberman 4.5.18
B=-(wce^2+wci^2+wpe^2+wpi^2); % set up the quadratic in w^2
C=wpe^2*wci^2+wpi^2*wce^2+wce^2*wci^2;
wUH=sqrt((-B+sqrt(B^2-4*C))/2); % upper hybrid resonance exact
wLH=sqrt((-B-sqrt(B^2-4*C))/2); % lower hybrid resonance exact
%% DISPERSION RELATIONS k=k(w) here
w = [0.001:0.001:1]*wcoR*1.2; % wR cutoff frequency (for Fig. 4.10)
k0 = w./c;
k0max = max(k0);
%% O wave
kO = sqrt(k0.^2-wp.^2/c^2); % w2 = wp2 + k2c2; k2=k02-wp2/c2 exact
%% R wave
kR = k0.*sqrt(1 - wpe.^2./(w.*(w-wce))- wpi.^2./(w.*(w+wci))); % Lieberman 4.5.16 exact
%% L wave
kL = k0.*sqrt(1 - wpe.^2./(w.*(w+wce))- wpi.^2./(w.*(w-wci))); % Lieberman 4.5.16 exact
%% X wave Lieberman 4.5.14
kX = sqrt(kR.^2.*kL.^2./(1 - wpe.^2./(w.^2-wce.^2)- wpi.^2./(w.^2-wci.^2)))./k0; % exact
%% PLOT 4 linear dispersion relations. See Lieberman Fig. 4.10
figure(1), clf
%% O wave
plot(kO,w,'m-','linewidth',1), hold on % exact dispersion relation
text(6,2.85e9,'O')
%% R wave
plot(kR,w,'r-','linewidth',1), % exact dispersion relation
text(10,4.87e9,'R')
text(9,1.87e9,'R whistler')
%% L wave
plot(kL,w,'b-','linewidth',1), % exact dispersion relation
text(6,0.65e9,'L')
text(9,3.07e9,'L')
%% X wave
plot(kX,w,'g-','linewidth',1), % exact dispersion relation
text(10,4.43e9,'X')
text(12,3.1e9,'X')
text(12,1.1e9,'X')
% cut offs same as wR, wL
%%
plot(k0,c*k0,'k-.','linewidth',1) % plot w/k = c
%% plot 3 cutoffs
plot([0,k0max/20],[wp,wp],'k-','linewidth',1), % O cutoff
text(1.1*k0max/20,wp,'\omega_{\itp}','fontsize',12) % O cutoff
plot([0,k0max/20],[wcoR,wcoR],'k-','linewidth',1), % R cutoff
text(1.1*k0max/20,wcoR,'\omega_{\itR}','fontsize',12) % R cutoff
plot([0,k0max/20],[wcoL,wcoL],'k-','linewidth',1), % L cutoff
text(1.1*k0max/20,wcoL,'\omega_{\itL}','fontsize',12) % L cutoff
%% plot 4 resonances
plot([0,k0max],[wce,wce],'k--','linewidth',1), % R resonance
text(1.01*k0max,wce,'\omega_{\itce}','fontsize',12) % R resonance
plot([0,k0max],[wci,wci],'k--','linewidth',1), % L resonance
text(1.01*k0max,wci,'\omega_{\itci}','fontsize',12) % L resonance
plot([0,k0max],[wUH,wUH],'k--','linewidth',1),
text(1.01*k0max,wUH,'\omega_{\itUH}','fontsize',12) % X resonance at upper hybrid frequency
plot([0,k0max],[wLH,wLH],'k--','linewidth',1),
text(1.01*k0max,wLH,'\omega_{\itLH}','fontsize',12) % X resonance at lower hybrid frequency
%% graphics
xlabel('\itk \rm[1/m]','fontsize',12), ylabel('\omega [rad/s]','fontsize',12)
%title('linear plot of all 4 dispersion relations')
axis tight,
xlim([0.001*k0max,k0max]), % remove the ugly y axis
text(0,-0.17e9,'0')
legend('O wave','R wave','L wave','X wave','\omega = \itck','location','east')
