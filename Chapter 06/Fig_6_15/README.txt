The file  Quantitative.m  takes the vacuum and plasma data in files
FreqShift_f_mode_0W.dat  and  FreqShift_f_mode_200W.dat,
which are obtained from Fig. 6.11, to plot the output Figs. 7 and 8.

These two figures make up the book Fig. 6.15.

