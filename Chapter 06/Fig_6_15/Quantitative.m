clear % Quantitative.m
% experiment and model data obtained using Fig. 6.11 folder

load('FreqShift_f_mode_0W.dat')
m=FreqShift_f_mode_0W(:,1);
fVAC=FreqShift_f_mode_0W(:,2);
load('FreqShift_f_mode_200W.dat')
fPLASMA=FreqShift_f_mode_200W(:,2);
fVACmodel=[20.608,15.966, 13.384, 11.830, 10.806];
fPLASMAmodel=[21.848, 16.802, 13.772, 11.990, 10.836];

figure(7), clf
fSHIFT=fPLASMA-fVAC;
fSHIFTmodel=fPLASMAmodel-fVACmodel;
subplot(111)
plot(m,fSHIFTmodel,'kv','MarkerSize',10,'LineWidth',2.0), hold on
plot(m,fSHIFT,'k^','MarkerSize',10,'MarkerFaceColor','k','LineWidth',2.0), hold on
legend('model', 'expt')
axis([1 11 0 1.3])
ylabel('frequency shift [MHz]','interpreter','tex','fontsize',16,'fontname','times')
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes

MAXvac=[250.4247, 258.1178, 208.7523, 130.6541, 97.6922];
MAXpl=[34.3, 32.7, 29.3, 28, 34.5];
MAXvacMODEL=[244.847, 268.323, 201.247, 141.638, 101.184];
MAXplMODEL=[33.3724,32.6961, 26.4108, 27.2628, 39.9102];

figure(8)
subplot(111)
plot(m,MAXvac,'ksq','MarkerSize',11,'MarkerFaceColor','k','LineWidth',2.0), hold on
plot(m,MAXvacMODEL,'ko','MarkerSize',10,'LineWidth',2.0), hold on
plot(m,MAXpl,'k+','MarkerSize',10,'LineWidth',2.0), hold on
plot(m,MAXplMODEL,'kd','MarkerSize',10,'LineWidth',2.0), hold on
legend('vac expt','vac model','pl expt','pl model')
axis([1 11 0 299])
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes
xlabel('mode number','interpreter','tex','fontsize',16,'fontname','times')
ylabel('peak impedance [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
