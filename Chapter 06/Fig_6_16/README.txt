Conductivity_SkinDepth.m  is used to compare the three x-axes in book Fig. 6.16.

Using the data files obtained from experiment and book Fig. 6.11,

the output Fig. 2 of  PowerScan_m_6.m, and
the output Fig. 3 of  Zchange_vs_ne_m_6.m

generate the book Fig. 6.16 parts (a) and (b), respectively.