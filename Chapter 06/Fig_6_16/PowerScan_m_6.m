clear % PowerScan_m_6.m for Fig. 6.16a
figure(2), clf
% Model frequency shift as a function of electron density ne_bar
% using m_6.m (see folder for Fig. 6.11), 
% which has ne=3e16*12/7.8 [m-3] for 200 W
neBar = [0.1,0.4,0.769, 1.5385, 3.0769, 4.6154, 6.1538, 7.6923, 9.2308,  10.769, 12.308, 13.846, 15.385, 16.923, 18.462, 20, 30.769, 46.154, 461.54, 4615.4, 46154, 461540,1e6]*1e+16;
fSHIFTm6 = [0,0.005,0.03, 0.108, 0.262, 0.388, 0.494, 0.582, 0.66, 0.726, 0.786, 0.84, 0.888, 0.932, 0.974, 1.01, 1.214, 1.4, 2.23, 2.62, 2.76, 2.805,2.81];

% COMPARE WITH MEASUREMENTS
load ('FreqShift_f_Power_M6.dat') % power scan m=6
power = FreqShift_f_Power_M6(:,1);
power=[50, 80, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900];
ne=[0.044013, 0.76060, 0.77901, 1.4826, 1.9084, ...
    2.4385, 4.1348, 5.6191, 6.4672, 8.0575, 9.8599, 11.238]*1e16;

load('SatCurM6_f_power.dat')
Power=SatCurM6_f_power(:,1)
SatCurM6=SatCurM6_f_power(:,2)
Power = [50, 80, 100, 150, 200, 300, 400, 500];
SatCurM6 = [0.0306,  0.0566, 0.0726, 0.1040, 0.1325, 0.1899, 0.2356, 0.2761];
neSatCurM6 = SatCurM6*(3e16*12/7.8)/0.1325; % normalize to 200W point
M6df =(FreqShift_f_Power_M6(3:10,2));
REDUCEDneBar = [200, 4615.4, 46154, 461540,2e6]*1e+16;

fLimit=2.825*ones(size(REDUCEDneBar)); % shift [MHz] for perfect screen at dplasma = 1.2 cm 
subplot(221)
semilogx(neSatCurM6,M6df,'ksq','MarkerSize',10,'LineWidth',2.0), hold on
semilogx(neBar,fSHIFTm6,'k-','MarkerSize',10,'LineWidth',2.0), hold on
semilogx(REDUCEDneBar,3.09*ones(size(REDUCEDneBar)),'k--d','MarkerSize',10,'LineWidth',2.0), hold on % from screen scan
semilogx(REDUCEDneBar,fLimit,'k--','MarkerSize',10,'LineWidth',2.0), hold on
legend('expt', 'model','screen expt','screen model','Location','SouthEast')
text(.25e18,3,'screen','fontsize',16,'fontname','times')
text(.7e16,3,'(a)','fontsize',16,'fontname','times')
ylabel('frequency shift [MHz]','interpreter','tex','fontsize',16,'fontname','times')
axis([1e15 1e22 0 3.5])
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes