clear % Zchange_vs_ne_m_6.m % from PowerScan_m_6.m
figure(3), clf
% Model impedance change as a function of electron density ne_bar
% using m_6.m, which has ne=3e16*12/7.8 [m-3] for 200 W
neBar = [0.046154,0.0769, 0.15385,0.30769,0.46154,0.769, 1.5385, 3.0769,...
    4.6154, 6.1538, 7.6923, 9.2308,  10.769,...
    12.308, 13.846, 15.385, 16.923, 18.462, 20, 30.769, 46.154, 461.54,...
    4615.4, 46154, 461540,4615400,46154000]*1e+16;
absZmodel_m6=[200.4801, 199.8920, 194.0908,160.8245,...
    126.1169,83.157,47.5297,31.3247,26.4078,24.096,22.785,21.9647,21.42,...
    21.0469,20.7843,20.5992,20.4724,20.3835,20.3298,20.3901,20.9497,32.3341,...
    57.1285,85.5611,103.8182,111.5419,114.2331]; % absZ_m_6.m  copied from shift_m_6.m

% COMPARE WITH MEASUREMENTS
load ('ImpMag_vs_power.dat') % power scan m=6 for magnitude of Z 
power = ImpMag_vs_power(3:10,1); % choose the powers for which ne data are known
absZexpt_m6 = ImpMag_vs_power(3:10,2);

Power = [50, 80, 100, 150, 200, 300, 400, 500];
SatCurM6 = [0.0306,  0.0566, 0.0726, 0.1040, 0.1325, 0.1899, 0.2356, 0.2761];
neSatCurM6 = SatCurM6*(3e16*12/7.8)/0.1325; % normalize to 200W point, same as Fig. 6

neBarRED = [0.046154,0.369, 3, 10.769,180.462 ]*1e16;
ZvacModel_m6 = 204*ones(size(neBarRED)); % vacuum limit for ne -> 0
ZvacExpt_m6 = 208.7*ones(size(neBarRED)); % vacuum limit for ne -> 0

REDUCEDneBar = [200, 6615.4, 46154, 461540,2e6]*1e+16;
ZscreenModel_m6 = 115.4*ones(size(REDUCEDneBar)); % screen limit for ne -> inf
ZscreenExpt_12mm_m6 = 123.7*ones(size(REDUCEDneBar)); % screen limit for ne -> inf

subplot(221)
semilogx(neSatCurM6,absZexpt_m6,'ksq','MarkerSize',10,'LineWidth',2.0), hold on
semilogx(neBar,absZmodel_m6,'k-','MarkerSize',10,'LineWidth',2.0), hold on
semilogx(neBarRED,ZvacExpt_m6,'ko--','MarkerSize',10,'LineWidth',2.0), hold on
semilogx(neBarRED,ZvacModel_m6,'k-.','MarkerSize',10,'LineWidth',2.0), hold on
semilogx(REDUCEDneBar,ZscreenExpt_12mm_m6,'kd-','MarkerSize',10,'LineWidth',2.0), hold on
semilogx(REDUCEDneBar,ZscreenModel_m6,'k--','MarkerSize',10,'LineWidth',2.0), hold on
xlabel('plasma density {\it n}_e [m^{-3}]','interpreter','tex','fontsize',16,'fontname','times'),
ylabel('mag ({\it Z}_{in})  [\Omega]','interpreter','tex','fontsize',16,'fontname','times'),
legend('expt', 'model','vac expt','vac model','screen expt','screen model','Location','North')
text(.7e21,135,'screen','fontsize',16,'fontname','times')
text(.6e16,195,'vacuum','fontsize',16,'fontname','times')
text(1e21,200,'(b)','fontsize',16,'fontname','times')
axis([1e15 1e22 0 230])
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes

