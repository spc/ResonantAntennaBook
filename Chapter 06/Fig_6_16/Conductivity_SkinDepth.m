clear % Conductivity_SkinDepth.m
% To determine the triple x axis in Fig. 6.16 vs plasma density ne
figure(1), clc, clf
mu0 = 4*pi*1e-7; % [H/m]
ne =  logspace(22, 15, 100); % plasma density [m-3]
%ne = [8e15 9.2e16] % for info: the density range of plasma experiment
one = ones(size(ne));
w=2*pi*300e6; %13.385e6; % angular frequency [Hz]
pressure = 1.3; % pressure [Pa]
dplasma = 1.2; % [cm]
% Collision frequency [1/s] = 2*pi*13.56 MHz FOR 3.33 Pa in Ar (Lieberman p465)
nu = w*pressure/3; 
e = 1.6e-19; % e [C]
me = 9.1e-31; % me [kg]
sigDC = ne*e*e./(nu*me); % DC conductivity [1/(ohm.m)], array along bars
sigPL = sigDC./(1+i*w./nu); % plasma conductivity [1/(ohm.m)]
RealSigPL = real(sigPL); % real conductivity [1/(ohm.m)]
p = (i*w*mu0*sigPL).^(-0.5); % complex penetration depth [m]
PlasmaSkinDepth = 100./real(1./p); % plasma real skin depth [m]

subplot(221), loglog(PlasmaSkinDepth,ne,'MarkerSize',10,'LineWidth',2.0)
ylabel('ne [m^{-3}]','interpreter','tex','fontsize',16,'fontname','times')
xlabel('plasma skin depth \delta [cm]','interpreter','tex','fontsize',16,'fontname','times')
axis tight
set(gca,'XDir','reverse')
legend('Plasma skin depth')
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes

subplot(223), loglog(RealSigPL,ne,'MarkerSize',10,'LineWidth',2.0)
ylabel('ne [m^{-3}]','interpreter','tex','fontsize',16,'fontname','times') 
xlabel('plasma conductivity \sigma_{pl} [S/m]','interpreter','tex','fontsize',16,'fontname','times')
legend('plasma conductivity'), axis tight
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes

