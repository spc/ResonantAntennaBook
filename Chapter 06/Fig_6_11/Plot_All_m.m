% Plot_All_m.m
clear 
figure(1), clf
subplot(211)
load('m10.mat'), plot(f10vac, ZvacMes10,'k','LineWidth',1.5), hold on
                 plot(fpl10,ZplMes10,'k.','LineWidth',1.5), hold on
text(10.85,100,'\itm\rm = 10','fontsize',16,'fontname','times')

load('m8.mat'), plot(f8vac, ZvacMes8,'k','LineWidth',1.5), hold on
                plot(fpl8,ZplMes8,'k.','LineWidth',1.5), hold on
text(12,130,'\itm\rm = 8','fontsize',16,'fontname','times')
               
load('m6.mat'), plot(f6vac, ZvacMes6,'k','LineWidth',1.5), hold on
                plot(fpl6,ZplMes6,'k.','LineWidth',1.5), hold on
text(13.5,210,'\itm\rm = 6','fontsize',16,'fontname','times')
                
load('m4.mat'), plot(f4vac, ZvacMes4,'k','LineWidth',1.5), hold on
                plot(fpl4,ZplMes4,'k.','LineWidth',1.5), hold on
text(16.15,210,'\itm\rm = 4 vacuum','fontsize',16,'fontname','times')
text(16.5,50,'\itm\rm = 4 plasma','fontsize',16,'fontname','times')

load('m2.mat'), plot(f2vac, ZvacMes2,'k','LineWidth',1.5), hold on
                plot(fpl2,ZplMes2,'k.','LineWidth',1.5), hold on
text(20.5,210,'\itm\rm = 2','fontsize',16,'fontname','times')             
                
axis([10 22 0 270])
set(gca,'fontname','times','fontsize',16,'LineWidth',1.5)
ylabel('mag ( Z\rm_i_n ) [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
xlabel('frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')
text(10.5,240,'(a) experiment','fontsize',16,'fontname','times')


subplot(212)
plot(f10/1e6,Zvac10,'b-','LineWidth',1.5), hold on
plot(f10(50:150)/1e6,Zpl10(50:150),'r-','LineWidth',1.5), hold on
text(10.9,100,'\itm\rm = 10','fontsize',16,'fontname','times')

plot(f8/1e6,Zvac8,'b-','LineWidth',1.5), hold on
plot(f8(151:351)/1e6,Zpl8(151:351),'r-','LineWidth',1.5), hold on
text(12,130,'\itm\rm = 8','fontsize',16,'fontname','times')

plot(f6/1e6,Zvac6,'b-','LineWidth',1.5), hold on
plot(f6(351:650)/1e6,Zpl6(351:650),'r-','LineWidth',1.5), hold on
text(13.5,210,'\itm\rm = 6','fontsize',16,'fontname','times')

plot(f4/1e6,Zvac4,'b-','LineWidth',1.5), hold on
plot(f4(751:1101)/1e6,Zpl4(751:1101),'r-','LineWidth',1.5), hold on
text(16.15,210,'\itm\rm = 4 vacuum','fontsize',16,'fontname','times')
text(16.5,50,'\itm\rm = 4 plasma','fontsize',16,'fontname','times')

plot(f2/1e6,Zvac2,'b-','LineWidth',1.5), hold on
plot(f2(801:1251)/1e6,Zpl2(801:1251),'r-','LineWidth',1.5), hold on
text(20.8,210,'\itm\rm = 2','fontsize',16,'fontname','times')

axis([10 22 0 270])
set(gca,'fontname','times','fontsize',16,'LineWidth',1.5)
ylabel('mag ( Z\rm_i_n ) [\Omega]','interpreter','tex','fontsize',16,'fontname','times')
xlabel('frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')
text(10.5,240,'(b) model','fontsize',16,'fontname','times')