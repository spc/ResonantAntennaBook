Run the files m_2.m, m_4.m, m_6.m, m_8.m, and m_10.m
to calculate the antenna impedance in vacuum, and with plasma,
for modes m=2,4,6,8,10, to compare with the experiment data files
Imp_f_freq_0W.dat (in vacuum) and Imp_f_freq_200W.dat (with plasma).

The outputs are saved in m2.mat, m4.mat,... etc.

Plot_All_m.m  generates the required book figure, Fig. 6.11.