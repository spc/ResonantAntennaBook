clear % m_8.m

% USE COMPLEX IMAGE MODEL (CIM) TO REPRESENT PLASMA
% First calculate for a perfect screen (vacuum spectrum)
% Then add the plasma complex image (plasma loading)
% Plasma image mutuals included inside the frequency loop
% Include ne profile effect in CIM
clc
mu0 = 4*pi*1e-7; % [H/m]
%% Plasma parameters:
% line-averaged ne [m-3] measured with microwave interferometry,
% normalized with respect to the density for the m=10 mode:
neBar = 3e16*12/7.8;
LegCount = [1:23];
% density profile along antenna: half-height parabola:
% (the results are not very sensitive to the profile shape)
ne = neBar*(1-((LegCount-12).^2)/242)*6/5;
% dplasma might depend on f via sheath dependence:
dplasma =1.2; % distance from leg axis to plasma-sheath interface NB: [cm]
pressure = 1.3; % pressure [Pa]
% Collision frequency = 2*pi*13.56 MHz FOR 3.33 Pa in Ar (Lieberman p465)
nu = 2*pi*13.56e6*pressure/3;
e = 1.6e-19; % e [C]
me = 9.1e-31; % me [kg]
sigDC = ne*e*e./(nu*me); % DC conductivity [1/(ohm.m)]
%% only for skin resistance calculations
rhoCu = 17e-9; % copper resistivity [ohm.m]
ray = 0.003; % leg radius [m]
Hleg = 0.2; % leg length [m]
Hring = 0.017; % effective stringer length [m]
dring = 0.025; % effective stringer width [m]
%% Spacing of floating point numbers; eps with no arguments returns 2^(-52)
dd = eps; % smallest parallel filament distance d in Grover p45 eq.28.

%% antenna component values
%Attention: these lengths in [cm]
hc = 1.9; % stringer length
W = 0.6; % stringer strip width

h = 19.2; % leg length
a = 0.3; % leg radius

dmin = 2.5; % distance between legs
ds = 5.5; % distance to screen

N = 23; % number of legs

C = 2.6e-9; % capacitance [F]
% stringer strip self-inductance:
Mstring = mu0.*(hc./100)./2./pi.*(log(2.*hc./W)+0.5);
Lleg = mu0.*(h./100)./2./pi.*(log(2.*h./a)-1); % leg bar inductance

%% RF connection positions
Nf = 12; % RF feedpoint
Ng = 1; % ground
Ng2 = 23; % ground
%% grounds and feeding node arrays used in the experiments
SM=[0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0]; % line A
SK=[-.5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 -.5]; % line B

%% transpose
SM=SM';
SK=SK';
%% calculate the antenna and screen mutual inductance matrix (indept of f)
fac = 1; %  fac=0 to put leg and stringer mutuals to zero
facIM=1; % facIM=0 puts all screen image mutuals to zero
for k = 1:N  % matrix index
    
    for j = 1:N  % matrix index
          
                % using Grover expression p45 eq. 28 for legs
                deltaL = -h;
                dL(k,j) = abs(k-j).*dmin;
                alphaL = h + h + deltaL;
                betaL = h + deltaL;
                gamaL = h + deltaL;
                % mutual inductance for legs
                L(k,j) = fac*1e-9.*(alphaL.*asinh(alphaL./dL(k,j)) - betaL.*asinh(betaL./dL(k,j)) - gamaL.*asinh(gamaL./dL(k,j)) + deltaL.*asinh(deltaL./dL(k,j)) - ...
                    sqrt(alphaL.^2 + dL(k,j).^2)+sqrt(betaL.^2 + dL(k,j).^2)+sqrt(gamaL.^2 + dL(k,j).^2)-sqrt(deltaL.^2 + dL(k,j).^2));
                
                % using Grover expression p45 eq. 28 for leg images
                deltaLim = -h;
                dpLim(k,j) = abs(k-j).*dmin;
                dLim(k,j) = sqrt(dpLim(k,j).^2 + (2.*ds).^2);
                alphaLim = h + h + deltaLim;
                betaLim = h + deltaLim;
                gamaLim = h + deltaLim;
                % mutual inductance for leg images
                Lim(k,j) = facIM*1e-9.*(alphaLim.*asinh(alphaLim./dLim(k,j)) - betaLim.*asinh(betaLim./dLim(k,j)) - gamaLim.*asinh(gamaLim./dLim(k,j)) + deltaLim.*asinh(deltaLim./dLim(k,j)) - ...
                    sqrt(alphaLim.^2 + dLim(k,j).^2)+sqrt(betaLim.^2 + dLim(k,j).^2)+sqrt(gamaLim.^2 + dLim(k,j).^2)-sqrt(deltaLim.^2 + dLim(k,j).^2));
                
                % using Grover expression p45 eq. 28 for in-line stringers
                deltaM(k,j) = (abs(k-j)-1).*dmin;
                dM = eps;
                alphaM(k,j) = hc + hc + deltaM(k,j);
                betaM(k,j) = hc + deltaM(k,j);
                gamaM(k,j) = hc + deltaM(k,j);
                % mutual inductance for in-line stringers (rather use the
                % limiting expression eq.35)
                M(k,j) = fac*1e-9.*(alphaM(k,j).*asinh(alphaM(k,j)./dM) - betaM(k,j).*asinh(betaM(k,j)./dM) - gamaM(k,j).*asinh(gamaM(k,j)./dM) + deltaM(k,j).*asinh(deltaM(k,j)./dM) - ...
                    sqrt(alphaM(k,j).^2 + dM.^2)+sqrt(betaM(k,j).^2 + dM.^2)+sqrt(gamaM(k,j).^2 + dM.^2)-sqrt(deltaM(k,j).^2 + dM.^2));
               
                % using Grover expression p45 eq. 28 for stringer images
                deltaMim(k,j) = (abs(k-j)-1).*dmin;
                dMim = 2.*ds;
                alphaMim(k,j) = hc + hc + deltaMim(k,j);
                betaMim(k,j) = hc + deltaMim(k,j);
                gamaMim(k,j) = hc + deltaMim(k,j);
                % mutual inductance for stringer images
                Mim(k,j) = facIM*1e-9.*(alphaMim(k,j).*asinh(alphaMim(k,j)./dMim) - betaMim(k,j).*asinh(betaMim(k,j)./dMim) - gamaMim(k,j).*asinh(gamaMim(k,j)./dMim) + deltaMim(k,j).*asinh(deltaMim(k,j)./dMim) - ...
                    sqrt(alphaMim(k,j).^2 + dMim.^2)+sqrt(betaMim(k,j).^2 + dMim.^2)+sqrt(gamaMim(k,j).^2 + dMim.^2)-sqrt(deltaMim(k,j).^2 + dMim.^2));
                
                % using Grover expression p45 eq. 28 for opposing stringers
                deltaM2(k,j) = (abs(k-j)-1).*dmin;
                dM2 = h;
                alphaM2(k,j) = hc + hc + deltaM2(k,j);
                betaM2(k,j) = hc + deltaM2(k,j);
                gamaM2(k,j) = hc + deltaM2(k,j);
                % mutual inductance for opposing stringers
                M2(k,j) = fac*1e-9.*(alphaM2(k,j).*asinh(alphaM2(k,j)./dM2) - betaM2(k,j).*asinh(betaM2(k,j)./dM2) - gamaM2(k,j).*asinh(gamaM2(k,j)./dM2) + deltaM2(k,j).*asinh(deltaM2(k,j)./dM2) - ...
                    sqrt(alphaM2(k,j).^2 + dM2.^2)+sqrt(betaM2(k,j).^2 + dM2.^2)+sqrt(gamaM2(k,j).^2 + dM2.^2)-sqrt(deltaM2(k,j).^2 + dM2.^2));
                
                % using Grover expression p45 eq. 28 for opposing stringer images
                deltaM2im(k,j) = (abs(k-j)-1).*dmin;
                dM2im = sqrt(h.^2 + (2.*ds).^2);
                alphaM2im(k,j) = hc + hc + deltaM2im(k,j);
                betaM2im(k,j) = hc + deltaM2im(k,j);
                gamaM2im(k,j) = hc + deltaM2im(k,j);
                % mutual inductance for opposing stringer images
                M2im(k,j) = facIM*1e-9.*(alphaM2im(k,j).*asinh(alphaM2im(k,j)./dM2im) - betaM2im(k,j).*asinh(betaM2im(k,j)./dM2im) - gamaM2im(k,j).*asinh(gamaM2im(k,j)./dM2im) + deltaM2im(k,j).*asinh(deltaM2im(k,j)./dM2im) - ...
                    sqrt(alphaM2im(k,j).^2 + dM2im.^2)+sqrt(betaM2im(k,j).^2 + dM2im.^2)+sqrt(gamaM2im(k,j).^2 + dM2im.^2)-sqrt(deltaM2im(k,j).^2 + dM2im.^2));
                
       if j==k % the ONLY elements NOT described by filament expressions:
       % self inductance IS a special case of mutual inductance, but here,
       % each wire is described by a SINGLE filament, which therefore
       % cannot describe the self-inductance of an extended object.
       L(k,j) = Lleg; % leg self inductance
       M(k,j) = Mstring; % stringer self inductance
       end
        
    end % of loop over j
end % of loop over k

%% Calculate antenna and screen mutuals for vacuum case
MMvac=M-Mim-M2+M2im; % stringer inductance matrix including in-line, image,
% opposing and opposing image (image of image therefore positive).
MMvac(1:N,N)=ones(N,1)*1e4; % force Nth fictive stringer to have no current
MMvac(N,1:N)=ones(1,N)*1e4; % force Nth fictive stringer to have no current

LLvac = L-Lim; % leg inductance matrix including image

for k = 1:N % identity matrix

    for j = 1:N
        
        if j==k;
            
            Id(k,j) = 1;
        else
            Id(k,j) = 0;
        end
    end
end

for k = 1:N % lower shift matrix
    
    for j = 1:N
        
        if j==k-1;
            
            Sm(k,j) = 1;
        else
            Sm(k,j) = 0;
        end
    end
end

U = (Id-Sm); % U
UT = transpose(U); % UT
%% All the above is frequency-independent
df = 0.2*1e4;
f = 11.5e6:df:12.5e6; % frequency range adapted to the mode
w = f*2*pi; % angular frequency
jc = find(w); % jc is simply the vector of indices of w

for jf = min(jc):max(jc) % from first frequency to last frequency
%% Calculate the leg and stringer skin effect resistances   
    skin = sqrt(2.*rhoCu./w(jf)./(4.*pi.*1e-7)); % skin depth in copper
    SurfL = pi.*(ray.^2-(ray-skin).^2); % area of leg skin
    SurfM = 2.*dring.*skin; % area of stringer skin
    % estimate the Equivalent Series Resistance of the capacitors:
    ESR = 1.3*(0.0064+1.2e-4*f(jf)/1e6);
    RM = rhoCu.*Hring./SurfM + ESR; % stringer resistance
    RL = rhoCu.*Hleg./SurfL; % leg resistance 
    % (note: coupling to neighbouring plasma images not considered here)
    MRM = Id*(RM-i/w(jf)/C); % stringer impedance of resistance and capacitor
    LRL = Id*RL; % leg impedance of resistance
    
    Z2 = i*w(jf)*LLvac+LRL; % total leg impedance
    Z1 = i*w(jf)*MMvac+MRM; % total stringer impedance
%% Calculate the I currents directly in order N using matrix solution:
    LHS=U*inv(Z1)*UT*Z2+2*Id; % order N
    CI=inv(LHS)*(SK-SM); % order N solution for the leg currents
    A=0.5*(UT\Z1/U*(SM+SK)-Z2*CI); % top node voltages (N valued)
    B=0.5*(UT\Z1/U*(SM+SK)+Z2*CI); % bottom node voltages (N valued)
    CK=U\(SK-CI); % vector of N-1 currents in bottom stringers PLUS zero current in last stringer
    CM=U\(CI+SM); % vector of N-1 currents in top stringers PLUS zero current in last stringer
    % Calculate input impedance using voltage across input and ground:
    % (for unit input current)
    %ZinVAC(jf) = A(Nf)-A(Ng);  % Zin for antenna and screen in vacuum vs f for unit current
    ZinVAC(jf) = A(Nf)-B(Ng);  % Zin for antenna and screen in vacuum vs f for unit current
% CI,A,B,K,M,Zin are the same as for (N-1) calculation
end % of vacuum calculation

for jf = min(jc):max(jc) % RESTART the frequency loop
%% calculate the mutual inductance matrix FOR THE PLASMA IMAGE
facPL = 1; %  facPL=0 to put all plasma leg mutuals to zero
facPS = 0; %  facPL=0 to put all plasma stringer mutuals to zero

sigPL = sigDC./(1+i*w(jf)./nu); % plasma conductivity [1/(ohm.m)]
p = (i*w(jf)*mu0*sigPL).^(-0.5); % complex penetration depth [m]
PlasmaSkinDepth(jf) = 100./real(1./p(12)); % at middle leg [cm] for info only
P=100.*p; % complex penetration depth in [cm]
%% dsP IS THE ONLY PARAMETER TO CHANGE FOR PLASMA in the CIM
dsP = dplasma+P; % complex distance to plasma effective mirror for each leg [cm]
% calculate the plasma mutual impedance inside the frequency loop

for k = 1:N  % matrix index
    
    for j = 1:N  % matrix index
 
                 % using Grover expression p45 eq. 28 for leg images
                deltaLim = -h;
                dpLim(k,j) = abs(k-j).*dmin;
                dLim(k,j) = sqrt(dpLim(k,j).^2 + (2.*dsP(j)).^2);
                alphaLim = h + h + deltaLim;
                betaLim = h + deltaLim;
                gamaLim = h + deltaLim;
                % mutual inductance for leg images in plasma
                LimPL(k,j) = facPL*1e-9.*(alphaLim.*asinh(alphaLim./dLim(k,j)) - betaLim.*asinh(betaLim./dLim(k,j)) - gamaLim.*asinh(gamaLim./dLim(k,j)) + deltaLim.*asinh(deltaLim./dLim(k,j)) - ...
                    sqrt(alphaLim.^2 + dLim(k,j).^2)+sqrt(betaLim.^2 + dLim(k,j).^2)+sqrt(gamaLim.^2 + dLim(k,j).^2)-sqrt(deltaLim.^2 + dLim(k,j).^2));
                
               % using Grover expression p45 eq. 28 for stringer images
                deltaMim(k,j) = (abs(k-j)-1).*dmin;
                dMim = 2.*dsP(j);
                alphaMim(k,j) = hc + hc + deltaMim(k,j);
                betaMim(k,j) = hc + deltaMim(k,j);
                gamaMim(k,j) = hc + deltaMim(k,j);
                % mutual inductance for stringer images in plasma
                MimPL(k,j) = facPS*1e-9.*(alphaMim(k,j).*asinh(alphaMim(k,j)./dMim) - betaMim(k,j).*asinh(betaMim(k,j)./dMim) - gamaMim(k,j).*asinh(gamaMim(k,j)./dMim) + deltaMim(k,j).*asinh(deltaMim(k,j)./dMim) - ...
                    sqrt(alphaMim(k,j).^2 + dMim.^2)+sqrt(betaMim(k,j).^2 + dMim.^2)+sqrt(gamaMim(k,j).^2 + dMim.^2)-sqrt(deltaMim(k,j).^2 + dMim.^2));
                
               % using Grover expression p45 eq. 28 for opposing stringer images
                deltaM2im(k,j) = (abs(k-j)-1).*dmin;
                dM2im = sqrt(h.^2 + (2.*dsP(j)).^2);
                alphaM2im(k,j) = hc + hc + deltaM2im(k,j);
                betaM2im(k,j) = hc + deltaM2im(k,j);
                gamaM2im(k,j) = hc + deltaM2im(k,j);
                % mutual inductance for opposing stringer images in plasma
                M2imPL(k,j) = facPS*1e-9.*(alphaM2im(k,j).*asinh(alphaM2im(k,j)./dM2im) - betaM2im(k,j).*asinh(betaM2im(k,j)./dM2im) - gamaM2im(k,j).*asinh(gamaM2im(k,j)./dM2im) + deltaM2im(k,j).*asinh(deltaM2im(k,j)./dM2im) - ...
                    sqrt(alphaM2im(k,j).^2 + dM2im.^2)+sqrt(betaM2im(k,j).^2 + dM2im.^2)+sqrt(gamaM2im(k,j).^2 + dM2im.^2)-sqrt(deltaM2im(k,j).^2 + dM2im.^2));

    end % of loop over j
end % of loop over k

%-Still inside frequency loop: Now add screen mutuals and plasma mutuals
MM=MMvac-MimPL+M2imPL; % EFFECT OF PLASMA IMAGES ON STRINGERS
MM(1:N,N)=ones(N,1)*1e4; % force Nth fictive stringer to have no current
MM(N,1:N)=ones(1,N)*1e4; % force Nth fictive stringer to have no current

LL = LLvac-LimPL; % EFFECT OF PLASMA IMAGES ON LEGS

%% Calculate the leg and stringer impedances 
%% Calculate the leg and stringer skin effect resistances   
    skin = sqrt(2.*rhoCu./w(jf)./(4.*pi.*1e-7)); % skin depth in copper
    SurfL = pi.*(ray.^2-(ray-skin).^2); % area of leg skin
    SurfM = 2.*dring.*skin; % area of stringer skin
    ESR = 1.3*(0.0064+1.2e-4*f(jf)/1e6); % Effective Series Resistance
    RM = rhoCu.*Hring./SurfM+ESR; % stringer resistance
    RL = rhoCu.*Hleg./SurfL; % leg resistance 
    % (note: coupling to neighbouring plasma images not considered here)
    MRM = Id*(RM-i/w(jf)/C); % stringer impedance of resistance and capacitor
    LRL = Id*RL; % leg impedance of resistance
    Z2 = i*w(jf)*LL+LRL; % total leg impedance (includes plasma coupling)
    Z1 = i*w(jf)*MM+MRM; % total stringer impedance (includes plasma coupling)
%% Calculate the I currents directly in order N:
    LHS=U*inv(Z1)*UT*Z2+2*Id; % order N
    CI=inv(LHS)*(SK-SM); % order N solution for the leg currents
    A=0.5*(UT\Z1/U*(SM+SK)-Z2*CI); % top node voltages (N valued)
    B=0.5*(UT\Z1/U*(SM+SK)+Z2*CI); % bottom node voltages (N valued)
    CK=U\(SK-CI); % vector of N-1 currents in bottom stringers PLUS zero current in last stringer
    CM=U\(CI+SM); % vector of N-1 currents in top stringers PLUS zero current in last stringer
    %Zin(jf) = A(Nf)-A(Ng);  % Zin for plasma vs f for unit current
    Zin(jf) = A(Nf)-B(Ng);  % Zin for plasma vs f for unit current
% CI,A,B,K,M,Zin are the same as for (N-1) calculation
end %of frequency loop

figure(8), clf
subplot(121)
plot(f/1e6,real(ZinVAC),'b-',f/1e6,real(Zin),'r-')  % Vacuum impedance for antenna and screen in vacuum
title('m=8 abs(Z) at antenna'), legend('vac','plasma')
axis([11.5 12.5 0 150]);

% TRANSFORM TO COMPARE WITH REMY MEASUREMENTS
% Part I: Copper bar connections to RF (4 cm) and Ground (2x4 cm parallel)
lcon = 4+4/2; % individual lengths 4 cm
acon = 0.3; % bar radius 0.3 cm
Lcon = mu0.*(lcon./100)./2./pi.*(log(2.*lcon./acon)-1); % connection [H]
ZinVACcon=ZinVAC+i*w*Lcon; % vacuum impedance corrected for connections
Zincon=Zin+i*w*Lcon;% plasma impedance corrected for connections
% Part II: coaxial connection
L=0.3; % estimated 50 ohm line connection length [m]
epsilonr = 2.1; % assuming teflon dielectric
kL = L*sqrt(epsilonr)*2*pi*f./3e8;
Z0 = 50;
ZinVACcorr = Z0.*(ZinVACcon+Z0.*i.*tan(kL))./(Z0+ZinVACcon.*i.*tan(kL));
Zincorr = Z0.*(Zincon+Z0.*i.*tan(kL))./(Z0+Zincon.*i.*tan(kL));

subplot(122)
plot(f/1e6,abs(ZinVACcorr),'b-',f/1e6,abs(Zincorr),'r-') % transformed
f8 = f; Zvac8=abs(ZinVACcorr); Zpl8=abs(Zincorr);
MAXvacMODEL=max(Zvac8)
MAXplMODEL=max(Zpl8)
f8VACres=f8(find(Zvac8==max(Zvac8)))
f8PLASMAres=f8(find(Zpl8==max(Zpl8)))
title('m=8 transformed')
axis([11.5 12.5 0 150]), legend('vac','plasma','exp vac','exp plasma')
hold

% COMPARE WITH MEASUREMENTS
load ('Imp_f_freq_0W.dat') % vacuum impedance spectrum
fvac = Imp_f_freq_0W(:,1)*1;
ZvacMes = Imp_f_freq_0W(:,2);
load ('Imp_f_freq_200W.dat') % vacuum impedance spectrum
fpl = Imp_f_freq_200W(:,1);
ZplMes = Imp_f_freq_200W(:,2);
plot(fvac, ZvacMes,'k',fpl,ZplMes,'g')
f8vac=fvac(3750:3794); ZvacMes8=ZvacMes(3750:3794); fpl8=fpl(40:60); ZplMes8=ZplMes(40:60);
MAXvac=max(ZvacMes8)
MAXpl=max(ZplMes8)

save('m8.mat','f8','Zvac8','Zpl8','f8vac','ZvacMes8','fpl8','ZplMes8')
