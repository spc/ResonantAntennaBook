The Matlab output of HsurfaceBessel.m gives the left hand side of book Fig. 8.7:
Fig. 1 (low frequency approximation), and Fig. 2 (Bessel exact solution).

The Matlab output of HsurfBessel.m gives the right hand side of book Fig. 8.7:
Fig. 3 (low frequency approximation), and Fig. 4 (Bessel exact solution).