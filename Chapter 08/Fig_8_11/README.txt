Book Fig. 8.11 is a cartoon of the variation in the amplitude of 
each of Iz, Ez, and �Hy�, during a half cycle. In fact, the individual
rows are not synchronous in time with each other, because 
the induced electric field Ez is pi/2 out of phase with Iz and Hy; 
Fig. 8.11 is misleading in this respect.
The reader can follow the amplitudes of Ez and Hy as a function of time 
using the explicit time dependence in the Matlab files:
Ez_time.m  and  H_time.m  by changing the t value.