Run PlasmaEHSurfaceShellScreenBessel.m to obtain output Figs. 1 to 5.

Figs. 1 and 2 are plots of Ez.

Figs. 3 and 4 represent book Fig. 8.20.

Fig. 5 is a plot of the Poynting power flux.

(a slight angular asymmetry visible in the book Fig. 8.20
was perhaps due to numerical error.)