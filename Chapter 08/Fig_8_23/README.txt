Run PlasmaEHSurfaceShellScreenBessel.m to obtain output Figs. 1 to 5.

Figs. 1 and 2 are plots of Ez.

Figs. 3 and 4 represent book Fig. 8.21.

Fig. 5 ouput is a plot of the Poynting power flux as in book Fig. 8.23(a).

Book Fig. 8.23 (a) power flux, and (b) dissipated power density,
were both produced by a Comsol numerical simulation;
the agreement with Matlab Fig. 5 power flux is good, as expected.