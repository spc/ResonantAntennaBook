Run EzBirdcage9legs.m to obtain output Figs. 1, 2, and 3. 
Figs. 1 and 2 represent book Fig. 8.16
(Fig. 3 is a supplementary contour plot).

This file takes a couple of minutes to run.