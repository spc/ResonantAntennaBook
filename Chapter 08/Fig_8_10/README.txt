HvectorsBesselScreen.m gives the left hand side of book Fig. 8.10;
output Fig. 1 is a low frequency approximation;
output Fig. 2 is the Bessel solution.

HvectorsBesselDistantScreen.m gives the right hand side of book Fig. 8.10;
output Fig. 1 is a low frequency approximation;
output Fig. 2 is the Bessel solution.
