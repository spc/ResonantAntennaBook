ApolloniusEzOneWireScreen.m outputs Figs. 1 to 3.
Fig. 2 corresponds to book Fig. 8.26 left hand side.

ApolloniusEzTwoWires.m outputs Fig. 4 to 6.
Fig. 5 corresponds to book Fig. 8.26 right hand side.

(These figures could also be produced using (8.58) directly.)