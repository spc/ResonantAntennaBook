Run EzBirdcage18legs.m to obtain output Figs. 1, 2, and 3. 
Figs. 1 and 2 represent book Fig. 8.15
(Fig. 3 is a supplementary contour plot).
The view angle in the book is slightly different compared to Fig. 2 here.

This file takes several minutes to run.