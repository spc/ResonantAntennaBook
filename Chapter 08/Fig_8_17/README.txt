Run HBirdcage18legs.m to obtain output Figs. 1 and 2. 
Fig. 1 represents book Fig. 8.17 left hand side
(Fig. 2 is a supplementary contour plot).


Run HBirdcage9legs.m to obtain output Figs. 1 and 2. 
Fig. 1 represents book Fig. 8.17 right hand side
(Fig. 2 is a supplementary contour plot).

These files takes several minutes to run, especially for 18 legs.