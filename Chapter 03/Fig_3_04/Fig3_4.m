% Fig3_4.m
clear
clc

%% Raw data. Feeding
AAA = load('A1.DAT'); % RF at A12, ground at A1
fm1 = AAA(:,1); % MEASURED VALUES
Rm1 = AAA(:,2); % MEASURED VALUES
Xm1 = AAA(:,3); % MEASURED VALUES


AAA = load('A11.DAT'); % RF at A12, ground at A11
fm11 = AAA(:,1); % MEASURED VALUES
Rm11 = AAA(:,2); % MEASURED VALUES
Xm11 = AAA(:,3); % MEASURED VALUES


%% plots

figure(1)
clf
subplot(4,1,1)
plot(fm1/1e6,Rm1+0.1,'k','LineWidth',1)
hold

plot([22.97 22.97],[-300 300],'k'), plot([20.34 20.34],[-300 300],'k')
plot([17.91 17.91],[-300 300],'k'), plot([16.00 16.00],[-300 300],'k')
plot([14.52 14.52],[-300 300],'k'), plot([13.42 13.42],[-300 300],'k')
plot([12.55 12.55],[-300 300],'k'), plot([11.85 11.85],[-300 300],'k')
plot([11.27 11.27],[-300 300],'k'), plot([10.79 10.79],[-300 300],'k')
plot([10.40 10.40],[-300 300],'k'), plot([10.05 10.05],[-300 300],'k')
plot([9.77 9.77],[-300 300],'k'), plot([9.54 9.54],[-300 300],'k')
plot([9.34 9.34],[-300 300],'k'), plot([9.15 9.15],[-300 300],'k')
plot([9.02 9.02],[-300 300],'k'), plot([8.88 8.88],[-300 300],'k')
plot([8.785 8.785],[-300 300],'k'), plot([8.727 8.727],[-300 300],'k')
plot([8.68 8.68],[-300 300],'k'), plot([8.64 8.64],[-300 300],'k')
axis([8 24 0.01 230]);
xlabel('\itf \rm[MHz]'), ylabel('Re(\itZ\rm)  [\Omega ]')
text(13.8,150,'(a) RF at A12, ground at A1','backgroundcolor','w')

subplot(4,1,2)

plot(fm1/1e6,Xm1,'k','LineWidth',1)
hold

plot([22.97 22.97],[-300 300],'k'), plot([20.34 20.34],[-300 300],'k')
text(21.9,60,'m=1'), text(20.44,60,'2')
plot([17.91 17.91],[-300 300],'k'), plot([16.00 16.00],[-300 300],'k')
text(18.01,60,'3'), text(16.1,60,'4')
plot([14.52 14.52],[-300 300],'k'), plot([13.42 13.42],[-300 300],'k')
text(14.62,60,'5'), text(13.52,60,'6')
plot([12.55 12.55],[-300 300],'k'), plot([11.85 11.85],[-300 300],'k')
text(12.65,60,'7'), text(11.95,60,'8')
plot([11.27 11.27],[-300 300],'k'), plot([10.79 10.79],[-300 300],'k')
text(11.37,60,'9'), text(10.8,60,'..')
plot([10.40 10.40],[-300 300],'k'), plot([10.05 10.05],[-300 300],'k')
plot([9.77 9.77],[-300 300],'k'), plot([9.54 9.54],[-300 300],'k')
plot([9.34 9.34],[-300 300],'k'), plot([9.15 9.15],[-300 300],'k')
plot([9.02 9.02],[-300 300],'k'), plot([8.88 8.88],[-300 300],'k')
plot([8.785 8.785],[-300 300],'k'), plot([8.727 8.727],[-300 300],'k')
plot([8.68 8.68],[-300 300],'k'), plot([8.64 8.64],[-300 300],'k')
text(8.06,60,'22'),
axis([8 24 -120 120]);
xlabel('\itf \rm[MHz]'), ylabel('Im(\itZ\rm)  [\Omega ]')




subplot(4,1,3)
plot(fm11/1e6,Rm11+0.1,'k','LineWidth',1)
hold
 
plot([22.97 22.97],[-300 300],'k'), plot([20.34 20.34],[-300 300],'k')
plot([17.91 17.91],[-300 300],'k'), plot([16.00 16.00],[-300 300],'k')
plot([14.52 14.52],[-300 300],'k'), plot([13.42 13.42],[-300 300],'k')
plot([12.55 12.55],[-300 300],'k'), plot([11.85 11.85],[-300 300],'k')
plot([11.27 11.27],[-300 300],'k'), plot([10.79 10.79],[-300 300],'k')
plot([10.40 10.40],[-300 300],'k'), plot([10.05 10.05],[-300 300],'k')
plot([9.77 9.77],[-300 300],'k'), plot([9.54 9.54],[-300 300],'k')
plot([9.34 9.34],[-300 300],'k'), plot([9.15 9.15],[-300 300],'k')
plot([9.02 9.02],[-300 300],'k'), plot([8.88 8.88],[-300 300],'k')
plot([8.785 8.785],[-300 300],'k'), plot([8.727 8.727],[-300 300],'k')
plot([8.68 8.68],[-300 300],'k'), plot([8.64 8.64],[-300 300],'k')
axis([8 24 0.01 70]);
xlabel('\itf \rm[MHz]'), ylabel('Re(\itZ\rm)  [\Omega ]')
text(13.8,50,'(b) RF at A12, ground at A11','backgroundcolor','w')

subplot(4,1,4)

plot(fm11/1e6,Xm11,'k','LineWidth',1)
hold on

plot([22.97 22.97],[-300 300],'k'), plot([20.34 20.34],[-300 300],'k')
plot([17.91 17.91],[-300 300],'k'), plot([16.00 16.00],[-300 300],'k')
plot([14.52 14.52],[-300 300],'k'), plot([13.42 13.42],[-300 300],'k')
plot([12.55 12.55],[-300 300],'k'), plot([11.85 11.85],[-300 300],'k')
plot([11.27 11.27],[-300 300],'k'), plot([10.79 10.79],[-300 300],'k')
plot([10.40 10.40],[-300 300],'k'), plot([10.05 10.05],[-300 300],'k')
plot([9.77 9.77],[-300 300],'k'), plot([9.54 9.54],[-300 300],'k')
plot([9.34 9.34],[-300 300],'k'), plot([9.15 9.15],[-300 300],'k')
plot([9.02 9.02],[-300 300],'k'), plot([8.88 8.88],[-300 300],'k')
plot([8.785 8.785],[-300 300],'k'), plot([8.727 8.727],[-300 300],'k')
plot([8.68 8.68],[-300 300],'k'), plot([8.64 8.64],[-300 300],'k')
axis([8 24 -60 60]);
xlabel('\itf \rm[MHz]'), ylabel('Im(\itZ\rm)  [\Omega ]')

