% ZinNfNgForbiddenPlane.m  
clear all, clf, clc
N=23; % number of legs
m=6; % mode number
% simplified data set for mode m=6:
Lleg=143e-9; R=36e-3; C=2.6e-9; Lstr=7.9e-9; r=2e-3;
% angular frequency for mode m=6 using (3.51):
w=1./sqrt(C.*(Lstr+2.*Lleg.*(sin(0.5*m.*pi./N))^2));
Z1=1./(i.*w.*C)+i.*w.*Lstr+r; % stringer impedance (3.1)
Z2=R+i*w*Lleg; % leg impedance (3.2)

dN=0.1; % resolution in N space
[Nf,Ng] = meshgrid(1:dN:N); % Nf along x axis, Ng along y axis

gam=acosh(1+Z1./Z2); % complex propagation constant (3.8)

% calculate the dissipative input impedance:
G = (cosh(gam.*(N-2.*Nf+1))+cosh(gam.*(N-2.*Ng+1)))/2 ...
+cosh(gam.*(N))-cosh(gam.*(N-Ng-Nf+1))-cosh(gam.*(N+Ng-Nf)); % (3.45)
F = 0.5*tanh(gam./2)./sinh(gam.*N); % (3.38)
Zin = Z1.*(Nf-Ng)/2+Z2.*F.*G; % (3.44)

% forbidden space:
Npts = round(1+(N-2)/dN);
for n=1:length(Nf)
Zin(n,1:n)=0;
end
   
xNf = [1:1:23];
yNg = xNf-1;
figure(1)
subplot(111),
v = [1 51 101 151 201 251 301]; % chosen contour values
contour(Nf,Ng,real(Zin),v,'k','LineWidth',1.0), hold on 
plot(xNf,yNg,'r','LineWidth',3.0) % line corresponding to a single capacitor; Nf-Ng = 1
plot([1 12],[8 8],'b','LineWidth',3.0) % line for Ng=8
plot([12 12],[1 8],'b','LineWidth',3.0) % line for Nf=12
plot(12,8,'bo','MarkerFaceColor','b') % marker for (Ng,Nf)=(8,12)

xlabel('RF input connection,  \itN_f ','interpreter','tex','fontsize',20,'fontname','times'),
ylabel('RF ground connection,  \itN_g ','interpreter','tex','fontsize',20,'fontname','times'),
axis([1 N 1 N]) % the whole space of possible connections
set(gca,'fontname','times','fontsize',18,'LineWidth',1.0)
set(gca,'xtick',[1 3 5 7 9 11 13 15 17 19 21 23])
set(gca,'ytick',[1 3 5 7 9 11 13 15 17 19 21 23])
text(3,18,'excluded region for (\itN_g\rm \geq \itN_f\rm )','fontsize',20,'fontname','times')
text(3,21,'real input impedance,  Re (\itZ\rm_{in}) [\Omega]','fontsize',20,...
    'fontname','times','fontweight', 'bold')
% plot the line for connection across a single capacitor
ht=text(2.5,3.3,'\itN_f\rm - \itN_g\rm =1','fontsize',20,'fontname','times','color','r');
set(ht,'Rotation',40)
% indicate maximum and minimum of the real input impedance:
text(11,14,'301\Omega ','fontsize',20,'fontname','times')
annotation('arrow',[0.52 0.65],[0.57 0.53],'LineWidth',1)
text(9,12,'1\Omega ','fontsize',20,'fontname','times')
annotation('arrow',[0.43 0.43],[0.5 0.43],'LineWidth',1)