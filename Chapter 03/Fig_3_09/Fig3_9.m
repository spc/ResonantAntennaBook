% Fig3_9.m
clear
clc

%% Feeding
AAA = load('A8A16.dat'); % RF at A12, ground at A8 and A16
fmA8A16 = AAA(:,1); % MEASURED VALUES
RmA8A16 = AAA(:,2); % MEASURED VALUES
XmA8A16 = AAA(:,3); % MEASURED VALUES

AAA = load('A8.dat'); % RF at A12, ground at A8 only
fmA8 = AAA(:,1); % MEASURED VALUES
RmA8 = AAA(:,2); % MEASURED VALUES
XmA8 = AAA(:,3); % MEASURED VALUES

%% plots

figure(1),
clf
 subplot(2,1,1)
 plot(fmA8/1e6,RmA8+0.1,'k','LineWidth',1)
 hold
axis([8 25 0.01 290]);
xlabel('\itf \rm[MHz]'), ylabel('Re(\itZ\rm)  [\Omega ]')
text(14,255,'(a) RF at A12, ground at A8')
text(22.5,30,'m=1'), text(20.2,40,'2')
text(17.7,100,'3'), text(15.8,200,'4')
text(14.4,40,'5'), text(13.0,200,'6')
text(12.45,60,'7'), text(11.65,130,'8')
text(11.07,65,'9'), text(10.45,25,'10')

 subplot(2,1,2)
 plot(fmA8A16/1e6,RmA8A16+0.1,'k','LineWidth',1)
 hold
axis([8 25 0.01 290]);
xlabel('\itf \rm[MHz]'), ylabel('Re(\itZ\rm)  [\Omega ]')
text(14,255,'(b) RF at A12, ground at A8 and A16')
text(20.2,40,'2') 
text(15.8,205,'4')
text(13.0,205,'6')
text(11.65,130,'8')
text(10.45,25,'10')
