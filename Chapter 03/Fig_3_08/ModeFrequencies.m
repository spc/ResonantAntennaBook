clear % ModeFrequencies.m
clc
figure(1), clf
% Experimental data:
Fig5=load('Fig5corr.dat'); % mode frequency measurements in Fig. 3.4.
m=Fig5(:,1); % mode number
experiment=Fig5(:,2); % same as vertical lines in Fig. 3.4
plot(m,experiment,'k-x','MarkerSize',10,'LineWidth',1.5), hold on

% Normal mode frequency calculation
N=23; % number of legs
Lleg=146e-9; C=2.6e-9; Lstr=10.54e-9; % typical antenna values
w=1./sqrt(C.*(Lstr+2.*Lleg.*(sin(0.5*m.*pi./N)).^2)); % (3.12) or (3.51)
f=0.5e-6*w/pi; % frequency [MHz] (essentially independent of R and r)
plot(m,f,'k-d','MarkerSize',8,'LineWidth',1.5), hold on

% draw mode markers to guide the eye, for every 5th mode:
plot([5 5],[0 30],'k--','LineWidth',1.0), hold on
plot([10 10],[0 30],'k--','LineWidth',1.0), hold on
plot([15 15],[0 30],'k--','LineWidth',1.0), hold on
plot([20 20],[0 30],'k--','LineWidth',1.0), hold on

legend('experimental measurement','normal mode model')
axis([0 23 0 30])
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes
xlabel('mode number \itm \rm= 1 to 22','interpreter','tex','fontsize',16,'fontname','times')
ylabel('mode frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')


