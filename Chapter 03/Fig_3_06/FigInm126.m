% FigInm126.m for Fig. 3.6
% Normal mode current amplitude phasor In for modes m= 1,2,6
clear, clc, clf

N=23; % number of legs
n=[1:1:N]; % leg number
x=[1:0.01:N]; % fine resolution x axis
figure(1)
m = 1; % mode number 
In = cos((n-1/2).*m*pi/N);
Ix = cos((x-1/2).*m*pi/N);
plot(n,In,'kd','linewidth',1.5), hold on
plot(x,Ix,'k-','linewidth',1),
text(11.5,0.2,'m=1','fontsize',14)

m = 2; % mode number 
In = cos((n-1/2).*m*pi/N);
Ix = cos((x-1/2).*m*pi/N);
plot(n,In,'ro','linewidth',1.5),
plot(x,Ix,'r-','linewidth',1),
text(7,-0.65,'m=2','fontsize',14,'color','r')

m = 6; % mode number 
In = cos((n-1/2).*m*pi/N);
Ix = cos((x-1/2).*m*pi/N);
plot(n,In,'bsq','linewidth',1.5),
plot(x,Ix,'b-','linewidth',1),
text(15,0.65,'m=6','fontsize',14,'color','b')

xlabel('leg number','fontsize',12)
ylabel('normalized current amplitude','fontsize',12)
axis([1 23 -1.1 1.1])

