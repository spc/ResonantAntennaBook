% DissipativeImpedanceSpectrum.m, for Fig. 3.13
clear, clc
%% Dimensions

% indices: 1 horizontal (stringer), 2 vertical (leg)

H1 = 0.025; % stringer section spacing [m]
Hcap = 0.0036; % capacitor length [m] to obtain conductor length

H2 = 0.19; % leg effective length [m] (see section 4.4.1)

a1 = 0.006; % stringer strip width [m]
a2 = 0.003; % leg radius [m]

%% Stringer capacitor

C1 = 2.6e-9; %  stringer capacitance 2.6 nF

%% Self inductances
mu0 = 4*pi*1e-7; % permeability of free space [H/m]

% stringer strip self inductance, width a1, see (4.30):
L1 = mu0.*(H1-Hcap)./2./pi.*(log(2.*(H1-Hcap)./a1)+0.5);

% leg cylinder self inductance, radius a2, see (4.29):
L2 = mu0.*H2./2./pi.*(log(2.*H2./a2)-1);
%% Dimensions 
N = 23; % 23 legs

%% Feeding RF at A12 and ground at A8 ok
AAA = load('A8.dat'); % network analyser measurement data file
fm = AAA(:,1); % measured frequency
Rm = AAA(:,2); % measured resistance Re(Zin)
Xm = AAA(:,3); % measured reactance Im(Zin)

Ng = 8; % ground at node 8  (index "g" for "ground")
% Position Vin (index "f" for "feed")
Nf = 12; % RF feed at node A12

%% Frequency range
df = 0.5*1e4; % 5 kHz resolution
f = 5e6:df:30e6; % 5 MHz to 30 MHz (Fig. 3.13)
w = 2*pi*f;

%% Estimate the leg and stringer resistances, R and r respectively

rhoCu = 17e-9; % electrical resistivity of copper [ohm.m].
skin = sqrt(2.*rhoCu./w./mu0); % skin depth [m]
Surf1 = pi.*(a1.^2-(a1-skin).^2); % surface area of stringer (assumed cylindrical)
Surf2 = pi.*(a2.^2-(a2-skin).^2); % surface area of leg [m2]

% Equivalent Series Resistance of capacitors (section 14.3)
ESR = 0.0064+1.2e-4*f/1e6; % [ohm]

Rsd1 = rhoCu.*H1./Surf1; % stringer conductor resistance [ohm]
Rsd2 = rhoCu.*H2./Surf2; % leg resistance [ohm]

R01 = Rsd1 + ESR; % total stringer resistance
R02 = Rsd2; % leg resistance

Rpul = 0.025; % [ohm/m] resistance per unit length. Fine tuning

R1 = R01 + Rpul*H1; 
R2 = R02 + Rpul*H2; 

%% plots
% Measured real input impedance at the antenna, Fig. 3.13
figure(1)
clf
plot(fm/1e6,Rm,'k','LineWidth',1) % measured real data
hold on
axis([5 30 0.01 300]);

% Antenna input impedance for dissipative model, Fig. 3.13
% (no mutual inductance - see chapter 4)

Z10 = 1i.*w.*L1 - 1i./w./C1 + R1; % Zstr (3.1)
Z20 = 1i.*w.*L2 + R2; % Zleg (3.2)
f0 = 1+Z10./Z20; % cosh(propagation constant) (3.8) and appendix section A.1
gam = acosh(f0); % complex propagation constant 
G = (cosh(gam.*(N-2.*Nf+1))+cosh(gam.*(N-2.*Ng+1)))/2 ...
+cosh(gam.*(N))-cosh(gam.*(N-Ng-Nf+1))-cosh(gam.*(N+Ng-Nf)); % (3.45)
F = 0.5*tanh(gam./2)./sinh(gam.*N); % (3.38)
ZinDiss = Z10.*(Nf-Ng)/2+Z20.*F.*G; % (3.44)
Rdiss = real(ZinDiss);
Xdiss = imag(ZinDiss);
% 
plot(f/1e6,real(ZinDiss),'r','LineWidth',1) % Fig. 3.13
legend('measurement','dissipative model','fontsize',16,'fontname','times')
xlabel('\itf \rm[MHz]','fontsize',16,'fontname','times'), 
ylabel('Re(\itZ\rm)  [\Omega ]','fontsize',16,'fontname','times')
set(gca,'fontname','times','fontsize',16,'LineWidth',1)

%% Plot the normal mode frequency markers (Fig. 3.8)
% (see NormalModeFrequenciesList.txt for list)
%mode number and f = [MHz]
%1   28.6130
plot([28.6130 28.6130],[50 300],'r--','LineWidth',1)
%2   24.7114
plot([24.7114 24.7114],[50 300],'r--','LineWidth',1)
%3   20.7500
plot([20.75 20.75],[150 300],'r--','LineWidth',1)
%4   17.5047
plot([17.5047 17.5047],[250 300],'r--','LineWidth',1)
%5   15.0023
plot([15.0023 15.0023],[100 300],'r--','LineWidth',1)
%6   13.0866
%7   11.6046
plot([11.6046 11.6046],[200 300],'r--','LineWidth',1)
%8   10.4409
plot([10.4409 10.4409],[200 300],'r--','LineWidth',1)
%9    9.5133
plot([9.5133 9.5133],[150 300],'r--','LineWidth',1)
%10    8.7642
plot([8.7642 8.7642],[100 300],'r--','LineWidth',1)
%11    8.1527
plot([8.1527 8.1527],[30 300],'r--','LineWidth',1)
%12    7.6493
plot([7.6493 7.6493],[50 300],'r--','LineWidth',1)
%13    7.2326
plot([7.2326 7.2326],[50 300],'r--','LineWidth',1)
%14    6.8865
plot([6.8865 6.8865],[50 300],'r--','LineWidth',1)
%15    6.5992
plot([6.5992 6.5992],[50 300],'r--','LineWidth',1)
%16    6.3616
plot([6.3616 6.3616],[150 300],'r--','LineWidth',1)
%17    6.1667
plot([6.1667 6.1667],[200 300],'r--','LineWidth',1)
%18    6.0092
plot([6.0092 6.0092],[200 300],'r--','LineWidth',1)
%19    5.8852
plot([5.8852 5.8852],[200 300],'r--','LineWidth',1)
%20    5.7916
plot([5.7916 5.7916],[200 300],'r--','LineWidth',1)
%21    5.7262
plot([5.7262 5.7262],[200 300],'r--','LineWidth',1)
%22    5.6875
plot([5.6875 5.6875],[200 300],'r--','LineWidth',1)
legend('measurement','dissipative model','normal mode frequencies','fontsize',16,'fontname','times')

