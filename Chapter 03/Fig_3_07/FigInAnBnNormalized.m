% FigInAnBn.m
% Fig. 3.7: (a) normal mode current amplitude phasor In for mode m= 6
% (b) An and Bn node voltages
% ... calculated and measured for (a) and for (b)
clear, clc, clf

%% Experimental measurements for Fig. 3.7 (Currents_Voltage.pdf)
% already normalized to values on the RF input, A12:
Iexpt = [-1. -.22 .61 1.05 .77 .01 -.7 -.99 -.73 -.02 .72 1 .69 -.07 -.73 -.96 -.7 0 .72 1.04 .62 -.21 -.95];
Aexpt = [.17 .48 .82 1.0 .9 .58 .21 0.025 .15 .5 .84 1 .86 .51 .16 .025 .21 .575 .91 .99 .83 .485 .18];
Bexpt = [.835 .57 .2 .02 .15 .5 .86 .99 .88 .53 .165 .025 .19 .58 .92 1 .825 .47 .13 .03 .22 .61 .88];
    
N=23; % number of legs
Nf=12; % RF input node
Ng = 8; % ground node A8 (and A16 for the mutual matrix model)
m = 6; % mode number 
n=[1:1:N]; % leg number

figure(1) % Fig. 3.7, 
% no need to take real parts because here, all are real trigonometry values
subplot(211)
m = 6; % mode number 
In = cos((n-1/2).*m*pi/N); % (3.22)
I12 = cos((12-1/2).*m*pi/N); % current on A12
plot(n,Iexpt,'rsq-','MarkerSize',10,'LineWidth',2), hold on
% normalize to current on A12:
plot(n,In/I12,'r+','MarkerSize',10,'LineWidth',2),
legend('\itI_n \rmexpt','\itI_n \rmcalc','location','southeast')
xlabel('node position \itn','interpreter','tex','fontsize',18,'fontname','times')
ylabel('node current [normalized]','interpreter','tex','fontsize',18,'fontname','times')
axis([0. 24 -1.02 1.02])
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes

D = cos((Nf-1/2).*m*pi/N) - cos((Ng-1/2).*m*pi/N); % (3.21)

subplot(212)
An = (cos((n-1/2).*m*pi/N) - cos((Ng-1/2).*m*pi/N))/D; % (3.19)
A12 = (cos((12-1/2).*m*pi/N) - cos((Ng-1/2).*m*pi/N))/D; % voltage on A12
Bn = -(cos((n-1/2).*m*pi/N) + cos((Ng-1/2).*m*pi/N))/D; % (3.20)
plot(n,Aexpt,'bsq-','MarkerSize',10,'LineWidth',2), hold on
% normalize An to voltage on A12:
plot(n,An/A12,'b+','MarkerSize',10,'LineWidth',2),
plot(n,Bexpt,'ksq-','MarkerSize',10,'LineWidth',2),
% normalize Bn to voltage on A12:
plot(n,Bn/A12,'k+','MarkerSize',10,'LineWidth',2),
legend('\itA_n \rmexpt','\itA_n \rmcalc','\itB_n \rmexpt','\itB_n \rmcalc','NumColumns',4)
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes
xlabel('node position \itn','interpreter','tex','fontsize',18,'fontname','times')
ylabel('node voltage [normalized]','interpreter','tex','fontsize',18,'fontname','times')
axis([0 24 -.02 1.02])
