% DissipativeEqCctm6.m for Fig. 3.15, m=6
% from DissipativeImpedanceSpectrum.m, for Fig. 3.13
clear, clc
%% Dimensions

% indices: 1 horizontal (stringer), 2 vertical (leg)

H1 = 0.025; % stringer section spacing [m]
Hcap = 0.0036; % capacitor length [m] to obtain conductor length

H2 = 0.19; % leg effective length [m] (see section 4.4.1)

a1 = 0.006; % stringer strip width [m]
a2 = 0.003; % leg radius [m]

%% Stringer capacitor

C1 = 2.6e-9; %  stringer capacitance 2.6 nF

%% Self inductances
mu0 = 4*pi*1e-7; % permeability of free space [H/m]

% stringer strip self inductance, width a1, see (4.30):
L1 = mu0.*(H1-Hcap)./2./pi.*(log(2.*(H1-Hcap)./a1)+0.5);

% leg cylinder self inductance, radius a2, see (4.29):
L2 = mu0.*H2./2./pi.*(log(2.*H2./a2)-1);

%% Dimensions 

N = 23; % 23 legs

Ng = 8; % ground at node 8 (index "g" for "ground")
% Position Vin (index "f" for "feed")
Nf = 12; % RF feed at node A12

%% Frequency range
df = 0.5*1e3; % 500 Hz resolution
f = 12.5e6:df:13.5e6; % 12.5 MHz to 13.5 MHz (Fig. 3.15 for m=6)
w = 2*pi*f;

%% Estimate the leg and stringer resistances, R and r respectively

rhoCu = 17e-9; % electrical resistivity of copper [ohm.m].
skin = sqrt(2.*rhoCu./w./mu0); % skin depth [m]
Surf1 = pi.*(a1.^2-(a1-skin).^2); % surface area of stringer (assumed cylindrical)
Surf2 = pi.*(a2.^2-(a2-skin).^2); % surface area of leg [m2]

% Equivalent Series Resistance of capacitors (section 14.3)
ESR = 0.0064+1.2e-4*f/1e6; % [ohm]

Rsd1 = rhoCu.*H1./Surf1; % stringer conductor resistance [ohm]
Rsd2 = rhoCu.*H2./Surf2; % leg resistance [ohm]

R01 = Rsd1 + ESR; % total stringer resistance
R02 = Rsd2; % leg resistance

Rpul = 0.025; % [ohm/m] resistance per unit length. Fine tuning

R1 = R01 + Rpul*H1;
R2 = R02 + Rpul*H2;

%% plot the dissipative model impedance
% Antenna input impedance for dissipative model, see also Fig. 3.13
% (no mutual inductance - see chapter 4)

Z10 = 1i.*w.*L1 - 1i./w./C1 + R1; % Zstr (3.1)
Z20 = 1i.*w.*L2 + R2; % Zleg (3.2)
f0 = 1+Z10./Z20; % cosh(propagation constant) (3.8) and appendix section A.1
gam = acosh(f0); % complex propagation constant 
G = (cosh(gam.*(N-2.*Nf+1))+cosh(gam.*(N-2.*Ng+1)))/2 ...
+cosh(gam.*(N))-cosh(gam.*(N-Ng-Nf+1))-cosh(gam.*(N+Ng-Nf)); % (3.45)
F = 0.5*tanh(gam./2)./sinh(gam.*N); % (3.38)
ZinDiss = Z10.*(Nf-Ng)/2+Z20.*F.*G; % (3.44)
Rdiss = real(ZinDiss);
Xdiss = imag(ZinDiss);
% 
subplot(241)
plot(f/1e6,real(ZinDiss),'r','LineWidth',1), hold on % Fig. 3.15
xlabel('\itf \rm[MHz]','fontsize',16,'fontname','times'), 
ylabel('Re(\itZ\rm)  [\Omega ]','fontsize',16,'fontname','times')
set(gca,'fontname','times','fontsize',10,'LineWidth',1)
axis([12.5 13.5 0.01 300]); % to show reduced frequency range,m=6

subplot(245)
plot(f/1e6,imag(ZinDiss),'r','LineWidth',1), hold on
axis([12.5 13.5 -200 200]); % to show reduced frequency range,m=6
xlabel('\itf \rm[MHz]','fontsize',10,'fontname','times'), 
ylabel('Im(\itZ\rm)  [\Omega ]','fontsize',12,'fontname','times')
set(gca,'fontname','times','fontsize',10,'LineWidth',1) % for axes

%% superpose the equivalent circuit impedance 
% for comparison with the dissipative model

IndexPeak = find(Rdiss==max(Rdiss)); % find the peak in the frequency range
fm = f(IndexPeak); % m=6, in this frequency range
wm = 2*pi*fm;
Lstr = L1; %str self inductance does make a difference
r = R1(IndexPeak); % stringer resistance at the peak frequency
C = C1; % stringer capacitance
Lleg = L2; % leg self inductance
R = R2(IndexPeak); % leg resistance at the peak frequency
m=6; % the mode number in the selected frequency range
D = cos((Ng-1/2)*m*pi/N) - cos((Nf-1/2)*m*pi/N); % (3.59)
Rprime = R*(1-wm^2*Lstr*C*(1-r/R*Lleg/Lstr)); % R' (3.47)
fac = 0.5*(1-wm^2*Lstr*C)*D^2/N; % (3.58)
Req = Rprime*fac; % the equivalent circuit resistance (3.55)
Leq = Lleg*fac; % the equivalent circuit inductance (3.56)
Q = wm*Leq/Req; % High Q value for RF circuit (3.54)
Qcheck = wm*Lleg/Rprime; % check same Q in (3.54)
Ceq = C*(1-cos(m*pi/N))/fac/(1-wm^2*Lstr*C); % equivalent cct cap (3.57)
Yeq = (1+2*1i*Q*(f-fm)/fm)/Req/Q^2; % eq cct admittiance (3.53)
Zeq = Z10./2.*(Nf-Ng) + 1./Yeq; % shift in imag part due to series stringers
%Zeq = 1./Yeq; % neglecting the series stringer impedances
% superpose impedances for comparison with the dissipative model:
subplot(241)
plot(f/1e6,real(Zeq),'g--','LineWidth',1)
title(['m = ',num2str(m),';  Q = ',int2str(Q)])

subplot(245)
plot(f/1e6,imag(Zeq),'g--','LineWidth',1)

