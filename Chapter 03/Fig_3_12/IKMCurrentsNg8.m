% IKMcurrents.m 
clear all
clc, clf
N=23;
Ng = 8; % 4 is bad configuration, 8 is good
Nf = 12;
m = 6;
% Data set for paper :%
L=143e-9; R=36e-3; C=2.6e-9; M=7.9e-9; r=2e-3; % components from resonance fit
w=1./sqrt(C.*(M+2.*L.*sin(0.5*m.*pi./N).*sin(0.5*m.*pi./N))); % angular frequency
Z1=1./(i.*w.*C)+i.*w.*M+r; Z2=R+i*w*L;
%: Data set for paper %

f=w/2/pi;
g = -acosh(1+Z1./Z2); % NB negative sign, because:
%acosh(0.6826)=0 + 0.8195i, but
%acosh(0.6826 - 0.0009i)=0.0012 - 0.8195i, i.e. sign change due to small
%negative imaginary term. It has an effect on the sign of alpha.

n1 = 1:1:Ng;
n2 = Ng:1:Nf;
n3 = Nf:1:N;

period = 2*pi/w;
t=period/8; % arbitrary phase
irf = exp(j*w*t)

G1=exp(-g); % forward wave (put the propagation factors in order)
G2=exp(g); % reverse wave (put the propagation factors in order)
B=(-irf/2)*tanh(g/2);
a11=exp(g*(N-Ng+1))-exp(g*(N-Nf+1))+exp(-g*(N-Ng))-exp(-g*(N-Nf));
a11=a11*B/(exp(-g*N)-exp(g*N));
a12=a11/exp(g);
a21=exp(g*(N-Ng+1))-exp(g*(N-Nf+1))+exp(g*(N+Ng))-exp(-g*(N-Nf));
a21=a21*B/(exp(-g*N)-exp(g*N));
a22=exp(-g*(N-Ng+1))-exp(-g*(N-Nf+1))+exp(-g*(N+Ng))-exp(g*(N-Nf));
a22=a22*B/(exp(-g*N)-exp(g*N));
a31=exp(g*(N-Ng+1))-exp(g*(N-Nf+1))+exp(g*(N+Ng))-exp(g*(N+Nf));
a31=a31*B/(exp(-g*N)-exp(g*N));
a32=a31*exp(-g*(2*N+1));

% definition of rung currents (respecting forward,reverse waves):
aahIn1 = a11.*G1.^n1 + a12.*G2.^n1;
aahIn2 = a21.*G1.^n2 + a22.*G2.^n2;
aahIn3 = a31.*G1.^n3 + a32.*G2.^n3;

% continue for stringer currents (need b values)
b11=a11/(1-exp(g));
b12=a12/(1-exp(-g));
b21=a21/(1-exp(g));
b22=a22/(1-exp(-g));
b31=a31/(1-exp(g));
b32=a32/(1-exp(-g));

aahMn1 = b11.*G1.^n1 + b12.*G2.^n1;
aahMn2 = b21.*G1.^n2 + b22.*G2.^n2 -irf/2;
aahMn3 = b31.*G1.^n3 + b32.*G2.^n3;

aahKn1 = -aahMn1;
aahKn2 = -aahMn2-irf;
aahKn3 = -aahMn3;

figure(1)
subplot(2,2,2)
plot(n1,real(aahMn1),'b-*','MarkerSize',6,'LineWidth',1.0), hold on
plot(n2,real(aahMn2),'b-*','MarkerSize',6,'LineWidth',1.0), hold on
plot(n3,real(aahMn3),'b-*','MarkerSize',6,'LineWidth',1.0), hold on
plot(n1,real(aahKn1),'k*-.','MarkerSize',6,'LineWidth',1.0), hold on
plot(n2,real(aahKn2),'k*-.','MarkerSize',6,'LineWidth',1.0), hold on
plot(n3,real(aahKn3),'k*-.','MarkerSize',6,'LineWidth',1.0), hold on
xlabel('stringer number \itn \rm= 1 - 22','interpreter','tex','fontsize',16,'fontname','times')
ylabel('stringer currents [A]','interpreter','tex','fontsize',16,'fontname','times')
text(18.5,21,'(a)','fontname','times','fontsize',16)
axis tight
ylim([-25 25])
xlim([1 22])
set(gca,'fontname','times','fontsize',16,'LineWidth',1.0,'TickLength',[0.02 0.025]) % for axes

% plot the i_rf arrows:
% Jump calculation (equal to irf):
RealJumpI = real(aahMn1(Ng))-real(aahMn2(1))
hI = annotation('arrow');  % store the arrow information in hI
hI.Parent = gca;           % associate the arrow the the current axes
hI.X = [8 8];          % the location in data units
hI.Y = [real(aahMn1(Ng)) real(aahMn2(1))];   
hI.LineWidth  = 2;         
hI.Color = 'b'
hI.HeadWidth  = 10;
hI.HeadLength = 10;
text(8.3,5.5,'\itI_{\rmrf}','fontname','times','fontsize',16,'color','b')

RealJumpII = -real(aahMn2(Nf-Ng+1))+real(aahMn3(1))
hII = annotation('arrow');  % store the arrow information in hII
hII.Parent = gca;           % associate the arrow the the current axes
hII.X = [12 12];          % the location in data units
hII.Y = [real(aahMn2(Nf-Ng+1)) real(aahMn3(1))];   
hII.LineWidth  = 2;         
hII.Color = 'b'
hII.HeadWidth  = 10;
hII.HeadLength = 10;
text(12.3,-9,'\itI_{\rmrf}','fontname','times','fontsize',16,'color','b')

% hyperbolic expressions for rung currents (respecting forward,reverse waves):
F=0.5*tanh(g/2)./sinh(g*N);

ahIn1=irf*F*(cosh(g*(n1-1+Ng-N))+cosh(g*(n1-Ng+N))-cosh(g*(n1-1+Nf-N))-cosh(g*(n1-Nf+N)));
ahIn2=irf*F*(cosh(g*(n2-1+Ng-N))+cosh(g*(n2-Ng-N))-cosh(g*(n2-1+Nf-N))-cosh(g*(n2-Nf+N)));
ahIn3=irf*F*(cosh(g*(n3-1+Ng-N))+cosh(g*(n3-Ng-N))-cosh(g*(n3-1+Nf-N))-cosh(g*(n3-Nf-N)));

% Approximation for In in limit of small alpha:
n=1:N; % all the antenna is the same
alpha=real(g);
approxIn=irf*(j/(alpha*N))*sin(m*pi/(2*N))*cos((n-1/2)*m*pi/N)...
   .*(cos((Ng-1/2)*m*pi/N)-cos((Nf-1/2)*m*pi/N))./cos(m*pi/(2*N));

subplot(2,2,1)
plot(n1,real(ahIn1),'r-*','MarkerSize',6,'LineWidth',1.0), hold on
plot(n2,real(ahIn2),'r-*','MarkerSize',6,'LineWidth',1.0), hold on
plot(n3,real(ahIn3),'r-*','MarkerSize',6,'LineWidth',1.0), hold on
plot(8,real(ahIn2(1)),'ko','MarkerSize',10,'LineWidth',1.0), hold on
plot(12,real(ahIn3(1)),'ko','MarkerSize',10,'LineWidth',1.0), hold on
text(2.5,17,'(a)','fontname','times','fontsize',16)
text(8.5,17,'\itN_g\rm= 8','fontname','times','fontsize',16)
axis tight
ylim([-20 20])
xlabel('leg number \itn \rm= 1 - 23','interpreter','tex','fontsize',16,'fontname','times')
ylabel('leg currents [A]','interpreter','tex','fontsize',16,'fontname','times')
set(gca,'fontname','times','fontsize',16,'LineWidth',1.0,'TickLength',[0.02 0.025]) % for axes

% Impedance calculation
Impedance=(Nf-Ng)*Z1 + Z2*(2*cosh(g*(N-Nf-Ng+1))-2*cosh(g*N)+2*cosh(g*(N-Nf+Ng))...
    -cosh(g*(N-2*Nf+1))-cosh(g*(N-2*Ng+1)) )*tanh(g/2)/(exp(-g*N)-exp(g*N));
Zin=Impedance/2

% Jump calculation (equal to irf):
JumpI = aahMn1(Ng)-aahMn2(1)
RealJumpI = real(aahMn1(Ng))-real(aahMn2(1))
JumpII = -aahMn2(Nf-Ng+1)+aahMn3(1)
RealJumpII = -real(aahMn2(Nf-Ng+1))+real(aahMn3(1))

