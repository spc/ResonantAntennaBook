Run Net2D_Fig14_14.m
The output Fig. 2 is similar to Book Fig. 14.14.

Note that in this example, two peaks are partially hidden in the rising
flank of the peak at 23.9 MHz. There are 15 peaks in total.

Note that the exact spectrum depends on multiple parameters such as:
Inductance of the branches L1 (and Hcap), and L2;
the screen distance Dscreen;
ground position (Nt1, Kt1);
RF feed input (Nf, Kf);
as well as the parameters stated in the book, namely: 
5x5 network with 5cm mesh size, and 1 nF capacitors.
