Hf = 0.025;
H1c = H1/100;
H2c = H2/100;
Wf = (N-1)*H1c*0.55;
Lf = (K-1)*H2c*0.55;

dxc = Wf/100;
dyc = Lf/100;

x0 = (N-1)*H1c/2;
y0 = (K-1)*H2c/2;

xc = (-Wf:dxc:Wf)+eps;
yc = (-Lf:dyc:Lf)+eps;

mu0 = 4*pi*1e-7;

tN0 = 5;    
  

for n = 1:N
       testN0 = n/N;
    
    if testN0*100>=tN0
        tN0;
        tN0 = tN0+5;
    else
    end

    for k = 1:K
        
        xm(n,k) = (n-0.5)*H1c-x0;
        ym(n,k) = (k-1)*H2c-y0;
        
        xi(n,k) = (n-1)*H1c-x0;
        yi(n,k) = (k-0.5)*H2c-y0;
        
        s = find(xc);
        
        for s=min(s):max(s)
            
            u = find(yc);
            
            for u = min(u):max(u)
                
                x = (xc(s)-xm(n,k));
                y = sqrt(Hf.^2+(yc(u)-ym(n,k)).^2)*sign(yc(u)-ym(n,k));
                tet = atan(eps+Hf/(yc(u)-ym(n,k)));
                dBm0 = -mu0*(MM(n+(k-1)*N))/4/pi/y*((x-H1c/2)/sqrt((x-H1c/2).^2+y.^2)-(x+H1c/2)/sqrt((x+H1c/2).^2+y.^2));
                dBmy(s,u) = -dBm0*sin(tet);
                dBmz(s,u) = dBm0*cos(tet);
                
                x = (yc(u)-yi(n,k));
                y = sqrt(Hf.^2+(xc(s)-xi(n,k)).^2)*sign(xc(s)-xi(n,k));
                tet = atan(Hf/(eps+xc(s)-xi(n,k)));
                dBi0 = -mu0*(II(n+(k-1)*N))/4/pi/y*((x-H2c/2)/sqrt((x-H2c/2).^2+y.^2)-(x+H2c/2)/sqrt((x+H2c/2).^2+y.^2));
                dBix(s,u) = -dBi0*sin(tet);
                dBiz(s,u) = dBi0*cos(tet);
                
                if n==1&&k==1
                    
                    Bx(s,u) = dBix(s,u);
                    By(s,u) = dBmy(s,u);
                    Bz(s,u) = dBmz(s,u)+dBiz(s,u);
                    
                else
                    Bx(s,u) = Bx(s,u) + dBix(s,u);
                    By(s,u) = By(s,u) + dBmy(s,u);
                    Bz(s,u) = Bz(s,u) + dBmz(s,u)+dBiz(s,u);
                    AmpB(s,u) = sqrt(Bx(s,u).*conj(Bx(s,u))+By(s,u).*conj(By(s,u))+Bz(s,u).*conj(Bz(s,u)));
                    
                end
                
            end
        end
        
        
        
    end
end


figure(5), clf
% hold
% 
% for n=1:N
%    
%     xp = [xi(n,1) xi(n,1)];
%     yp = [yi(n,1)-H2c/2 yi(n,1)+H2c/2];
%     
%     figure(5)
%     plot(yp,xp,'k','LineWidth',1.5)
%     
% end
% 
% 
% for n=1:N-1
%     
%     xp = [xm(n,1)-H1c/2 xm(n,1)+H1c/2];
%     yp = [ym(n,1) ym(n,1)];
%     figure(5)
%     plot(yp,xp,'k','LineWidth',1.5)
% end
% 
% 
% for n=1:N-1
%     
%     xp = [xm(n,2)-H1c/2 xm(n,2)+H1c/2];
%     yp = [ym(n,2) ym(n,2)];
%     figure(5)
%     plot(yp,xp,'k','LineWidth',1.5)
% end
colormap hot
surf((xc+Wf)*100,(yc+Wf)*100,AmpB')
shading flat
xlabel('cm')
ylabel('cm')

view(0,90)
axis equal

% figure(6)
% % clf
% plot(xc,AmpB(:,round(length(yc)/2)),'k','LineWidth',3)
% title('Bx profile')
% xlabel('x [m]')
% grid
