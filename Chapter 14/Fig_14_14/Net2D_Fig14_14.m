clear
clc

%% Mesh dimensions, distance to screen 

% indices: 1 horizontal, 2 vertical

% Attention length units: cm
H1 = 5;

% Hcap: correction due to the size of the capacitors (ceramic) 

Hcap = 1.36;

H2 = 5;

a1 = 0.3;
a2 = 0.3;
Dscreen = 5;

%% Capas

C1 = 1000e-12;
C2 = 1000;

%% Selfs

mu0 = 4*pi*1e-7;

% to be used for wires stringers/legs (a1,2=radius)
 
%L1 = mu0.*((H1)/100)./2./pi.*(log(2.*(H1)./a1)-1); % originally
%L2 = mu0.*((H2)/100)./2./pi.*(log(2.*(H2)./a2)-1); % originally

% to be used for strips stringers/legs (a1,2=width)
% 
 L1a = mu0.*((H1-Hcap)./100)./2./pi.*(log(2.*(H1-Hcap)./a1)+0.5);
 dAB = eps;
 delAB = Hcap;
 alAB = H1-Hcap+delAB;
 betAB = (H1-Hcap)/2+delAB;
 gamAB = (H1-Hcap)/2+delAB;
% 
 muAB = 1e-9.*(alAB.*asinh(alAB./dAB) - betAB.*asinh(betAB./dAB) - gamAB.*asinh(gamAB./dAB) + delAB.*asinh(delAB./dAB) - ...
     sqrt(alAB.^2 + dAB^2)+sqrt(betAB.^2 + dAB^2)+sqrt(gamAB.^2 + dAB^2)-sqrt(delAB.^2 + dAB^2));
% 
 L1 = (L1a+2.*muAB); % accounts for strip and capacitor
 L2 = mu0.*((H2)./100)./2./pi.*(log(2.*(H2)./a2)+0.5); % strip


%% Dimensions

N = 5;
K = 5;

%% Feeding

% ground positions
Nt1 = 1;
Kt1 = 1;
Nt2 = 1;
Kt2 = 2;
% number of grounds (max 2)
nbt = 1;
% Position Vin
Nf = N-1;
Kf = N;
%% Definition vector S

Iin = 1;
Pin = 100;

if nbt==1
    
    for k = 1:(N*K)
        
        
        if k==((Kf-1)*N+Nf)
            
            SS(k) = Iin;
        elseif k==((Kt1-1)*N+Nt1)
            SS(k) = -Iin;
        else
            SS(k) = 0;
        end
    end
    
else
    
    for k = 1:(N*K)
        
        
        if k==((Kf-1)*N+Nf)
            
            SS(k) = Iin;
        elseif k==((Kt1-1)*N+Nt1)
            SS(k) = -Iin/2;
        elseif k==((Kt2-1)*N+Nt2)
            SS(k) = -Iin/2;
        else
            SS(k) = 0;
        end
    end
end
[DD,VV]=size(SS);
if DD==1
    SS=SS';
else
end

%% Definition Matrices S1, SN, U1, UN and transpositions

m = 1;

for k = 1:(N*K)
    
    for j = 1:(N*K)
        
        if j==k;
            
            Id(k,j) = 1;
            
        else
            Id(k,j) = 0;
        end
    end
end

m = 1;

for k = 1:(N*K)
    
    for j = 1:(N*K)
        
        if j==k-1
            
            if j==m*N
                S1(k,j) = 0;
                m = m+1;
            else
                S1(k,j) = 1;
            end
        else
            S1(k,j) = 0;
        end
    end
end

m = 1;

for k = 1:(N*K)
    
    for j = 1:(N*K)
        
        if j==k-N
            SN(k,j) = 1;
        else
            SN(k,j) = 0;
        end
    end
end

U1 = Id-S1;
UN = Id-SN;
U1T = transpose(U1);
UNT = transpose(UN);
S1T = transpose(S1);
SNT = transpose(SN);


%% Definition Matrices Self avec self des branches hypothetiques (LEnds) �lev�e

LEnds = 1i*1e2;

MUT = 1;

if MUT==1
    
    CalcMatMut
    CalcMatScreen2
    
else
    
    CalcMatDiag
    
end


%% Frequency range

df = 5e4;
f = 5e6:df:32e6;
w = 2*pi*f;

%% Branch resistances

rhoCu = 17e-9;
skin = sqrt(2.*rhoCu./w./mu0);
Surf1 = pi.*((a1/100).^2-((a1/100)-skin).^2);
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2);

ESR = 0.015+0.8e-4*f/1e6;
ESRmes = 0.07;


Rsd1 = rhoCu.*H1/100./Surf1;
Rsd2 = rhoCu.*H2/100./Surf2;

R01 = Rsd1 + ESR;
R02 = Rsd2;

Rp = 0.01;

R1 = R01 + Rp*H1/100;
R2 = R02 + Rp*H2/100;


%% Calculate impedance as a function of frequency

p = find(w);
tN0 = 5;
Lfr = length(f);
for p = min(p):max(p)
    
    testN0 = p/Lfr;
    
    if testN0*100>=tN0
        tN0
        tN0 = tN0+5;
    else
    end
    
    % matrix impedances
    Z1 = 1i*w(p)*mu1 + (R1(p)-1i/w(p)/C1)*Id;
    Z2 = 1i*w(p)*mu2 + (R2(p)-1i/w(p)/C2)*Id;
    
    % general equation
    Op = U1/Z1*U1T+UN/Z2*UNT;
    AA = Op\SS;
    
    % definition Zin=DeltaV/Iin for Iin = 1A;
    if nbt==1
        Zin(p) = AA((Kf-1)*N+Nf)-AA((Kt1-1)*N+Nt1);
        
    else
        Zin(p) = AA((Kf-1)*N+Nf)-AA((Kt1-1)*N+Nt1)/2-AA((Kt2-1)*N+Nt2)/2;
        
    end
    
end

%% connection parameters (TL plus self)
dTL = 0.0001;
dconS = 0.08;
aconS = 0.003;
Rin = real(Zin);
Xin = imag(Zin);
ZConectic
% plots

figure(2)
clf
semilogy(f/1e6,Rin2,'b','LineWidth',2)
hold
xlabel('[MHz]')
ylabel('[\Omega]')
grid



%% onefrequ: calculations and plots for a given frequency
%oneFrequ  % commented out for Fig. 14.14