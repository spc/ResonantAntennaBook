LconS = mu0.*(dconS)./2./pi.*(log(2.*(dconS)./aconS)-1);

ZconS = w.*LconS;
Xinc = Xin+ZconS;
Z0 = 50;
Zinc = Rin + 1i*Xinc;

G0TL = (Zinc-Z0)./(Zinc+Z0);

c = 3e8;
betTL = w./(0.7.*c);

GTL = G0TL.*exp(-1i.*2.*betTL.*dTL);

GrTL = real(GTL);
GxTL = imag(GTL);

Rin2 = Z0.*(1-GrTL.^2-GxTL.^2)./((1-GrTL).^2+GxTL.^2);
Xin2 = Z0.*(2.*GxTL)./((1-GrTL).^2+GxTL.^2);
Zin2 = Rin2+1i*Xin2;