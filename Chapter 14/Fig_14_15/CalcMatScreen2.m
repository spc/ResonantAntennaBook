
m = 1;

for n = 1:K*N
    
    if n==m*N
        
       conv(n) = 1+(cos(pi*(n-0.5)/N).^2+0.5)/1.5*(cos(pi*(m-0.5)/K).^2+0.5)/1.5;
        m = m+1;
        
    else
        
        conv(n) = 1+(cos(pi*(n-0.5)/N).^2+0.5)/1.5*(cos(pi*(m-0.5)/K).^2+0.5)/1.5;
        
    end
end
Dscreen2 = Dscreen.*conv;

for n = 1:K*N
    
    for k = 1:K*N
        
        
        if (x(n)-x(k))==0
            
            d1 = sqrt(abs(y(n)-y(k)).^2 + (2*Dscreen2(k)).^2)+eps;
            del1aa = -(H1-Hcap)/2;
            del1ab = Hcap;
            del1ba = Hcap;
            
            al1aa = (H1-Hcap)+del1aa;
            bet1aa = (H1-Hcap)/2+del1aa;
            gam1aa = (H1-Hcap)/2+del1aa;
            
            al1ab = (H1-Hcap)+del1ab;
            bet1ab = (H1-Hcap)/2+del1ab;
            gam1ab = (H1-Hcap)/2+del1ab;
            
            al1ba = (H1-Hcap)+del1ba;
            bet1ba = (H1-Hcap)/2+del1ba;
            gam1ba = (H1-Hcap)/2+del1ba;
            
            
            
            mu1aa = 1e-9.*(al1aa.*asinh(al1aa./d1) - bet1aa.*asinh(bet1aa./d1) - gam1aa.*asinh(gam1aa./d1) + del1aa.*asinh(del1aa./d1) - ...
                sqrt(al1aa.^2 + d1^2)+sqrt(bet1aa.^2 + d1^2)+sqrt(gam1aa.^2 + d1^2)-sqrt(del1aa.^2 + d1^2));
            
            mu1ab = 1e-9.*(al1ab.*asinh(al1ab./d1) - bet1ab.*asinh(bet1ab./d1) - gam1ab.*asinh(gam1ab./d1) + del1ab.*asinh(del1ab./d1) - ...
                sqrt(al1ab.^2 + d1^2)+sqrt(bet1ab.^2 + d1^2)+sqrt(gam1ab.^2 + d1^2)-sqrt(del1ab.^2 + d1^2));
            
            mu1ba = 1e-9.*(al1ba.*asinh(al1ba./d1) - bet1ba.*asinh(bet1ba./d1) - gam1ba.*asinh(gam1ba./d1) + del1ba.*asinh(del1ba./d1) - ...
                sqrt(al1ba.^2 + d1^2)+sqrt(bet1ba.^2 + d1^2)+sqrt(gam1ba.^2 + d1^2)-sqrt(del1ba.^2 + d1^2));
            
          
                muIm1(n,k) = (2*mu1aa + mu1ba + mu1ab);
           
            
        else
            
            
            d1 = sqrt(abs(y(n)-y(k)).^2 + (2*Dscreen2(k)).^2)+eps;
            del1aa = abs(x(n)-x(k))-(H1-Hcap)/2;
            del1ab = abs(x(n)-x(k))+Hcap;
            del1ba = abs(x(n)-x(k))-H1;
            
            al1aa = (H1-Hcap)+del1aa;
            bet1aa = (H1-Hcap)/2+del1aa;
            gam1aa = (H1-Hcap)/2+del1aa;
            
            al1ab = (H1-Hcap)+del1ab;
            bet1ab = (H1-Hcap)/2+del1ab;
            gam1ab = (H1-Hcap)/2+del1ab;
            
            al1ba = (H1-Hcap)+del1ba;
            bet1ba = (H1-Hcap)/2+del1ba;
            gam1ba = (H1-Hcap)/2+del1ba;
            
            
            
            mu1aa = 1e-9.*(al1aa.*asinh(al1aa./d1) - bet1aa.*asinh(bet1aa./d1) - gam1aa.*asinh(gam1aa./d1) + del1aa.*asinh(del1aa./d1) - ...
                sqrt(al1aa.^2 + d1^2)+sqrt(bet1aa.^2 + d1^2)+sqrt(gam1aa.^2 + d1^2)-sqrt(del1aa.^2 + d1^2));
            
            mu1ab = 1e-9.*(al1ab.*asinh(al1ab./d1) - bet1ab.*asinh(bet1ab./d1) - gam1ab.*asinh(gam1ab./d1) + del1ab.*asinh(del1ab./d1) - ...
                sqrt(al1ab.^2 + d1^2)+sqrt(bet1ab.^2 + d1^2)+sqrt(gam1ab.^2 + d1^2)-sqrt(del1ab.^2 + d1^2));
            
            mu1ba = 1e-9.*(al1ba.*asinh(al1ba./d1) - bet1ba.*asinh(bet1ba./d1) - gam1ba.*asinh(gam1ba./d1) + del1ba.*asinh(del1ba./d1) - ...
                sqrt(al1ba.^2 + d1^2)+sqrt(bet1ba.^2 + d1^2)+sqrt(gam1ba.^2 + d1^2)-sqrt(del1ba.^2 + d1^2));
            
             
                muIm1(n,k) = (2*mu1aa + mu1ba + mu1ab);
            
            
        end
        
        
        
        d2 = sqrt(abs(x(n)-x(k)).^2 + (Dscreen2(k)*2).^2)+eps;
        del2 = abs(y(n)-y(k))-(H2);
        al2 = 2*(H2)+del2;
        bet2 = (H2)+del2;
        gam2 = (H2)+del2;
        
       
        muIm2(n,k) = 1e-9.*(al2.*asinh(al2./d2) - bet2.*asinh(bet2./d2) - gam2.*asinh(gam2./d2) + del2.*asinh(del2./d2) - ...
            sqrt(al2.^2 + d2^2)+sqrt(bet2.^2 + d2^2)+sqrt(gam2.^2 + d2^2)-sqrt(del2.^2 + d2^2));
        
    end
end

mu1 = mu1-muIm1;
mu2 = mu2-muIm2;