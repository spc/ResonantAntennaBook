Run Net2D_Fig14_15.m
The output Figs. 3 and 5 are similar to Book Fig. 14.15.
Note that in this example, two peaks are partially hidden in the rising
flank of the peak at 23.9 MHz. There are 15 peaks in total.

To obtain similar upper figures of current distribution and B field,
input the frequency 8.1 MHz at the prompt of Net2D_Fig14_15.m.
This corresponds to the p=1 mode frequency.

To obtain similar lower figures of current distribution and B field,
input the frequency 27.95 MHz at the prompt of Net2D_Fig14_15.m.
This corresponds to the p=15 mode frequency.


(Note that the exact results depends on multiple parameters such as:
Inductance of the branches L1 (and Hcap), and L2;
the screen distance Dscreen;
ground position (Nt1, Kt1);
RF feed input (Nf, Kf);
as well as the parameters stated in the book, namely: 
5x5 network with 5cm mesh size, and 1 nF capacitors.)
