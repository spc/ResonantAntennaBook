Run Net2D_Fig14_16.m
The output Fig. 5 for each mode p frequency is similar to Book Fig. 14.16.
Note that in this example, two peaks are partially hidden in the rising
flank of the peak at 23.9 MHz. There are 15 peaks in total.

For p=1, input frequency 8.1 MHz at the prompt of Net2D_Fig14_16.m.
For p=15, input frequency 27.95 MHz at the prompt of Net2D_Fig14_16.m.
These correspond to Fig. 14.15.
For the intermediate modes, select the corresponding p number frequency in MHz
from the spectrum in output Fig. 2.
For the prompt "Again?" insert "1".

If a field plot appears distorted, it is necessary, for example, to change the RF feed positions (Nf,Kf) to avoid particularly-low mode impedance, or closely-similar mode frequencies.

(Note that the exact results depends on multiple parameters such as:
Inductance of the branches L1 (and Hcap), and L2;
the screen distance Dscreen;
ground position (Nt1, Kt1);
RF feed input (Nf, Kf);
as well as the parameters stated in the book, namely: 
5x5 network with 5cm mesh size, and 1 nF capacitors.)
