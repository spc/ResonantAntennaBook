f = input('frequency:')*1e6;
w = 2*pi*f;

skin = sqrt(2.*rhoCu./w./mu0);
Surf1 = pi.*((a1/100).^2-((a1/100)-skin).^2);
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2);

Rsd1 = rhoCu.*H1/100./Surf1;
Rsd2 = rhoCu.*H2/100./Surf2;
ESR = 0.0064+1.2e-4*f/1e6;
R01 = Rsd1 + ESR;
R02 = Rsd2;

R1 = R01 + Rp*H1/100;
R2 = R02 + Rp*H2/100;

Z1 = 1i*w*mu1 + (R1-1i/w/C1)*Id;
Z2 = 1i*w*mu2 + (R2-1i/w/C2)*Id;

Op = U1*inv(Z1)*U1T+UN*inv(Z2)*UNT;
AA = inv(Op)*SS;
% definition Zin=DeltaV/Iin avec Iin = 1A;
if nbt==1
    Zin = AA((Kf-1)*N+Nf)-AA((Kt1-1)*N+Nt1);
    
else
    Zin = AA((Kf-1)*N+Nf)-AA((Kt1-1)*N+Nt1)/2-AA((Kt2-1)*N+Nt2)/2;
    
end

Rin = real(Zin);
Xin = imag(Zin);
Iin = sqrt(Pin/Rin);

if nbt==1
    
    for k = 1:(N*K)
        
        
        if k==((Kf-1)*N+Nf)
            
            SS(k) = Iin;
        elseif k==((Kt1-1)*N+Nt1)
            SS(k) = -Iin;
        else
            SS(k) = 0;
        end
    end
    
else
    
    for k = 1:(N*K)
        
        
        if k==((Kf-1)*N+Nf)
            
            SS(k) = Iin;
        elseif k==((Kt1-1)*N+Nt1)
            SS(k) = -Iin/2;
        elseif k==((Kt2-1)*N+Nt2)
            SS(k) = -Iin/2;
        else
            SS(k) = 0;
        end
    end
end
[DD,VV]=size(SS);
if DD==1
    SS=SS';
else
end

AA = Op\SS;
II = Z2\UNT*AA;
MM = Z1\U1T*AA;

for m=1:K
    
    Aint = AA((m-1)*N+1:m*N);
    if m==1
        Am = Aint;
    else
        Am = [Am Aint];
    end
end

for m=1:K
    
    Aint = II((m-1)*N+1:m*N);
    if m==1
        Im = Aint;
    else
        Im = [Im Aint];
    end
end

for m=1:K
    
    Aint = MM((m-1)*N+1:m*N);
    if m==1
        Mm = Aint;
    else
        Mm = [Mm Aint];
    end
end

% figure(2)
% clf
% mesh(1:K,1:N,abs(Am-AA(Nt1)))
% grid
% xlabel('n')
% ylabel('k')


figure(1)
clf
% plot(1:N*K,abs(MM),'k-*'), grid
% hold
plot((1:N*(K-1)),abs(II(1:N*(K-1))),'r-*'), grid

figure(4)
clf
% plot(1:N*K,abs(MM),'k-*'), grid
% hold
plot((1:N*(K)),abs(AA(1:N*(K))),'r-*'), grid


for n=1:(N-1)
    
    for k=1:(K)
        
        Mx(n,k) = Mm(n,k);
        My(n,k) = 0;
        
    end
end


for n=1:(N)
    
    for k=1:(K-1)
        
        Iy(n,k) = Im(n,k);
        Ix(n,k) = 0;
    end
end


figure(3)
clf

for n = 1:N
    if n==1
        y0 = [0 (K-1)*H2];
        x0 = [(n-1)*H1 (n-1)*H1];
        plot(x0,y0,'g','LineWidth',2)
        hold
    else
        y0 = [0 (K-1)*H2];
        x0 = [(n-1)*H1 (n-1)*H1];
        plot(x0,y0,'g','LineWidth',2)
    end
end

for k = 1:K
    
    x0 = [0 (N-1)*H1];
    y0 = [(k-1)*H2 (k-1)*H2];
    plot(x0,y0,'g','LineWidth',2)
    
end

pS = 0.45;

if K==2
    YY = 1:1:N;
    quiver(((1:(N-1))-0.5)*H1,((1:(K))-1)*H2,imag(Mx'),imag(My'),pS,'r','LineWidth',5)
    quiver(((1:(N))-1)*H1,(YY./YY-0.5)*H2,imag(Ix'),imag(Iy'),pS,'k','LineWidth',5)
else
    quiver(((1:(N-1))-0.5)*H1,((1:(K))-1)*H2,imag(Mx'),imag(My'),pS,'r','LineWidth',5)
    quiver(((1:(N))-1)*H1,((1:(K-1))-0.5)*H2,imag(Ix'),imag(Iy'),pS,'k','LineWidth',5)
end
axis equal
xlabel('cm')
ylabel('cm')

grid

fieldCalc

another = input('again?:');
if another==1
    oneFrequ
else
end