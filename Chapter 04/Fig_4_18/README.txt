All of the following files must be downloaded into the same folder.
The matrix calculations are based on a 2D treatment, see section 14.2.

First, run  Net2DA8A16mutualsFIT.m
which creates the data file AnMutA8A16.mat  using:
CalcMatMut.m  for the matrix of mutual partial inductances in the antenna,
CalcMatScreen.m  for the matrix of mutual partial inductances with screen images,
and oneFrequA8A16.m  which generates the data file  AnMutA8A16.mat
for the frequency of mode m=6, as confirmed by the experimental data in
A8A16.DAT (see Figure 20 of the Matlab output)

Second, run FigInAnBnNormalized.m
which reproduces Fig. 3.7 (see Figure 1 of the Matlab output), and adds the mutual partial inductance calculation
AnMutA8A16.mat  generated as described above.
The combination reproduces Fig. 4.18 as required (see Figure 2 of the Matlab output).

