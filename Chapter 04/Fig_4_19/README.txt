All of the following files must be downloaded into the same folder.
The matrix calculations are based on a 2D treatment, see section 14.2.

First, run  Net2DA9distortion.m
which uses
CalcMatMut.m,
CalcMatScreen.m,
to calculate and plot Fig. 11 of the Matlab output, the impedance resonance curve.
The voltage distributions below, at, and above resonance are calculated in
oneFrequA9below.m (see Fig. 19 of the Matlab output),
oneFrequA9.m (see Fig. 20 of the Matlab output),
oneFrequA9above.m (see Fig. 21 of the Matlab output),
and this calculated data is stored in  A9OffResonance.mat.

Second, run  PlotOffResonance.m, to obtain Fig. 4 of the Matlab output,
which re-plots the impedance curve Fig. 11 of the Matlab output,
and compares the voltage distributions with the experimental data of
Fig2_expt_Graph6.dat,  Fig3_expt_Graph7.dat, and  Fig4_expt_Graph8.dat.
Fig. 4 of the Matlab output is the required graph, Fig. 4.19 in the book.