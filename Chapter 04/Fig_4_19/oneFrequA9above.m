% oneFrequA9above
%f = input('frequency:')*1e6;
fabove = (11.817+0.1)*1e6; % frequency above resonance
w = 2*pi*fabove;

skin = sqrt(2.*rhoCu./w./mu0);
Surf1 = pi.*((a1/100).^2-((a2/100)-skin).^2);
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2);

R01 = rhoCu.*H1/100./Surf1;
R02 = rhoCu.*H2/100./Surf2;

R1 = R01 + Rp*H1/100;
R2 = R02 + Rp*H2/100;

Z1 = 1i*w*mu1 + (R1-1i/w/C1)*Id;
Z2 = 1i*w*mu2 + (R2-1i/w/C2)*Id;

IZ1 = Z1\Id;
IZ2 = Z2\Id;
Op = U1*IZ1*U1T+UN*IZ2*UNT;
AA = Op\SS;

MM = IZ1*U1T*AA;
II = IZ2*UNT*AA;

for m=1:K
    
    Aint = AA((m-1)*N+1:m*N);
    if m==1
        Am = Aint;
    else
        Am = [Am Aint];
    end
end

for m=1:K
    
    Aint = II((m-1)*N+1:m*N);
    if m==1
        Im = Aint;
    else
        Im = [Im Aint];
    end
end

for m=1:K
    
    Aint = MM((m-1)*N+1:m*N);
    if m==1
        Mm = Aint;
    else
        Mm = [Mm Aint];
    end
end

% plot and save the An node voltages at the chosen frequency:
figure(21)
clf
% normalize to A12, then take real part:
AnMutA9above = real((Am(1:N)-Am(Nt1))./(Am(12)-Am(Nt1)));
plot(1:N,AnMutA9above,'k-o','LineWidth',2)
xlabel('node position \itn','interpreter','tex','fontsize',20,'fontname','times')
ylabel('node voltage [normalized]','interpreter','tex','fontsize',20,'fontname','times')
%save('AnMutA9above','AnMutA9above') % save the An voltage node data
% complete the plot using FigOffResonance.m
