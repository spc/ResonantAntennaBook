 % FigOffResonance.m
clear
figure(4), clf
load('A9OffResonance.mat') % load the data from Net2DA9distortion.m
% A9OffResonance.mat contains f,Rin2,AnMutA9below,AnMutA9res,AnMutA9above
% resonance curve first:
subplot(212)
plot(f/1e6,Rin2,'k-','LineWidth',2), hold on
v=[0 90]; 
f0=11.817; % resonance frequency [MHz]
marka=(f0-0.1)*[1 1];
plot(marka,v,'k-','MarkerSize',6,'LineWidth',2), hold on
annotation('arrow',[0.252 0.252],[0.31 0.49],'LineWidth',2)
text(11.695,105,'(a) f_0 - 0.1 MHz','fontsize',14,'fontname','times')

markb=f0*[1 1];
plot(markb,v,'k-','MarkerSize',6,'LineWidth',2), hold on
annotation('arrow',[0.509 0.509],[0.31 0.49],'LineWidth',2)
text(11.807,105,'(b) f_0','fontsize',14,'fontname','times')

markc=(f0+0.1)*[1 1];
plot(markc,v,'k-','MarkerSize',6,'LineWidth',2), hold on
annotation('arrow',[0.768 0.768],[0.31 0.49],'LineWidth',2)
text(11.895,105,'(c) f_0 + 0.1 MHz','fontsize',14,'fontname','times')
set(gca,'fontname','times','fontsize',14,'LineWidth',1,'TickLength',[0.01 0.025]) % for axes
xlabel('frequency [MHz]','interpreter','tex','fontsize',14,'fontname','times')
ylabel('real impedance [\Omega]','interpreter','tex','fontsize',14,'fontname','times')
axis([(11.82-0.15) (11.82+0.15) 0 210]);

%% Now the voltage distributions:
% below resonance:
Expt2=load('Fig2_expt_Graph6.dat');
n=[1:23];
ex2=Expt2(:,4);
subplot(231)
plot(n,ex2,'b-s','MarkerSize',6,'LineWidth',1.5), hold on
plot(n,AnMutA9below,'ro','MarkerSize',6,'MarkerFaceColor','r','LineWidth',1.5), hold on
text(8,1.17,'(a) f_0 - 0.1 MHz','fontsize',14,'fontname','times')
%legend('model','expt')
axis([0 24 -.22 1.32])
set(gca,'fontname','times','fontsize',14,'LineWidth',1,'TickLength',[0.02 0.025]) % for axes
xlabel('node number \itn','interpreter','tex','fontsize',14,'fontname','times')
ylabel('node voltage [normalized]','interpreter','tex','fontsize',14,'fontname','times')

% at resonance:
Expt3=load('Fig3_expt_Graph7.dat');
ex3=Expt3(:,4);
subplot(232)
plot(n,ex3,'b-s','MarkerSize',6,'LineWidth',1.5), hold on
plot(n,AnMutA9res,'ro','MarkerSize',6,'MarkerFaceColor','r','LineWidth',1.5), hold on
%legend('model','expt')
text(9,1.17,'(b) f_0','fontsize',14,'fontname','times')
axis([0 24 -.22 1.32])
set(gca,'fontname','times','fontsize',14,'LineWidth',1,'TickLength',[0.02 0.025]) % for axes
xlabel('node number \itn','interpreter','tex','fontsize',14,'fontname','times')
ylabel('node voltage [normalized]','interpreter','tex','fontsize',14,'fontname','times')

% above resonance:
Expt4=load('Fig4_expt_Graph8.dat');
ex4=Expt4(:,4);
subplot(233)
plot(n,ex4,'b-s','MarkerSize',6,'LineWidth',1.5), hold on
plot(n,AnMutA9above,'ro','MarkerSize',6,'MarkerFaceColor','r','LineWidth',1.5), hold on
legend('model','expt','location','SouthEast')
text(6,1.17,'(c) f_0 + 0.1 MHz','fontsize',14,'fontname','times')
axis([1 23 -.22 1.32])
set(gca,'fontname','times','fontsize',14,'LineWidth',1,'TickLength',[0.02 0.025]) % for axes
xlabel('node number \itn','interpreter','tex','fontsize',14,'fontname','times')
ylabel('node voltage [normalized]','interpreter','tex','fontsize',14,'fontname','times')
