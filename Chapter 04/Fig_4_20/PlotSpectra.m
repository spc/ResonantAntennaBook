% PlotSpectra.m
% Fig. 4.20: (a) Ground at A8 only
% (b) Ground at A8 and A16
clear, clc, clf
figure(4)
%% Fig. 4.20(a) Ground at A8 only
% Load the experimental data
AAA = load('A8.dat'); % network analyser measurement data file
fm = AAA(:,1); % measured frequency
Rm = AAA(:,2); % measured resistance Re(Zin)
Xm = AAA(:,3); % measured reactance Im(Zin)
load('A8'); % model data, saved by Net2DA8mutualsFIT.m

subplot(4,1,1)
plot(fm/1e6,Rm,'k.','LineWidth',0.5), hold on % measurement
plot(f/1e6,Rin2,'r','LineWidth',1) % calculation
axis([8 24 -2 300]);
xlabel('\itf \rm[MHz]')
ylabel('Re(\itZ\rm) [\Omega]')

plot([22.97 22.97],[-300 300],'k'), plot([20.34 20.34],[-300 220],'k')
text(21.9,60,'\it{m}\rm=1'), text(20.44,60,'2')
plot([17.91 17.91],[-300 220],'k'), plot([16.00 16.00],[-300 220],'k')
text(18.01,60,'3'), text(16.1,60,'4')
plot([14.52 14.52],[-300 300],'k'), plot([13.42 13.42],[-300 300],'k')
text(14.62,60,'5'), text(13.52,60,'6')
plot([12.55 12.55],[-300 300],'k'), plot([11.85 11.85],[-300 300],'k')
text(12.65,60,'7'), text(11.95,60,'8')
plot([11.27 11.27],[-300 300],'k'), plot([10.79 10.79],[-300 300],'k')
text(11.37,60,'9'), %text(10.8,60,'..')
plot([10.40 10.40],[-300 300],'k'), plot([10.05 10.05],[-300 300],'k')
plot([9.77 9.77],[-300 300],'k'), plot([9.54 9.54],[-300 300],'k')
plot([9.34 9.34],[-300 300],'k'), plot([9.15 9.15],[-300 300],'k')
plot([9.02 9.02],[-300 300],'k'), plot([8.88 8.88],[-300 300],'k')
plot([8.785 8.785],[-300 300],'k'), plot([8.727 8.727],[-300 300],'k')
plot([8.68 8.68],[-300 300],'k'), plot([8.64 8.64],[-300 300],'k')
text(8.06,60,'22'),
text(15,250,'(a) RF at A12, ground at A8')

subplot(4,1,2)
plot(fm/1e6,Xm,'k.','LineWidth',0.5), hold on % measurement
plot(f/1e6,Xin2,'r','LineWidth',1) % calculation
axis([8 24 -150 150]);
xlabel('\itf \rm[MHz]')
ylabel('Im(\itZ\rm) [\Omega]')
legend('measurement','matrix model','location','northeast')

%% Fig. 4.20(b) Ground at A8 and A16
% Load the experimental data
AAA = load('A8A16.dat'); % network analyser measurement data file
fm = AAA(:,1); % measured frequency
Rm = AAA(:,2); % measured resistance Re(Zin)
Xm = AAA(:,3); % measured reactance Im(Zin)
load('A8A16'); % model data, saved by Net2DA8mutualsFIT.m
subplot(4,1,3)
plot(fm/1e6,Rm,'k.','LineWidth',0.5), hold on % measurement
plot(f/1e6,Rin2,'r','LineWidth',1) % calculation
axis([8 24 -2 300]);
xlabel('\itf \rm[MHz]')
ylabel('Re(\itZ\rm) [\Omega]')

text(15,250,'(b) RF at A12, ground at A8 and A16')
%plot([22.97 22.97],[-300 300],'k'), text(21.9,60,'\it{m}\rm=1'),
plot([20.34 20.34],[-300 220],'k'), text(20.44,60,'2')
%plot([17.91 17.91],[-300 300],'k'), text(18.01,60,'3'),
plot([16.00 16.00],[-300 220],'k'), text(16.1,60,'4')
%plot([14.52 14.52],[-300 300],'k'), text(14.62,60,'5'),
plot([13.42 13.42],[-300 300],'k'), text(13.52,60,'6')
%plot([12.55 12.55],[-300 300],'k'), text(12.65,60,'7'),
plot([11.85 11.85],[-300 300],'k'), text(11.95,60,'8')
%plot([11.27 11.27],[-300 300],'k'), text(11.37,60,'9'),
plot([10.79 10.79],[-300 300],'k'), text(10.8,60,'10')
%plot([10.40 10.40],[-300 300],'k'), 
plot([10.05 10.05],[-300 300],'k')
%plot([9.77 9.77],[-300 300],'k'), 
plot([9.54 9.54],[-300 300],'k')
%plot([9.34 9.34],[-300 300],'k'), 
plot([9.15 9.15],[-300 300],'k')
%plot([9.02 9.02],[-300 300],'k'), 
plot([8.88 8.88],[-300 300],'k')
%plot([8.785 8.785],[-300 300],'k'), 
plot([8.727 8.727],[-300 300],'k')
%plot([8.68 8.68],[-300 300],'k'), 
plot([8.64 8.64],[-300 300],'k'), text(8.06,60,'22'),


subplot(4,1,4)
plot(fm/1e6,Xm,'k.','LineWidth',0.5) % measurement
hold
plot(f/1e6,Xin2,'r','LineWidth',1) % calculation
axis([8 24 -150 150]);
xlabel('\itf \rm[MHz]')
ylabel('Im(\itZ\rm) [\Omega]')
legend('measurement','matrix model','location','northeast')


%set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes
%xlabel('node position \itn','interpreter','tex','fontsize',18,'fontname','times')
%ylabel('node voltage [normalized]','interpreter','tex','fontsize',18,'fontname','times')
%axis([0.9 23 -.02 1.02])