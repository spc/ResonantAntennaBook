% Example1Dfit.m from m_6_VACedit.m edited from m_6.m
% USE SAME NOTATION AS Net2DA8mutualsFIT.m

% NB This 1D original version as explained in Appendix B.
% The Net2D fit is more accurate because the stringer has
% been decomposed into a metal-capacitor-metal structure

% SAME NOTATION AS DISSIPATIVE PAPER
% ADD A FICTIVE LAST (Nth) STRINGER WITH INFINITE IMPEDANCE TO REPRESENT
% ZERO CURRENT BOUNDARY CONDITION. KEEP ORDER N THROUGHOUT

% First calculate for a perfect screen (vacuum spectrum)

clear, clc

%Attention: unit� de longueur en cm
H1 = 2.5; % stringer length [cm]
Hcap = 0.36; % capacitor length [cm] to remove from stringer

H2 = 19; % leg effective length [cm]

a1 = 0.6; % stringer "width" [cm]
a2 = 0.3; % leg radius [cm]
facs = 1;
DscreenL = 4.5*facs;
DscreenC = 4.8*facs;
% Dscreen = 4.5;
%% Capacitance
C1 = 2.6e-9; % stringer capacitance 2.6 nF

%% self inductances
mu0 = 4*pi*1e-7; % [H/m]
L1 = mu0.*((H1-Hcap)./100)./2./pi.*(log(2.*(H1-Hcap)./a1)+0.5); % stringer strip self-inductance
L2 = mu0.*(H2./100)./2./pi.*(log(2.*H2./a2)-1); % leg bar inductance
%%
N = 23; % number of legs
%% Load the experimental data for Fig. 4.20
AAA = load('A8.dat'); % network analyser measurement data file
fm = AAA(:,1); % measured frequency
Rm = AAA(:,2); % measured resistance Re(Zin)
Xm = AAA(:,3); % measured reactance Im(Zin)
%% RF connection positions
Nf = 12; % RF feedpoint
Ng = 8; % ground for Fig. 4.20

%% A grounds and feeding; use Af - Ag
SM=[0 0 0 0 0 0 0 -1 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0]; % line A
SK=[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]; % line B
%% transpose
SM=SM';
SK=SK';
%% calculate the antenna and screen mutual inductance matrix (indept of f)
fac = 1; %  fac=0 to put bar and stringer mutuals to zero
facIM=1; % facIM=0 puts all screen image mutuals to zero
for k = 1:N  % matrix column? index
    
    for j = 1:N  % matrix row? index
          
                % using Grover expression p45 eq. 28 for legs
                deltaL = -H2; % delta is negative H2 for overlap
                dL(k,j) = abs(k-j).*H1; % separation d
                alphaL = H2 + H2 + deltaL; % alpha
                betaL = H2 + deltaL; % beta
                gamaL = H2 + deltaL; % gamma
                % mutual inductance for legs
                L(k,j) = fac*1e-9.*(alphaL.*asinh(alphaL./dL(k,j)) - betaL.*asinh(betaL./dL(k,j)) - gamaL.*asinh(gamaL./dL(k,j)) + deltaL.*asinh(deltaL./dL(k,j)) - ...
                    sqrt(alphaL.^2 + dL(k,j).^2)+sqrt(betaL.^2 + dL(k,j).^2)+sqrt(gamaL.^2 + dL(k,j).^2)-sqrt(deltaL.^2 + dL(k,j).^2));
                
                 % using Grover expression p45 eq. 28 for leg images
                deltaLim = -H2;
                dpLim(k,j) = abs(k-j).*H1;
                dLim(k,j) = sqrt(dpLim(k,j).^2 + (2.*DscreenL).^2);
                alphaLim = H2 + H2 + deltaLim;
                betaLim = H2 + deltaLim;
                gamaLim = H2 + deltaLim;
                % mutual inductance for leg images
                Lim(k,j) = facIM*1e-9.*(alphaLim.*asinh(alphaLim./dLim(k,j)) - betaLim.*asinh(betaLim./dLim(k,j)) - gamaLim.*asinh(gamaLim./dLim(k,j)) + deltaLim.*asinh(deltaLim./dLim(k,j)) - ...
                    sqrt(alphaLim.^2 + dLim(k,j).^2)+sqrt(betaLim.^2 + dLim(k,j).^2)+sqrt(gamaLim.^2 + dLim(k,j).^2)-sqrt(deltaLim.^2 + dLim(k,j).^2));
                
                % using Grover expression p45 eq. 28 for in-line stringers
                deltaM(k,j) = (abs(k-j)-1).*H1;
                dM = eps;
                alphaM(k,j) = (H1-Hcap) + (H1-Hcap) + deltaM(k,j);
                betaM(k,j) = (H1-Hcap) + deltaM(k,j);
                gamaM(k,j) = (H1-Hcap) + deltaM(k,j);
                % mutual inductance for in-line stringers (rather use the
                % limiting expression eq.35)
                M(k,j) = fac*1e-9.*(alphaM(k,j).*asinh(alphaM(k,j)./dM) - betaM(k,j).*asinh(betaM(k,j)./dM) - gamaM(k,j).*asinh(gamaM(k,j)./dM) + deltaM(k,j).*asinh(deltaM(k,j)./dM) - ...
                    sqrt(alphaM(k,j).^2 + dM.^2)+sqrt(betaM(k,j).^2 + dM.^2)+sqrt(gamaM(k,j).^2 + dM.^2)-sqrt(deltaM(k,j).^2 + dM.^2));
               
                % using Grover expression p45 eq. 28 for stringer images
                deltaMim(k,j) = (abs(k-j)-1).*H1;
                dMim = 2.*DscreenC;
                alphaMim(k,j) = (H1-Hcap) + (H1-Hcap) + deltaMim(k,j);
                betaMim(k,j) = (H1-Hcap) + deltaMim(k,j);
                gamaMim(k,j) = (H1-Hcap) + deltaMim(k,j);
                % mutual inductance for stringer images
                Mim(k,j) = facIM*1e-9.*(alphaMim(k,j).*asinh(alphaMim(k,j)./dMim) - betaMim(k,j).*asinh(betaMim(k,j)./dMim) - gamaMim(k,j).*asinh(gamaMim(k,j)./dMim) + deltaMim(k,j).*asinh(deltaMim(k,j)./dMim) - ...
                    sqrt(alphaMim(k,j).^2 + dMim.^2)+sqrt(betaMim(k,j).^2 + dMim.^2)+sqrt(gamaMim(k,j).^2 + dMim.^2)-sqrt(deltaMim(k,j).^2 + dMim.^2));
                
                % using Grover expression p45 eq. 28 for opposing stringers
                deltaM2(k,j) = (abs(k-j)-1).*H1;
                dM2 = H2;
                alphaM2(k,j) = (H1-Hcap) + (H1-Hcap) + deltaM2(k,j);
                betaM2(k,j) = (H1-Hcap) + deltaM2(k,j);
                gamaM2(k,j) = (H1-Hcap) + deltaM2(k,j);
                % mutual inductance for opposing stringers
                M2(k,j) = fac*1e-9.*(alphaM2(k,j).*asinh(alphaM2(k,j)./dM2) - betaM2(k,j).*asinh(betaM2(k,j)./dM2) - gamaM2(k,j).*asinh(gamaM2(k,j)./dM2) + deltaM2(k,j).*asinh(deltaM2(k,j)./dM2) - ...
                    sqrt(alphaM2(k,j).^2 + dM2.^2)+sqrt(betaM2(k,j).^2 + dM2.^2)+sqrt(gamaM2(k,j).^2 + dM2.^2)-sqrt(deltaM2(k,j).^2 + dM2.^2));
                
                % using Grover expression p45 eq. 28 for opposing stringer images
                deltaM2im(k,j) = (abs(k-j)-1).*H1;
                dM2im = sqrt(H2.^2 + (2.*DscreenC).^2);
                alphaM2im(k,j) = (H1-Hcap) + (H1-Hcap) + deltaM2im(k,j);
                betaM2im(k,j) = (H1-Hcap) + deltaM2im(k,j);
                gamaM2im(k,j) = (H1-Hcap) + deltaM2im(k,j);
                % mutual inductance for opposing stringer images
                M2im(k,j) = facIM*1e-9.*(alphaM2im(k,j).*asinh(alphaM2im(k,j)./dM2im) - betaM2im(k,j).*asinh(betaM2im(k,j)./dM2im) - gamaM2im(k,j).*asinh(gamaM2im(k,j)./dM2im) + deltaM2im(k,j).*asinh(deltaM2im(k,j)./dM2im) - ...
                    sqrt(alphaM2im(k,j).^2 + dM2im.^2)+sqrt(betaM2im(k,j).^2 + dM2im.^2)+sqrt(gamaM2im(k,j).^2 + dM2im.^2)-sqrt(deltaM2im(k,j).^2 + dM2im.^2));
                
       if j==k % the ONLY elements NOT described by filament expressions:
       % self inductance IS a special case of mutual inductance, but here,
       % each wire is described by a SINGLE filament, which therefore
       % cannot describe the self-inductance of an extended object.
       L(k,j) = L2; % leg self inductance
       M(k,j) = L1; % stringer self inductance
       end
        
    end % of loop over j
end % of loop over k

%% Calculate antenna and screen mutuals for vacuum case
MMvac=M-Mim-M2+M2im; % stringer inductance matrix including in-line, image,
% opposing and opposing image (image of image therefore positive);
% see Jin p166 eq.4.49). Once only, because of M and K currents.
MMvac(1:N,N)=ones(N,1)*1e4; % force Nth fictive stringer to have no current
MMvac(N,1:N)=ones(1,N)*1e4; % force Nth fictive stringer to have no current

LLvac = L-Lim; % leg inductance matrix including image

for k = 1:N % identity matrix

    for j = 1:N
        
        if j==k;
            
            Id(k,j) = 1;
        else
            Id(k,j) = 0;
        end
    end
end

for k = 1:N % lower shift matrix
    
    for j = 1:N
        
        if j==k-1;
            
            Sm(k,j) = 1;
        else
            Sm(k,j) = 0;
        end
    end
end

U = (Id-Sm); % U
UT = transpose(U); % UT
%% Frequency range for the model
df = 0.5*1e4;
f = 8e6:df:24e6; % frequency range of Fig. 4.20
w = 2*pi*f; % angular frequency

%% Calculate the leg and stringer skin effect resistances  
rhoCu = 17e-9; % copper resistivity [ohm.m]
skin = sqrt(2.*rhoCu./w./mu0); % skin depth in copper [m]
Surf1 = pi.*((a1/100).^2-((a1/100)-skin).^2); % area of stringer skin
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2); % area of leg skin

ESR = 0.0064+1.2e-4*f/1e6; % Equivalent Series Resistance of capacitors [ohm]

Rsd1 = rhoCu.*H1/100./Surf1; % stringer resistance
Rsd2 = rhoCu.*H2./100./Surf2; % leg resistance 

R01 = Rsd1 + ESR; % total stringer resistance
R02 = Rsd2; % leg resistance

Rpul = 0.025; % [ohm/m] 

R1 = R01 + Rpul*H1/100; % 
R2 = R02 + Rpul*H2/100; % 

p = find(w); % p is simply the vector of indices of w
for p = min(p):max(p) % from first frequency to last frequency

    Z1 = 1i*w(p)*MMvac+(R1(p)-1i/w(p)/C1)*Id; % total stringer impedance (now same as Dissipative paper)
    Z2 = 1i*w(p)*LLvac+R2(p)*Id; % total leg impedance (now same as Dissipative paper)
%% Calculate the I currents directly in order N:
    LHS=U*inv(Z1)*UT*Z2+2*Id; % order N (see B.13)
    CI=inv(LHS)*(SK-SM); % order N solution for the leg currents (B.14)
    A=0.5*(UT\Z1/U*(SM+SK)-Z2*CI); % (B.16) Eq.9, for top node voltages (N valued)
    B=0.5*(UT\Z1/U*(SM+SK)+Z2*CI); % (B.15) Eq.9, for bottom node voltages (N valued)
    CK=U\(SK-CI); % Eq.7, vector of N-1 currents in bottom stringers PLUS zero current in last stringer
    CM=U\(CI+SM); % Eq.7, vector of N-1 currents in top stringers PLUS zero current in last stringer
    % OLD ZinVAC(p) = dot(SM,A);  % Zin for antenna and screen in vacuum vs f
    Zin(p) = A(Nf)-A(Ng);  % Zin for antenna and screen in vacuum vs f for unit current
% CI,A,B,K,M,Zin are the same as for (N-1) calculation
end % of vacuum calculation

%% Correction du spectre d'impedance : self en serie (long=dconS + ligne de transmission long=dTL)
% impedance correction for 50 ohm coax cable connection to antenna
% copied from Net2DA8mutualsFIT.m
Rin = real(Zin);
Xin = imag(Zin);

dconS = 0.045; % connector stub length
aconS = 0.003; % connector stub radius

LconS = mu0.*(dconS)./2./pi.*(log(2.*(dconS)./aconS)-1); % stub inductance
Rcons = 0.05; % stub guessed resistance

ZconS = 1i.*w.*LconS+Rcons; % stub impedance

Xinc = Xin+imag(ZconS);
Rinc = Rin + real(ZconS);

Zinc = Rinc + 1i.*Xinc;

Z0 = 50; % 

G0TL = (Zinc-Z0)./(Zinc+Z0);

c = 3e8;
betTL = w./(0.7.*c);
dTL = 0.05;
GTL = G0TL.*exp(-1i.*2.*betTL.*dTL);

GrTL = real(GTL);
GxTL = imag(GTL);

Rin2 = Z0.*(1-GrTL.^2-GxTL.^2)./((1-GrTL).^2+GxTL.^2);
Xin2 = Z0.*(2.*GxTL)./((1-GrTL).^2+GxTL.^2);

%% plot
figure(6), clf
subplot(211)
plot(fm/1e6,Rm,'k.','LineWidth',0.5), hold on % measurement
plot(f/1e6,Rin2,'r-','LineWidth',1)%,'LineWidth',1) % Vacuum impedance for antenna and screen in vacuum
ylabel('Re(Zvac)')
xlabel('f [MHz]')
axis([8 24 -2 300])

subplot(212)
plot(fm/1e6,Xm,'k.','LineWidth',0.5), hold on % measurement
plot(f/1e6,Xin2,'r-','LineWidth',1) % Vacuum impedance for antenna and screen in vacuum
ylabel('Im(Zvac)')
xlabel('f [MHz]')
axis([8 24 -150 150])

