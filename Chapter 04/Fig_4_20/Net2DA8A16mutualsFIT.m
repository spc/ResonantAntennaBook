% Net2DA8A16mutualsFIT.m
% from Net2D.m by PhG
clear, clc
%% Dimensions
% indices: 1 horizontal (stringer), 2 vertical (leg)

% Careful for length units: [cm]
H1 = 2.5; % stringer length ok
Hcap = 0.36; % capacitor length to remove from stringer length

H2 = 19; % leg effective length ok

a1 = 0.6; % stringer strip width ok
a2 = 0.3; % leg radius ok
facs = 1; % used here only
DscreenL = 4.5*facs; % distance of bars from screen
DscreenC = 4.8*facs; % distance of capacitor assembly from screen
% Dscreen = 4.5;

%% Capacitance
C1 = 2600e-12; %  stringer capacitance 2.6 nF ok
C2 = 1; % a capacitor is included in the leg for a 2D network treatment;
% for the usual high-pass ladder antennas, the capacitive impedance
% is zero, so C2 = 1 Farad means that it is effectively absent

%% Selfs
mu0 = 4*pi*1e-7; % permeability of free space [H/m]
% self inductance of stringer strip, width a1:
L1 = mu0.*((H1-Hcap)./100)./2./pi.*(log(2.*(H1-Hcap)./a1)+0.5);
% L1 = 10.549 nH; ok
% self inductance of leg wire, radius a2:
L2 = mu0.*((H2)/100)./2./pi.*(log(2.*(H2)./a2)-1);
% L2 145.98 nH; ok
%% Dimensions (2D formulation)
N = 23; % 23 legs ok, 23 horizontal nodes
K = 2; % 2 vertical nodes (2 stringers ok)

%% Load the experimental data
% 'A8A16.dat' is experimental data for two grounds A8 and A16:
AAA = load('A8A16.dat'); % network analyser measurement data file
fm = AAA(:,1); % measured frequency
Rm = AAA(:,2); % measured resistance Re(Zin)
Xm = AAA(:,3); % measured reactance Im(Zin)

%% Feeding RF at A12 and grounds at A8 and A16 (for Fig. 4.18)
Nt1 = 8; % ground at node 8 ok
Kt1 = 1; % ground at stringer A ok, i.e. ground at A8 ok
Nt2 = 16; % second ground at node A16 (Fig. 4.18 only)
Kt2 = 1; % 

% number of grounds (max 2, ASSUMED SYMMETRIC WRT Nf)
nbt = 2; % two ground connections for Fig. 4.18
% 2 GROUNDS IN CHAPTERS 3 AND 4, USED ONLY FOR MEASUREMENTS.
% only Fig. 4.18 uses the model for two grounds


% Position Vin (index "f" for "feed")
Nf = 12; % RF feed at node 12 ok
Kf = 1; % RF feed at stringer A ok, RF feed at A12 ok

%% Definition vecteur S (the source current array (on stringer A?))

Iin = 1; % normalized to unit RF input current)

if nbt==1 % only one ground ok SET UP THE SOURCE ARRAY
    
    for k = 1:(N*K) % total horizontal node counter (N for stringer A plus N for stringer B)
        if k==((Kf-1)*N+Nf) %  source on stringer A at Nf
            SS(k) = Iin; % RF I in at node Nf on stringer A
        elseif k==((Kt1-1)*N+Nt1) % ground at Ng 
            SS(k) = -Iin; % RF I out at node Ng on stringer A
        else
            SS(k) = 0; % all other nodes not connected to source
        end
    end % SS is the source array, S^J in appendix B (B.1) and (B.18)
  
else % for two symmetric grounds:
    
    for k = 1:(N*K)
        
        
        if k==((Kf-1)*N+Nf)
            
            SS(k) = Iin; % I in at node Nf on stringer A
        elseif k==((Kt1-1)*N+Nt1)
            SS(k) = -Iin/2; % I out shared between two grounds ASSUMED SYMMETRIC, NOT GENERAL!
        elseif k==((Kt2-1)*N+Nt2)
            SS(k) = -Iin/2; % I out shared between two grounds ASSUMED SYMMETRIC, NOT GENERAL
        else
           SS(k) = 0;
        end
    end
end
[DD,VV]=size(SS); % [only 1 source side stringer A, total 46 nodes)
if DD==1 % ok
    SS=SS'; % transpose to a column array (as in appendix B (B.1))
else  % 
end

%% Definition Matrices S1, SN, U1, UN and transpositions

for k = 1:(N*K) % reminder N=23 and K=2
    
    for j = 1:(N*K)
        
        if j==k
            
            Id(k,j) = 1; % create a 46 x 46 identity matrix I think
            
        else
            Id(k,j) = 0;
        end
    end
end

m = 1; % m multiplies N below

for k = 1:(N*K)
    
    for j = 1:(N*K)
        
        if j==k-1
            
            if j==m*N % here is m
                S1(k,j) = 0;
                m = m+1; % and here is m increased
            else
                S1(k,j) = 1; % this is the lower shift matrix??
            end
        else
            S1(k,j) = 0;
        end
    end
end

m = 1; % re-set m to 1

for k = 1:(N*K)
    
    for j = 1:(N*K)
        
        if j==k-N
            SN(k,j) = 1;
        else
            SN(k,j) = 0;
        end
    end
end

U1 = Id-S1; % U = U1 = identity - lower shift (B.4)
UN = Id-SN; % see appendix B
U1T = transpose(U1);% U = U1 = identity - lower shift (B.4)
UNT = transpose(UN);
S1T = transpose(S1); 
SNT = transpose(SN);


%% Definition Matrices Self avec self des branches hypothetiques (LEnds) �lev�e

LEnds = 1e4; % effective open circuit at ends ("infinite" inductance)

%MUT = 1;

%if MUT==1
    
    CalcMatMut % calculate matrix of mutual inductances for antenna
    CalcMatScreen % calculate matrix of mutual inductances with screen
    
%else
    
%    CalcMatDiag % no mutuals
    
%end


%% Frequency range for the model
df = 0.5*1e4;
f = 8e6:df:24e6;
w = 2*pi*f;

%% Resistance of the conductors
rhoCu = 17e-9; % electrical resistivity of copper [ohm.m]
skin = sqrt(2.*rhoCu./w./mu0); % skin depth [m]
Surf1 = pi.*((a1/100).^2-((a1/100)-skin).^2); % surface area of stringer (assumed cylindrical)
Surf2 = pi.*((a2/100).^2-((a2/100)-skin).^2); % surface area of leg

ESR = 0.0064+1.2e-4*f/1e6; % Equivalent Series Resistance of capacitors [ohm]

Rsd1 = rhoCu.*H1/100./Surf1; % stringer conductor resistance
Rsd2 = rhoCu.*H2/100./Surf2; % leg resistance

R01 = Rsd1 + ESR; % total stringer resistance, about 10 m.ohm, ok
R02 = Rsd2; % leg resistance, about 14 m.ohm, ok for Fig. 3.13 caption

Rp = 0.025; % [ohm/m] ad hoc per unit length

R1 = R01 + Rp*H1/100;
R2 = R02 + Rp*H2/100;

%% Calcul input impedance fonction de frequence

p = find(w); % array of the index numbers
tN0 = 10;  % percentage step length
Lfr = length(f); % highest index number
Zin = zeros(1,length(p)); % initialize the input impedance array
for p = min(p):max(p) % over the whole range of frequency
    
    testN0 = p/Lfr;
    
    if testN0*100>=tN0
        
        stN0 = num2str(tN0);
        disp([stN0 '%'])
        tN0 = tN0+10;
    else
    end
    
    % matrices impedances (includes mutuals - see appendix B)
    Z1 = 1i*w(p)*mu1 + (R1(p)-1i/w(p)/C1)*Id; % stringer impedance matrix
    Z2 = 1i*w(p)*mu2 + (R2(p)-1i/w(p)/C2)*Id; % leg impedance matrix
    % C2 impedance is effectively zero - see above for C2
    
    % equation generale
    IZ1 = Z1\Id; % stringer current for unit current
    IZ2 = Z2\Id; % leg current for unit current
    Op = U1*IZ1*U1T+UN*IZ2*UNT; % see appendix
    AA = Op\SS; % 
    
    % definition Zin=DeltaV/Iin avec Iin = 1A;
    if nbt==1 % single ground
        Zin(p) = AA((Kf-1)*N+Nf)-AA((Kt1-1)*N+Nt1);
        % input impedance = voltage difference for unit current
        
    else
        Zin(p) = AA((Kf-1)*N+Nf)-AA((Kt1-1)*N+Nt1)/2-AA((Kt2-1)*N+Nt2)/2;
        % input impedance for 2 grounds
    end
    
end

%% Impedance corrections: series connector, and transmission line dTL

Rin = real(Zin);
Xin = imag(Zin);

dconS = 0.045; % connector stub length
aconS = 0.003; % connector stub radius

LconS = mu0.*(dconS)./2./pi.*(log(2.*(dconS)./aconS)-1); % stub inductance
Rcons = 0.05; % stub resistance

ZconS = 1i.*w.*LconS+Rcons; % stub connector impedance

Xinc = Xin+imag(ZconS);
Rinc = Rin + real(ZconS);

Zinc = Rinc + 1i.*Xinc; % impedance corrected for stubs

% impedance correction for 50 ohm coax cable connection to antenna
Z0 = 50;

G0TL = (Zinc-Z0)./(Zinc+Z0);

c = 3e8;
betTL = w./(0.7.*c);
dTL = 0.05;
GTL = G0TL.*exp(-1i.*2.*betTL.*dTL);

GrTL = real(GTL);
GxTL = imag(GTL);

Rin2 = Z0.*(1-GrTL.^2-GxTL.^2)./((1-GrTL).^2+GxTL.^2);
Xin2 = Z0.*(2.*GxTL)./((1-GrTL).^2+GxTL.^2);

%% plots

figure(20)
clf
subplot(2,1,1)
plot(fm/1e6,Rm,'k.','LineWidth',0.5) % plot experimental R measurement
hold
plot(f/1e6,Rin2,'r','LineWidth',1) % plot calculated antenna resistance
axis([8 24 -2 300]);
xlabel('\itf \rm[MHz]')
ylabel('Re(\itZ\rm) [\Omega]')

%plot([22.97 22.97],[-300 300],'k'), text(21.9,60,'\it{m}\rm=1'),
plot([20.34 20.34],[-300 300],'k'), text(20.44,60,'2')
%plot([17.91 17.91],[-300 300],'k'), text(18.01,60,'3'),
plot([16.00 16.00],[-300 300],'k'), text(16.1,60,'4')
%plot([14.52 14.52],[-300 300],'k'), text(14.62,60,'5'),
plot([13.42 13.42],[-300 300],'k'), text(13.52,60,'6')
%plot([12.55 12.55],[-300 300],'k'), text(12.65,60,'7'),
plot([11.85 11.85],[-300 300],'k'), text(11.95,60,'8')
%plot([11.27 11.27],[-300 300],'k'), text(11.37,60,'9'),
plot([10.79 10.79],[-300 300],'k'), text(10.8,60,'10')
%plot([10.40 10.40],[-300 300],'k'), 
plot([10.05 10.05],[-300 300],'k')
%plot([9.77 9.77],[-300 300],'k'), 
plot([9.54 9.54],[-300 300],'k')
%plot([9.34 9.34],[-300 300],'k'), 
plot([9.15 9.15],[-300 300],'k')
%plot([9.02 9.02],[-300 300],'k'), 
plot([8.88 8.88],[-300 300],'k')
%plot([8.785 8.785],[-300 300],'k'), 
plot([8.727 8.727],[-300 300],'k')
%plot([8.68 8.68],[-300 300],'k'), 
plot([8.64 8.64],[-300 300],'k'), text(8.06,60,'22'),


subplot(2,1,2)
plot(fm/1e6,Xm,'k.','LineWidth',0.5) % measurement
hold
plot(f/1e6,Xin2,'r','LineWidth',1) % calculation
axis([8 24 -150 150]);
xlabel('\itf \rm[MHz]')
ylabel('Im(\itZ\rm) [\Omega]')
legend('measurement of input impedance','mutual partial impedance matrix model')

save('A8A16.mat','Rin2','Xin2')
%% oneFrequ: for further analysis at one specified frequency
% use 13.38 MHz input to 'oneFrequ' for the m=6 mode model in Fig. 4.18
% for the case of two grounds, A8 and A16
%oneFrequA8A16

