All files must be downloaded to the same folder to run Matlab.

The original 1D matrix version as described in Appendix B,
run Example1Dfit.m (see Matlab output Fig. 6),
is not as accurate as the spectrum fit in the book Fig. 4.20.

The Net2D fit (run Net2DA8mutualsFIT.m) is more accurate because the stringer has
been decomposed into a metal-capacitor-metal structure (not because of the 2D treatment, see section 14.2).

Run Net2DA8mutualsFIT.m gives Matlab output Fig. 1 and saves data to A8.mat
Run Net2DA8A16mutualsFIT.m gives Matlab output Fig. 20 and saves data to A8A16.mat
Run PlotSpectra.m combines all the data files to give Matlab output Fig. 4,
which is the required book figure, Fig. 4.20.