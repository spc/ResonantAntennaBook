It is necessary to download the data file Fig5corr.dat to the same folder as ModeFrequencies.m

The frequencies for experiment, and the matrix model, are taken from book Fig. 4.20.
The analytical calculation is given in ModeFrequencies.m in the folder Fig_3_8.
All three of the frequencies are saved in the data file Fig5corr.dat.

Run ModeFrequencies.m to obtain Matlab output Fig. 1, the same as book Fig. 4.21.