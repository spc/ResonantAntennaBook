clear % ModeFrequencies.m
clc
figure(1), clf
subplot(111)

Fig5=load('Fig5corr.dat'); % data corrected for mode frequencies in paper
n=Fig5(:,1);
experiment=Fig5(:,2);
analytical=Fig5(:,3);
matrix=Fig5(:,4);

plot(n,experiment,'k-x','MarkerSize',10,'LineWidth',1.5), hold on
plot(n,matrix,'ko', 'MarkerSize',8,'LineWidth',1.5), hold on
plot(n,analytical,'k-d','MarkerSize',8,'LineWidth',1.5), hold on
plot([5 5],[0 30],'k--','LineWidth',1.0), hold on
plot([10 10],[0 30],'k--','LineWidth',1.0), hold on
plot([15 15],[0 30],'k--','LineWidth',1.0), hold on
plot([20 20],[0 30],'k--','LineWidth',1.0), hold on

legend('experimental measurement','mutuals matrix model','normal mode model')
axis([0 23 0 30])
set(gca,'fontname','times','fontsize',16,'LineWidth',2.0,'TickLength',[0.02 0.025]) % for axes
xlabel('mode number \itm \rm= 1 to 22','interpreter','tex','fontsize',16,'fontname','times')
ylabel('mode frequency [MHz]','interpreter','tex','fontsize',16,'fontname','times')

