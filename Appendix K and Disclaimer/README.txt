Concerning the book entitled:

"Resonant Network Antennas for Radio-Frequency Plasma Sources",
by Philippe Guittienne, Alan Howling, and Ivo Furno.

IOP Series in Plasma Physics, copyright IOP Publishing Ltd 2024

ISBN 978-0-7503-5296-3 (ebook)
ISBN 978-0-7503-5294-9 (print)
ISBN 978-0-7503-5297-0 (myPrint)
ISBN 978-0-7503-5295-6 (mobi)
DOI 10.1088/978-0-7503-5296-3

Published by IOP Publishing, wholly owned by The Institute of Physics, London

IOP Publishing, No.2 The Distillery, Glassfields, Avon Street, Bristol, 
BS2 0GR, UK

US Office: IOP Publishing, Inc., 190 North Independence Mall West, Suite 601, 
Philadelphia, PA 19106, USA

This link, https://gitlab.epfl.ch/spc/ResonantAntennaBook, is provided for 
readers to freely download Matlab [1] programs � according to availability � 
which were used to calculate many of the figures throughout the book. We hope 
that these can be useful for adapting the parameter values to new, individual 
projects. These files are by no means intended as examples of good programming. 
The library of routines may be updated from time to time after publication of the 
book itself.

Please reference the book in any use of these Matlab files.

Disclaimer: The authors cannot be held responsible for any consequences arising 
from errors in the Matlab files.

Contact: 
Dr. Philippe Guittienne: philippe.guittienne@epfl.ch
Dr. Alan Howling (retired): alan.howling@epfl.ch
Professor Ivo Furno: ivo.furno@epfl.ch

[1] The MathWorks Inc. (2024). MATLAB version: 9.5.0 (R2018b), Natick, Massachusetts: The MathWorks Inc. https://www.mathworks.com

